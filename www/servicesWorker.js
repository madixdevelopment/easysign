/* xlsx.js (C) 2013-2015 SheetJS -- http://sheetjs.com */

importScripts('lz-string.min.js');
/*
onmessage = function (oEvent) {
  var v;
  try {
    v = XLSX.read(oEvent.data.d, {type: oEvent.data.b ? 'binary' : 'base64'});
  } catch(e) { postMessage({t:"e",d:e.stack||e}); }
  postMessage({t:"xlsx", d:JSON.stringify(v)});
};*/
console.log('testing web worker');

self.addEventListener('message', function(e) {
  //self.postMessage(e.data);
    console.log('in the web worker data');
    //e.data = 'hello from the worker';
    //self.postMessage(e.data);
    
    //material list is really large, so have to compress it first.
    console.log('compressing');
    var jsonMaterialList = JSON.stringify(e.data);
    var compressed = LZString.compress(jsonMaterialList);
    //$localStorage.materialListCompress = compressed;
    //window.localStorage.setItem('materialListCompressed',compressed);
    //console.log('data stored');
    jsonMaterialList = '';
    compressed = '';
    
    self.postMessage(compressed);
}, false);
