{
"header" : {
  "navbar" : {
    "SHIPMENTLIST" : "Shipment List",
    "PROFILE" : "Profile",
    "SYNCH" : "Synch",
    "ADMIN" : "Admin"
  }
},
"app" : {
  "shipmentList" : {
    "TITLE" : "Shipment List",
    "SHIPMENT" : "Shipment",
    "TOTAL" : "Total",
    "DESTINATION" : "Destination",
    "CUSTOMER" : "Customer",
    "APPOINTMENTTIME" : "Appointment Time",
    "INSTRUCTIONS" : "Drivers, to sign in please select your shipment number from the list below or search for your shipment number with the search box on the right."
  },
  "shipmentDetail" : {
    "SHIPMENTCHECKIN" : "Shipment Check In",
    "CHECKIN" : "Check In",
    "NOTMYSHIPMENT" : "Not My Shipment/Back",
    "SHIPMENT" : "Shipment",
    "TOTAL" : "Total",
    "DESTINATION" : "Destination",
    "CUSTOMER" : "Customer",
    "COMPANYNAME" : "Company Name",
    "DRIVERNAME" : "Driver Name",
    "CELLPHONE" : "Cell Phone #",
    "TRUCKNUMBER" : "Truck Number",
    "TRAILERNUMBER" : "Trailer Number",
    "TERMSHEADER" : "ATTENTION ALL CUSTOMER ARRANGED DRIVERS",   
    "APPOINTMENT" : "Appointment",
    "APPOINTMENTTIME" : "Appointment Time",
    "TERMS1" : "You will be required to complete the following steps before Madix will start loading your trailer.",
    "TERMS2" : "Slide your rear tandems all the way back then back into the designated door.",
    "TERMS3" : "Lower the trailer landing gear all the way down.",
    "TERMS4" : "Disconnect the trailer air line and stay under the trailer.",
    "TERMS5" : "Wait in your tractor.  When we are done loading someone will alert you.",
    "TERMS6" : "A lock will be placed on the trailer brake line by the loader.  When the loading is complete, the lock will be removed.",
    "TERMS7" : "If any step above is altered then loading will immediately stop until all the above requirements are met again.",
    "TERMS8" : "SAFETY IS OUR FIRST AND PRIMARY CONCERN",
    "AGREE"  : "By signing this document, I acknowledge the above safety requirements and procedures and agree to follow them.",
    "SIGNHERE": "Sign Here &amp; Agree to Terms:",
    "CLOSE" : "Close",
    "FINISHCHECKIN" : "Finish Check In",
    "MADIXTITLE" : "Madix Inc. Driver Check In",
    "PICKUP" : "Pick Up # (Shipment): ",
    "CHECKINDATE": "Check In Date",
    "CHECKINTIME" : "Check In Time",
    "FINISHCHECKIN" : "Finish Check In",
    "COMPANYERROR": "Please enter your company name",
    "ERROR": "Error",
    "NAMEERROR": "Please enter your name",
    "PHONEERROR": "Please enter your phone number",
    "TRUCKERROR": "Please enter your truck",
    "TRAILERERROR": "Please enter your trailer",
    "SIGNERROR": "Please sign the document at the bottom of the page"
        
  }
}
}
