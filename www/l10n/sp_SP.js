{
"header" : {
  "navbar" : {
    "SHIPMENTLIST" : "Lista de envío",
    "PROFILE" : "Perfil",
    "SYNCH" : "Sincronizar",
    "ADMIN" : "Administración"
  }
},
"app" : {
  "shipmentList" : {
    "TITLE" : "Envío Lista",
    "SHIPMENT" : "Envío",
    "TOTAL" : "Total",
    "DESTINATION" : "Destino",
    "CUSTOMER" : "Cliente",
    "APPOINTMENTTIME" : "Hora de la cita",
    "INSTRUCTIONS" : "Controladores, para iniciar sesión, seleccione su número de envío de la lista a continuación o busque su número de envío con el cuadro de búsqueda a la derecha."
  },
  "shipmentDetail" : {
    "SHIPMENTCHECKIN" : "Verificación del envío",
    "CHECKIN" : "Registrarse",
    "NOTMYSHIPMENT" : "No mi envío/Espalda",
    "SHIPMENT" : "Envío",
    "TOTAL" : "Total",
    "DESTINATION" : "Destino",
    "CUSTOMER" : "Cliente",
    "COMPANYNAME" : "Nombre de empresa",
    "DRIVERNAME" : "Nombre del conductor",
    "CELLPHONE" : "Teléfono móvil #",
    "TRUCKNUMBER" : "Numero de camion",
    "TRAILERNUMBER" : "Numero del trailer",
    "TERMSHEADER" : "ATENCIÓN TODOS LOS CONTROLADORES DISPUESTOS POR EL CLIENTE",   
    "APPOINTMENT" : "Cita",
    "APPOINTMENTTIME" : "Hora de la cita",
    "TERMS1" : "Se le pedirá que complete los siguientes pasos antes de que Madix comience a cargar su trailer.",
    "TERMS2" : "Deslice sus tándems traseros todo el camino de regreso y luego de vuelta a la puerta designada.",
    "TERMS3" : "Baje el tren de aterrizaje del remolque hasta el fondo.",
    "TERMS4" : "Desconecte la línea de aire del remolque y permanezca debajo del remolque..",
    "TERMS5" : "Espera en tu tractor. Cuando terminemos de cargar, alguien te avisará.",
    "TERMS6" : "El cargador colocará un bloqueo en la línea del freno del remolque. Cuando se complete la carga, se eliminará el bloqueo.",
    "TERMS7" : "Si se modifica cualquier paso anterior, la carga se detendrá inmediatamente hasta que se cumplan todos los requisitos anteriores.",
    "TERMS8" : "LA SEGURIDAD ES NUESTRA PRIMERA Y PRIMARIA PREOCUPACIÓN",
    "AGREE"  : "Al firmar este documento, reconozco los requisitos y procedimientos de seguridad anteriores y acepto seguirlos.",
    "SIGNHERE": "Iniciar sesión aquí &amp; Está de acuerdo con los términos:",
    "CLOSE" : "Cerca",
    "FINISHCHECKIN" : "Terminar el check in",
    "MADIXTITLE" : "Madix Inc. Verificación del controlador",
    "PICKUP" : "Recoger # (Envío): ",
    "CHECKINDATE": "Comprobar en la fecha",
    "CHECKINTIME" : "Hora de entrada",
    "FINISHCHECKIN" : "Finalizar el registro",
    "COMPANYERROR": "Por favor ingrese el nombre de su compañía",
    "ERROR": "Error",
    "NAMEERROR": "Por favor, escriba su nombre",
    "PHONEERROR": "Por favor, introduzca su número de teléfono",
    "TRUCKERROR": "Por favor ingrese su camión",
    "TRAILERERROR": "Por favor ingrese su remolque",
    "SIGNERROR": "Por favor, firme el documento en la parte inferior de la página"
        
  }
}
}