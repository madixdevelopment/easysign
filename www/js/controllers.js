'use strict';

/* Controllers */
angular.module('app.controllers', ['pascalprecht.translate', 'ngCookies'])

.controller("InitController", function($scope, $http, $state, AppService, ShipmentService, $rootScope, $timeout) {
    
     $scope.synchAll = function() {    
            AppService.loadConfiguration(); //load initial configuration

            if ($rootScope.plantId == null)
                {
                    $state.go('app.profile');
                }
            else
                {
                    console.log('synching data queue');
                    ShipmentService.synchShipmentList().then(function(d) {
                      }).catch(function(error) {
                        showMessage('Error', 'easySign cannot connect with Madix SAP, please check your connection and try again.'); 
                      });
                }
        }
				

	   // get the url parameters
         //set up variable.
         $scope.getParameterByName = function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }                

         
        $scope.readRole = function readRole()
        {
         $rootScope.userRole = $scope.getParameterByName('role');
            
        }
        
        $scope.readRole();
    
	
        // this is called on the Synch queue button and every 5 mins for the synchronization.
		$scope.synchData = function synchData() {
            
            if ($rootScope.plantId == null)
                {
                    $state.go('app.profile');
                }
            else
                {
                    console.log('synching data queue');
                    ShipmentService.synchShipmentList().then(function(d) {
                      }).catch(function(error) {
                        showMessage('Error', 'easySign cannot connect with Madix SAP, please check your connection and try again.'); 
                      });
                }
		}
	
		$scope.cancel = function() {
		  window.history.back();
		};
			

        $scope.initializeClock = function()
        {
            $rootScope.clock = "loading clock..."; // initialise the time variable
            $scope.tickInterval = 1000 //ms

            var tick = function() {
                $rootScope.clock = Date.now() // get the current time
                //console.log('tick');
                $timeout(tick, $scope.tickInterval); // reset the timer   
        }

            // Start the timer
            $timeout(tick, $scope.tickInterval);
        }
        
		   // this function will be used to start a timer that will retrigger synchronizations.
		  var timerFrequency = 300000; //600000; //5 minutes
		  var startSynchTimer = function startSynchTimer()
		  {
			  setInterval(function(){
				  console.log('timer fired for synch');
				  $scope.synchData(); //synching 
				  },timerFrequency);
		  }
										
			// this is the beginning... kicks off all processing
            //AppService.loadConfiguration(); //benson perf testing
            $scope.synchAll();
            startSynchTimer(); //starts a 10 minute synch timer
            AppService.countMetrics();
            $scope.initializeClock();
    
	})


  .controller('ModalConfirmCtrl', ['$scope', '$modal', '$log', function($scope, $modal, $log) {
    $scope.items = new Object();
	$scope.items.title = 'title';
	$scope.items.message = 'messaging';
    var ModalInstanceCtrl = function ($scope, $modalInstance, items) {
      $scope.items = items;

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };
    };

    $scope.open = function (size) {
      var modalInstance = $modal.open({
        templateUrl: 'myModalConfirm.html',
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });

      modalInstance.result.then(function () {
        //$scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
  }])
  
  	
  .controller('ModalDemoCtrl', ['$scope', '$modal', '$log', function($scope, $modal, $log) {
    $scope.items = ['item1', 'item2', 'item3'];
    var ModalInstanceCtrl = function ($scope, $modalInstance, items) {
      $scope.items = items;
      $scope.selected = {
        item: $scope.items[0]
      };

      $scope.ok = function () {
        $modalInstance.close($scope.selected.item);
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    };

    $scope.open = function (size) {
      var modalInstance = $modal.open({
        templateUrl: 'myModalContent2.html',
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
  }])
 
 
  .controller('ModalQuotes', ['$scope', '$modal', '$log', function($scope, $modal, $log) {
    $scope.items = ['item1', 'item2', 'item3'];
    var ModalInstanceCtrl = function ($scope, $modalInstance, items) {
      $scope.items = items;
      $scope.selected = {
        item: $scope.items[0]
      };

      $scope.ok = function () {
        $modalInstance.close($scope.selected.item);
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    };

    $scope.open = function (size) {
      var modalInstance = $modal.open({
        templateUrl: 'quoteList.html',
        controller: ModalInstanceCtrl,
        size: size,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
  }])
  
.controller('DatepickerDemoCtrl', ['$scope', function($scope) {
    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };

    $scope.initDate = new Date('2016-15-20');
    //$scope.formats = ['MM/dd/yyyy', 'shortDate'];
    $scope.formats = ['MM/dd/yyyy'];
    $scope.format = $scope.formats[0];
  }])
  
.controller('fileUploadCtrl', ['$scope', function($scope) {
    
    var X = XLSX;
    var XW = {
        /* worker message */
        msg: 'xlsx',
        /* worker scripts */
        rABS: './xlsxworker2.js',
        norABS: './xlsxworker1.js',
        noxfer: './xlsxworker.js'
    };

    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
    if(!rABS) {
        document.getElementsByName("userabs")[0].disabled = true;
        document.getElementsByName("userabs")[0].checked = false;
    }

    var use_worker = typeof Worker !== 'undefined';
    if(!use_worker) {
        document.getElementsByName("useworker")[0].disabled = true;
        document.getElementsByName("useworker")[0].checked = false;
    }

    var transferable = use_worker;
    if(!transferable) {
        document.getElementsByName("xferable")[0].disabled = true;
        document.getElementsByName("xferable")[0].checked = false;
    }

    var wtf_mode = false;

    function fixdata(data) {
        var o = "", l = 0, w = 10240;
        for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
        o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
        return o;
    }

    function ab2str(data) {
        var o = "", l = 0, w = 10240;
        for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint16Array(data.slice(l*w,l*w+w)));
        o+=String.fromCharCode.apply(null, new Uint16Array(data.slice(l*w)));
        return o;
    }

    function s2ab(s) {
        var b = new ArrayBuffer(s.length*2), v = new Uint16Array(b);
        for (var i=0; i != s.length; ++i) v[i] = s.charCodeAt(i);
        return [v, b];
    }

    // should not be used by default...
    function xw_noxfer(data, cb) {
        var worker = new Worker(XW.noxfer);
        worker.onmessage = function(e) {
            switch(e.data.t) {
                case 'ready': break;
                case 'e': console.error(e.data.d); break;
                case XW.msg: cb(JSON.parse(e.data.d)); break;
            }
        };
        var arr = rABS ? data : btoa(fixdata(data));
        worker.postMessage({d:arr,b:rABS});
    }

    // default option
    function xw_xfer(data, cb) {
        var worker = new Worker(rABS ? XW.rABS : XW.norABS);
        worker.onmessage = function(e) {
            switch(e.data.t) {
                case 'ready': break;
                case 'e': console.error(e.data.d); break;
                default: var xx=ab2str(e.data).replace(/\n/g,"\\n").replace(/\r/g,"\\r"); console.log("done"); cb(JSON.parse(xx)); break;
            }
        };
        if(rABS) {
            var val = s2ab(data);
            worker.postMessage(val[1], [val[1]]);
        } else {
            worker.postMessage(data, [data]);
        }
    }

    function xw(data, cb) {
        transferable = document.getElementsByName("xferable")[0].checked;
        if(transferable)  // if the transferable flag is checked, use it.  default is yes.
        {
            xw_xfer(data, cb);
        }
        else 
        {
            xw_noxfer(data, cb);
        }
    }

    // may need to enhance to populate the data.
    function to_json(workbook) {
        var result = {};
        workbook.SheetNames.forEach(function(sheetName) {
            var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            if(roa.length > 0){
                result[sheetName] = roa;
            }
        });
        return result;
    }
    
    function to_csv(workbook) {
        var result = [];
        workbook.SheetNames.forEach(function(sheetName) {
            var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
            if(csv.length > 0){
                result.push("SHEET: " + sheetName);
                result.push("");
                result.push(csv);
            }
        });
        return result.join("\n");
    }

    function to_formulae(workbook) {
        var result = [];
        workbook.SheetNames.forEach(function(sheetName) {
            var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
            if(formulae.length > 0){
                result.push("SHEET: " + sheetName);
                result.push("");
                result.push(formulae.join("\n"));
            }
        });
        return result.join("\n");
    }

    function process_wb(wb) {
        var output = "";    
        
        
        // always convert to json for now.
        output = JSON.stringify(to_json(wb), 2, 2);
        //output = to_formulae(wb);
		//output = to_csv(wb);
        
        // write output to the screen... replace this with
        // easyprice cart population
        if(out.innerText === undefined) 
            {
                out.textContent = output;
            }
        else 
            {
                out.innerText = output;
            }
        
        $scope.finishedUpload(output);

        // if we can access the console, log it.
        if(typeof console !== 'undefined') 
            {
                console.log("output generated from excel at:", new Date());
            }
    }
    
    $scope.finishedUpload = function(output)
    {
        console.log('finished upload');
        // call parent again to 
        $scope.buildUploadedProducts(output);
    }
    
    $scope.handleFileUpload = function(callbackFunction)
    {
        //alert('handling file');
        console.log('loading the file for the user');
        $scope.handleFile();

    }
        
    $scope.handleFile = function()
    {
        var xlf = document.getElementById('xlf');
        rABS = document.getElementsByName("userabs")[0].checked;
        use_worker = document.getElementsByName("useworker")[0].checked;

        var xlf = document.getElementById('xlf');
        var files = xlf.files;
        var f = files[0];
        {
            var reader = new FileReader();
            var name = f.name;

            var e = {}; //benson dummy
            reader.onload = function(e) {
                if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                var data = e.target.result;
                if(use_worker) {
                    xw(data, process_wb);
                } else {
                    var wb;
                    if(rABS) {
                        wb = X.read(data, {type: 'binary'});
                    } else {
                    var arr = fixdata(data);
                        wb = X.read(btoa(arr), {type: 'base64'});
                    }
                    process_wb(wb);
                }
            };
            if(rABS)
            {
                reader.readAsBinaryString(f);
            }
            else 
            {
                reader.readAsArrayBuffer(f);
            }
        }

    }
    

  }])
   
  ;