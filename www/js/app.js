'use strict';
// easyPrice Application

angular.module('app', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.utils',
    'ui.load',
    'ui.jq',
    'oc.lazyLoad',
    'pascalprecht.translate',
    'app.controllers',
    'nouislider',
    'ui.grid',
    'ui.grid.pagination',
    'ui.grid.exporter',
    'wt.responsive'
])
.run(
[ '$rootScope', '$state', '$stateParams', '$templateCache', '$http', '$location', 'AppService',
    function ($rootScope,   $state,   $stateParams, $templateCache, $http, $location, AppService) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;  
   
        $rootScope.serverURL = AppService.determineServer();
        $rootScope.client = AppService.determineClient();
        $rootScope.lastRefresh = 0;
        $rootScope.submittedCount = 0;
		$rootScope.queueCount = 0;
        $rootScope.completeCount = 0;
        $rootScope.approvedCount = 0;
        $rootScope.draftCount = 0;
		$rootScope.errorCount = 0;
        $rootScope.expiringSoonCount = 0;
        $rootScope.needsApprovalCount = 0;
        $rootScope.expiredCount = 0;
        $rootScope.needsApprovalValue = 0;
        $rootScope.approvedValue = 0;
        $rootScope.totalValue = 0;
        $rootScope.totalCount = 0;
        $rootScope.shipmentList = [];
        $rootScope.priceRequestList = [];
        $rootScope.salesRep;
        $rootScope.plant;
        $rootScope.employeeData = {};
        $rootScope.cockpitLoaded = false;
        $rootScope.daysHistory = 7;
        $rootScope.clock = "";

        $rootScope.refreshStartDate;
        $rootScope.refreshEndDate;
        $rootScope.userRole = '';
        $rootScope.userProfile = {};
        
		//$rootScope.message = new Object();
		$rootScope.messageTitle = new String();
		$rootScope.messageText = new String();	
        $rootScope.isOnline = false;
        
        // get the parameter from the url;
        // Example - http://my.site.com/?myparam=33
        var fullUrl = $location.$$absUrl;
        //var employee = getParameterByName('employee', fullUrl);        
        //$rootScope.salesRep = employee;
        $rootScope.plantId = '0030'; //temp
		
    }
  ]
)
;

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
