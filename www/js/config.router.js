'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG) {
          var layout = "tpl/app.html";

            $urlRouterProvider
              .otherwise('/app/shipmentList');

          $stateProvider
              .state('app', {
                  abstract: true,
                  url: '/app',
                  templateUrl: layout
              })

            .state('app.shipmentList', { //user can see their own price submissions
                url: '/shipmentList',
                templateUrl: 'components/Shipments/shipmentList.html'
            })
			 .state('app.shipmentDetail', {
                url: '/shipment/:id',
                templateUrl: 'components/Shipments/shipmentDetail.html'
            })
			 .state('app.shipmentEdit', {
                url: '/shipmentEdit/:id',
                templateUrl: 'components/Shipments/shipmentEdit.html'
            })
			 .state('app.shipmentCreate', {
                url: '/shipmentCreate',
                templateUrl: 'components/Shipments/shipmentCreate.html'
            })
          
			 .state('app.priceDetail', {
                url: '/price/:id',
                templateUrl: 'components/Pricing/priceDetail.html'
            })
			 .state('app.priceEdit', {
                url: '/priceEdit/:id',
                templateUrl: 'components/Pricing/priceEdit.html'
            })
			 .state('app.priceCreate', {
                url: '/priceCreate',
                templateUrl: 'components/Pricing/priceCreate.html'
            })
			 .state('app.priceList', { //user can see their own price submissions
                url: '/priceList',
                templateUrl: 'components/Pricing/priceList.html'
            })
			 .state('app.queueList', {
                url: '/queueList',
                templateUrl: 'components/Queue/queueList.html'
            })
            .state('app.profile', {
                  url: '/profile',
                  templateUrl: 'components/Profile/profileDetail.html'
              })

            
          ;

          function load(srcs, callback) {
            return {
                deps: ['$ocLazyLoad', '$q',
                  function( $ocLazyLoad, $q ){
                    var deferred = $q.defer();
                    var promise  = false;
                    srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                    if(!promise){
                      promise = deferred.promise;
                    }
                    angular.forEach(srcs, function(src) {
                      promise = promise.then( function(){
                        if(JQ_CONFIG[src]){
                          return $ocLazyLoad.load(JQ_CONFIG[src]);
                        }
                        angular.forEach(MODULE_CONFIG, function(module) {
                          if( module.name == src){
                            name = module.name;
                          }else{
                            name = src;
                          }
                        });
                        return $ocLazyLoad.load(name);
                      } );
                    });
                    deferred.resolve();
                    return callback ? promise.then(function(){ return callback(); }) : promise;
                }]
            }
          }


      }
    ]
  );
