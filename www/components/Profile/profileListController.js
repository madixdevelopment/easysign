angular.module("app")
.config(function($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
})
.controller("profileEditCtrl", function ($scope, $http, $stateParams, $rootScope, ProfileService, AppService) {

})
.controller("profileDetailCtrl", function ($scope, $http, $stateParams, $state, $rootScope, $localStorage, ProfileService, AppService, ShipmentService) {

		$scope.plantId; 
        $scope.daysHistory; 
        $scope.shipment = {};

        $scope.gap = 5;
    
        $scope.filteredItems = [];
        $scope.groupedItems = [];
        $scope.itemsPerPage = 5;
        $scope.pagedItems = [];
        $scope.currentPage = 0;
    
		//read details based on the user selected
		$scope.getUser = function getUser() {
			
            if ($rootScope.userProfile)
                {
			     $scope.plantId = $rootScope.plantId; 
                }
            
            $scope.daysHistory = $rootScope.daysHistory;
    	}

        $scope.saveUser = function saveUser()
        {
            $rootScope.plantId = $scope.plantId;
            $localStorage.plantId = $rootScope.plantId;
            
            $rootScope.daysHistory = $scope.daysHistory;
            $localStorage.daysHistory = parseInt($scope.daysHistory);
            
            showMessage('Info', 'Profile updated, reloading data');
            //$state.go('app.cockpit');
            angular.element('#loaderDiv').show();
            ShipmentService.synchShipmentList().then(function(d) {
                $state.go('app.shipmentList');
                angular.element('#loaderDiv').hide();
              }).catch(function(error) {
                showMessage('Error', 'easySign cannot connect with Madix SAP, please check your connection and try again.'); 
                angular.element('#loaderDiv').hide();
              });
        }
        
		$scope.showCancelShipmentCheckIn = function()
		{
			$('#mainScreen').hide();	
			$('#cancelCheckInScreen').show(200);
		}
        
		$scope.hideCancelShipmentCheckIn = function()
		{
			$('#mainScreen').show(200);
			$('#cancelCheckInScreen').hide();			
		}
        
		$scope.showShipmentSearch = function()
		{
            console.log($rootScope.shipmentList);
			$('#cancelCheckInScreen').hide();	
			$('#shipmentSearch').show(200);
		}
        
		$scope.hideShipmentSearch = function()
		{
			$('#cancelCheckInScreen').show(200);
			$('#shipmentSearch').hide();			
		}
        
        $scope.selectShipment = function(item)
        {
            $scope.shipment.SHIPMENT = item.TKNUM;
            console.log(item);
            $scope.hideShipmentSearch();
        }
        
        $scope.cancelCheckIn = function cancelCheckIn()
        {
              ShipmentService.cancelShipmentCheckIn($scope.shipment).then(function(d){
                    if (d.type == 'S') //if it was a success, go to the shipment list
                    {
                        ShipmentService.synchShipmentList().then(function(d) {
                            $state.go('app.shipmentList', {id: 'complete'});    
                        angular.element('#loaderDiv').hide();
                        });
                     }
                    angular.element('#loaderDiv').hide();
              });
        }
        
        // calculate page in place
        $scope.groupToPages = function () {
            
            $scope.filteredItems = $rootScope.shipmentList;
            
            $scope.pagedItems = [];

            for (var i = 0; i < $scope.filteredItems.length; i++) {
                if (i % $scope.itemsPerPage === 0) {
                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                } else {
                    $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                }
            }
        };
    
        $scope.range = function (size,start, end) {
            var ret = [];        
            console.log(size,start, end);

            if (size < end) {
                end = size;
                start = size-$scope.gap;
            }
            for (var i = start; i < end; i++) {
                ret.push(i);
            }        
             console.log(ret);        
            return ret;
        };

        $scope.prevPage = function () {
            if ($scope.currentPage > 0) {
                $scope.currentPage--;
            }
        };

        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.pagedItems.length - 1) {
                $scope.currentPage++;
            }
        };

        $scope.setPage = function () {
            $scope.currentPage = this.n;
        };
    
        $scope.currentPage = 0;
        // now group by pages
        $scope.groupToPages();
    
        $('#cancelCheckInScreen').hide();
        $('#shipmentSearch').hide();
        
        showMessage('Info', 'Please enter your SAP Plant number and save');
        $scope.getUser();
    
});
