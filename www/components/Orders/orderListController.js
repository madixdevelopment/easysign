// order master controller
angular.module("app")
.config(function($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
})
.controller("orderEditCtrl", function ($scope, $http, $stateParams, OrderService) {
	

})
.controller("orderDetailCtrl", function ($scope, $http, $stateParams, OrderService) {
	
		$scope.order = new Object();
		$scope.orderDetails = new Array(); // will hold the order items
		$scope.quote = new Object();
		$scope.opportunity = {};
    
		$scope.order_id = $stateParams.id; //$routeParams.id;
		var Cart = new Array();

		//read details based on the requisition selected
		$scope.getOrder = function (id) {
			$scope.orderDetails = OrderService.getOrder($scope.order_id);
			$scope.order = $scope.orderDetails[0]; //get the first item
            
    	}
				

		// call function to get the order details.
		if ($scope.order_id)
		{
			$scope.getOrder($scope.order_id);
		}

})
.controller("orderCreateCtrl", function ($scope, $http, $stateParams,  $rootScope, OrderService, AppService, PriceService) {
		$scope.orderHeader = new Object();
		$scope.orderDetails = new Array(); // will hold the order items
		$scope.requisitionHeader = new Object();
		$scope.requisitionDetails = new Array();
		$scope.quote = {};
		var Cart = new Array();

        $scope.requestedDeliveryDate = new Date();  
        $scope.requestedDeliveryDate.setDate($scope.requestedDeliveryDate.getDate() + 7); //default to 1 week
        $scope.requestedDeliveryDate = ('0' + ($scope.requestedDeliveryDate.getMonth()+1)).slice(-2) + '/'
								 + ('0' + $scope.requestedDeliveryDate.getDate()).slice(-2) + '/' +
								($scope.requestedDeliveryDate.getFullYear());
    
        $scope.request_id = $stateParams.id; //$routeParams.id;
    
		// logic to validate order create
		$scope.validateOrder = function validateOrder()
		{
			
			if (!$scope.orderHeader.Quote)
			{
				showMessage('Error', 'Please select a quote');
				return false;
			}

			if (!$('#requestedDeliveryDate').val())
			{
				showMessage('Error', 'Please select a delivery date');
				return false;
			}
			
			if (!$scope.orderHeader.PONumber)
			{
				showMessage('Error', 'Please enter a PO number');
				return false;
			}
            
            // verify we have a quantity
            for (var x=0; x<$scope.orderDetails.length;x++)
                {
                    if (  parseFloat($scope.orderDetails[x].Quantity == 0) || 
                        $scope.orderDetails[x].Quantity == "")
                        {
                            var itemNumber = x+1;
                            showMessage('Error', 'Please enter a quantity on item ' + itemNumber);
                            return false;
                        }
                }
            
			return true; // if we are here then all is good
			
		}
		
		// logic to actually save the requisition
		$scope.saveOrder = function () {
			
			var isValid = false;
			isValid = $scope.validateOrder()
			
			
			if (isValid == true)
				{
				//angular.element('#loaderDiv').show();
				//OrderService.addItemsToCart();
				//OrderService.updateCreateFields($scope.orderHeader, $scope.orderDetails);
				//OrderService.getCSRFToken(OrderService.saveOrderCreateData);
                $scope.orderHeader.OrderScore = $scope.orderScore;
                OrderService.saveOrderCreateData($scope.orderHeader, $scope.orderDetails);
				}
		}		
		

		$scope.listQuotes = function () {
			// synch if needed.				
			if ($rootScope.priceRequestList.length == 0)
			{
                angular.element('#loaderDiv').show();
	   	  		AppService.synchPriceRequestList().then(function(d) {
		  		angular.element('#loaderDiv').hide();
	  			});
			}
		}
        
		// this will resynch requisitions
		/*$scope.synchRequisitions = function () {
			
			angular.element('#loaderDiv').show();
			RequisitionService.synchRequisitions().then(function(d) {
				$scope.app.requisitionList = d;
				angular.element('#loaderDiv').hide();
			});
		}*/

		$scope.showOpportunityList = function()
		{
			$('#opportunityListScreen').show(300);
			$('#createScreen').hide();			
		}
        
		$scope.showQuoteList = function()
		{
			$('#quoteListScreen').show(300);
			$('#createScreen').hide();			
		}
    
		$scope.closeQuoteList = function()
		{
			$('#createScreen').show(300);
			$('#quoteListScreen').hide();			
		}
        
        $scope.closeOpportunityList = function()
		{
			$('#createScreen').show(300);
			$('#opportunityListScreen').hide();			
		}
        
		$scope.priceDetails = new Array(); // will hold the price items
    
        $scope.selectOpportunity = function(opportunity)
		{
			$('#opportunityListScreen').hide(0);
			$('#createScreen').show(300);
	
            $scope.priceDetails = PriceService.getOpportunityItems(opportunity.id.OPPORTUNITYID);	
            
            $scope.quote = $scope.priceDetails[0];
            
            if (!$scope.quote) // undefined, no data to retrieve.
            {
                showMessage('Error', 'At least one quote needs to be created for this opportunity.  Please create a quote');
                return;
            }
            
            $scope.orderHeader.Quote = $scope.quote.PRICE_REQUEST;
			$scope.orderHeader.SoldTo = $scope.quote.CUSTOMER;
			$scope.orderHeader.SoldToName = $scope.quote.NAME;
			$scope.orderHeader.ShipTo = $scope.quote.CUSTOMER;
			$scope.orderHeader.ShipToName = $scope.quote.NAME;
            $scope.orderHeader.OpportunityNum = $scope.quote.OPPORTUNITY_NUM;
            $scope.orderHeader.OpportunityName = $scope.quote.OPPORTUNITY_NAME;
			$scope.buildInitialCart();
            
		}
        
		$scope.selectQuote = function(quote)
		{
			$('#quoteListScreen').hide(0);
			$('#createScreen').show(300);
	
            $scope.priceDetails = $scope.loadQuoteData(quote.id);
            $scope.quote = $scope.priceDetails[0];
            $scope.orderHeader.Quote = $scope.quote.PRICE_REQUEST;
            $scope.orderHeader.QuoteNum = $scope.quote.PRICE_REQUEST;
			$scope.orderHeader.SoldTo = $scope.quote.CUSTOMER;
			$scope.orderHeader.SoldToName = $scope.quote.NAME;
			$scope.orderHeader.ShipTo = $scope.quote.CUSTOMER;
			$scope.orderHeader.ShipToName = $scope.quote.NAME;
            $scope.orderHeader.OpportunityNum = $scope.quote.OPPORTUNITY_NUM;
            $scope.orderHeader.OpportunityName = $scope.quote.OPPORTUNITY_NAME;
			$scope.buildInitialCart();
            
            
		}
                
		
		// this is used to load data from a quote in case of a new requisition
		$scope.loadQuoteData = function loadQuoteData(id)
		{
            $scope.quote = PriceService.getPrice(id);	
            return $scope.quote;
		}
	
        $scope.quantityChange = function quantityChange()
        {
            $scope.orderValue = 0;
            $scope.orderTargetValue = 0;
            $scope.orderScore = 0;
            
            var reqLength = $scope.orderDetails.length;
			for (x = 0; x < reqLength; x++)
			{
                
                $scope.orderValue = $scope.orderValue + ($scope.orderDetails[x].BID_PRICE * $scope.orderDetails[x].Quantity);
                
                $scope.orderTargetValue = $scope.orderTargetValue + ($scope.orderDetails[x].TARGET_PRICE * $scope.orderDetails[x].Quantity);

                $scope.orderScore = ($scope.orderValue/$scope.orderTargetValue) * 100;
                $scope.orderScore = $scope.orderScore.toFixed(2);
                //$scope.orderValue = parseFloat($scope.orderValue.toFixed(2));
                
                //formatted data for the cockpit
                 $scope.orderValue = nFormatter( $scope.orderValue);
                 $scope.orderValue = parseFloat($scope.orderValue.toFixed(2));
                                            
			}
        }
        
        // values to capture the overall order value and target values
		$scope.orderValue = 0;
        $scope.orderTargetValue = 0;
        $scope.orderScore = 0;
    
        $scope.configuration = {};
    
		$scope.buildInitialCart = function buildInitialCart()
		{
			//$scope.requisitionDetails = new Array();
			// for the initial cart, we will read the consumables for the specified items.
			var reqLength = $scope.priceDetails.length;
			$scope.orderDetails.length = 0;
			for (x = 0; x < reqLength; x++)
			{
                
                var cartRow = new Object();
                cartRow.Material = $scope.priceDetails[x].MATERIAL;
                cartRow.Description = $scope.priceDetails[x].PRODUCT;
                cartRow.SalesUnit = $scope.priceDetails[x].BID_PRICE_UOM;
                cartRow.Quantity = $scope.priceDetails[x].EXPECTED_SALES;
                cartRow.TARGET_PRICE_INDEX = $scope.priceDetails[x].TARGET_PRICE_INDEX;
                cartRow.BID_PRICE = $scope.priceDetails[x].BID_PRICE;
                cartRow.TPI_CLASS = $scope.priceDetails[x].TPI_CLASS;
                cartRow.TARGET_PRICE = $scope.priceDetails[x].TARGET_PRICE;   
                
                cartRow.PRICE_REQUEST = $scope.priceDetails[x].PRICE_REQUEST;
                cartRow.PRICE_REQUEST_ITM = $scope.priceDetails[x].ITM_NUMBER;                
                
                $scope.orderValue = $scope.orderValue + (parseFloat(cartRow.BID_PRICE) * parseFloat(cartRow.Quantity));
                $scope.orderTargetValue += (parseFloat(cartRow.TARGET_PRICE) * parseFloat(cartRow.Quantity));
                $scope.orderScore = ($scope.orderValue/$scope.orderTargetValue) * 100;
                $scope.orderScore = $scope.orderScore.toFixed(2);
                //$scope.orderValue = $scope.orderValue.toFixed(2);
                
                $scope.orderDetails.push(cartRow);

                // get configuration data
                
                    // populate configuration data
                    if ($scope.priceDetails[x].IS_PARENT_ITEM == 'X')
                    {
                        $scope.configuration.PRODUCT_LINE = $scope.priceDetails[x].PRODUCT_LINE; 
                        $scope.configuration.UNIT_TYPE = $scope.priceDetails[x].UNIT_TYPE; 
                        $scope.configuration.TOTAL_LENGTH = $scope.priceDetails[x].TOTAL_LENGTH; 
                        $scope.configuration.HEIGHT = $scope.priceDetails[x].HEIGHT; 
                        $scope.configuration.BASE_DEPTH = $scope.priceDetails[x].BASE_DEPTH; 
                        $scope.configuration.BACKING = $scope.priceDetails[x].BACKING; 
                        $scope.configuration.SHELF_TYPE = $scope.priceDetails[x].SHELF_TYPE; 
                        $scope.configuration.SHELF_COUNT = $scope.priceDetails[x].SHELF_COUNT; 
                        $scope.configuration.SHELF_DEPTH = $scope.priceDetails[x].SHELF_DEPTH; 
                        $scope.configuration.COLOR = $scope.priceDetails[x].COLOR; 
                        $scope.configuration.CONFIG_QTY = $scope.priceDetails[x].CONFIG_QTY; 
                    }
                
			  // get array of items
			  /*var equipmentConsumablesList = AppService.readConsumablesForEquipType($scope.requisitionDetails[x].Klass);
			    // loop through each consumable and add it to the order details.
			  		  var eqLength = equipmentConsumablesList.length;
					      for (y=0; y < eqLength; y++)
						  	{
								var cartRow = new Object();
								//cartRow.EquipName = $scope.requisitionDetails[x].equipment;
								cartRow.EquipmentType = equipmentConsumablesList[y].EquipmentType;
								cartRow.Material = equipmentConsumablesList[y].Material;
								cartRow.Description = equipmentConsumablesList[y].Description;
								cartRow.SalesUnit = equipmentConsumablesList[y].SalesUnit;
								cartRow.Quantity = '0';
								$scope.orderDetails.push(cartRow);
							}*/
			}
		}
			
		//read details based on the requisition selected
		/*$scope.getRequisition = function (id) {
			
			$scope.requisitionHeader = RequisitionService.getRequisition(id);
						
			//now we have a req, we can load the details.
			$scope.requisitionDetails = $scope.requisitionHeader.To_RequisItems.results; 
    	}*/
		
	//  functions to initialize the controller.
        $scope.listQuotes();
   	   $('#quoteListScreen').hide(0);
       $('#opportunityListScreen').hide(0);
        
    // if we are creating from an opportunity, then look it up.
    // add a field on the object.
    if ($scope.request_id)
        {

            var opportunityFound = false;
            
            for (var x=0; x<$rootScope.opportunityList.length; x++)
                {
                    if ($rootScope.opportunityList[x].OPPORTUNITYID == $scope.request_id)
                        {
                            var opportunity = {};
                            opportunity.id = {};
                            opportunity.id.OPPORTUNITYID = $scope.request_id;
                            $scope.selectOpportunity(opportunity); 
                            opportunityFound = true;
                            break;
                        }
                }

                // if we are here, then it is a quote
            if (opportunityFound == false)
                {
                var quote = {};
                quote.id = $scope.request_id;
                $scope.selectQuote(quote);
                }
        }

})
.controller("orderListCtrl", function ($scope, $http, $rootScope, OrderService) {
    
    $scope.listOrders = function () {
		
			// read from memory if nothing available.
			/*if ($scope.app.orderList.length === 0)
				{
	   			$scope.orderList = OrderService.getOrderList();
				}

			// synch if needed.				
			if ($scope.app.orderList.length === 0)
			{
	   	  		OrderService.synchOrders().then(function(d) {
		  		$scope.app.orderList = d;
	  	    });
			}*/
        
        // moved to service
        /*var orderListLength = $rootScope.orderList.length;
        
        $scope.openOrderList.length = 0;
        for (var x=0; x<orderListLength; x++)
            {
                if ($rootScope.orderList[x].DOC_STATUS != "Completed")
                    {
                        $scope.openOrderList.push($rootScope.orderList[x]);
                    }
            }*/
    }

//  functions to initialize the controller.
   $scope.listOrders(); //call list to start the screen
	
});
