angular.module("app")
.service('OrderService', function($http, $state, $rootScope, AppService) {

		// variable to hold order items.		
		var orderList = new Array();

		var orderHeaderUpdate = new Object();
		var orderItemsUpdate = new Array();
		var orderHeaderCreate = new Object();
		var orderItemsCreate = new Array();
		var orderHeaderOriginal = new Object();
		var orderItemsOriginal = new Array();
		var Cart = new Array();

		this.updateCreateFields = function updateCreateFields(orderHeader, orderItems)
		{
			orderHeaderCreate = orderHeader;
			orderItemsCreate = orderItems;
		}
		
		function saveData(results)
		{
		
		}
		 
		this.synchOrders = function()
		{
            
		}
						

		// this will return the order list
		this.getOrderList = function() { 
			return orderList;
		};
		
		//read details based on the order selected
		this.getOrder = function (id) {
			
			var orderObject = [];
			var reqLength = $rootScope.orderItems.length;
						//now we have an order, we can load the details.
						for (var x = 0; x < reqLength; x++)
						{
							if ($rootScope.orderItems[x].SD_DOC == id)
							{
								orderObject.push($rootScope.orderItems[x]); //copy to header structure
								//break; //exit out if we have the order
							}
						}
			return orderObject;					
    	}
		

		this.addItemsToCart = function addItemsToCart()
		{
				Cart.length = 0; // initialize cart.
				var itemNbr = 10;
			    $('#orderItems').find('tr').each(function () {
					
        			var row = $(this); //use this to get items that are selected
					var quantity = row.find('#quantity').val();
					quantity = parseInt(quantity);
					if (quantity > 0)
						{
							var cartRow = new Object();
							var qty = row.find('#quantity').val();
							 var material = row.find('#material').val();
							 var salesUnit = row.find('#salesUnit').val();
											
							cartRow.ItemNumber = itemNbr.toString();	
							cartRow.QuoteNumber = orderHeaderCreate.Quote;
							cartRow.Material = material; //'GV7IN15KH';
							cartRow.Quantity = qty;
							cartRow.Plant = orderHeaderCreate.Base;
							cartRow.SalesUnit = salesUnit;
							Cart.push(cartRow);
							itemNbr = itemNbr + 10;
			
       					 }
					
  			  	});
		}
		
	this.saveOrderCreateData = function saveOrderCreateData(orderHeaderCreate, orderItemsCreate)
		{
	
            angular.element('#loaderDiv').show();
        
            // create the server url
            var serverURL = $rootScope.serverURL;        
		    var requestUri = serverURL + '/sap/bc/zeasyprice/ZSD_EASYORDER_ORDER_CREATE?sap-client=800';
            var requestURL = requestUri;
        
			// need to add leading zeros to dates... i.e. 8-1-2014 needs to be 08-01-2014
			var startDateFormatted = $('#requestedDeliveryDate').val();
			var startDate = new Date(startDateFormatted);
			reqDelDate = (startDate.getFullYear()) +  '-'
            		 + ('0' + (startDate.getMonth()+1)).slice(-2) + '-'
            		 + ('0' + startDate.getDate()).slice(-2);
	
            var reqDelDate;
			reqDelDate = (startDate.getFullYear())  
            		 + ('0' + (startDate.getMonth()+1)).slice(-2) 
            		 + ('0' + startDate.getDate()).slice(-2);
					
			// build line items.
			var lineItems = Cart; //create array for line items
			
            var orderHeader = {};
            orderHeader.SOLD_TO = orderHeaderCreate.SoldTo;
            orderHeader.SHIP_TO = orderHeaderCreate.ShipTo;
            orderHeader.PO_NUMBER = orderHeaderCreate.PONumber;
            orderHeader.COMMENTS = orderHeaderCreate.Comments;
            orderHeader.OPPORTUNITY_NUM = orderHeaderCreate.OpportunityNum;
            orderHeader.QUOTE_NUM = orderHeaderCreate.Quote;
            orderHeader.SCORE = orderHeaderCreate.OrderScore;
            orderHeader.REQ_DATE_H = reqDelDate;
            orderHeader.OPPORTUNITY_NAME = orderHeaderCreate.OpportunityName;
        
            var orderItems = new Array();
            var requestLength = orderItemsCreate.length;
            for (var x = 0; x < requestLength; x++)
            {
             var orderItem = new Object();  
                orderItem.MATERIAL    = orderItemsCreate[x].Material;
                orderItem.TARGET_QTY  = orderItemsCreate[x].Quantity;
                orderItem.TARGET_QU = orderItemsCreate[x].SalesUnit;
                
                orderItem.PRICE_REQUEST  = orderItemsCreate[x].PRICE_REQUEST;
                orderItem.PRICE_REQUEST_ITM = orderItemsCreate[x].PRICE_REQUEST_ITM;
                
                orderItems.push(orderItem);
            }
        
			var orderCreateContent =  { 
                ORDER_HEADER: orderHeader,
                ORDER_ITEMS: orderItems,
            };
                 

            var config = { headers: {
                "X-Requested-With": "X",
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json" } };

            $http.post(requestURL, orderCreateContent, config)
                .success(function (data, status, headers, config) 
                    {
                        showMessage('Info', 'Sales Order created: ' + data.DOC_NUMBER);
                        var requestCreated = data.DOC_NUMBER;
                        AppService.synchCustomerList().then(function(d) {//update and wait for order details
                            angular.element('#loaderDiv').hide();
                            AppService.synchPriceRequestList(); //trigger price request update
                            $state.go('app.orderDetail', {id: requestCreated});
                        });

                    })
                .error(function (error, status, header, config) 
                    {
                        angular.element('#loaderDiv').hide();
                        showMessage('Error', 'An error occurred, please review your order and retry');
                    });
    }
});