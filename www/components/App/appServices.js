angular.module("app")
.service('AppService', function($http, $rootScope, $q, $location, $localStorage) {

    // easyPrice Application Services
		// variables for the current role
		var currentUserRole;
		var currentUser;
		var userProfile = new Object();
		
		// this will load configuration and key master data from local storage.	
		this.loadConfiguration = function loadConfiguration()
		{            
            var fullUrl = $location.$$absUrl;
            var employee = getParameterByName('employee', fullUrl); 
            if (employee)
                {
                $rootScope.salesRep = employee;
                $localStorage.salesRep = employee;
                }
            
            $rootScope.plantId = $localStorage.plantId; 
            
            if ($localStorage.daysHistory)
                {
                    $rootScope.daysHistory = $localStorage.daysHistory;
                }


		}
        				
		// this will be used to determine what roles and permissions are available in the application.
		this.getRolePermissions = function getRolePermissions(userRole)
		{

			
		}
		
		// gets the current user for the application.
		this.getCurrentUser = function getCurrentUser()
		{
			return currentUser;	
		}

		this.getCurrentUserProfile = function getCurrentUserProfile()
		{
			return userProfile;	
		}
		
		// gets the current user role.
		this.getCurrentUserRole = function getCurrentUserRole()
		{
			return currentUserRole;	
		}

		this.parseJsonDate = function parseJsonDate(jsonDate) {
			var JsDate = parseJsonDatetoJsDate(jsonDate);
			return JsDate;
		}
		
		var parseJsonDatetoJsDate = function parseJsonDatetoJsDate(jsonDate)
		{
				var d = new Date(parseInt(jsonDate.substr(6)));
				var offset = new Date().getTimezoneOffset(); //we trust that the date sent is in the local user timezone.
				d.setMinutes(d.getMinutes() + offset);
				return d;					
		}


		this.parseJsonDateToString = function parseJsonDateToString(jsonDate) 
		{
			
			if (!jsonDate)
			{
			 return; //if there is no date, then return.	
			}
			
			var jsDate = parseJsonDatetoJsDate(jsonDate);
			var jsDateString = ('0' + (jsDate.getMonth()+1)).slice(-2) + '/'
								 + ('0' + jsDate.getDate()).slice(-2) + '/' +
								(jsDate.getFullYear());
			return jsDateString;				 			
		}
		
		this.parseStringDateToJsonDate = function parseStringDateToJsonDate(inputDate)
		{
            // need to add leading zeros to dates... i.e. 8-1-2014 needs to be 08-01-2014
			var startDateFormatted = inputDate;
			var startDate = new Date(startDateFormatted);
			var docDate = (startDate.getFullYear()) +  '-'
            		 + ('0' + (startDate.getMonth()+1)).slice(-2) + '-'
            		 + ('0' + startDate.getDate()).slice(-2);
	
			docDate = docDate + "T12:00"; //add time suffix	
			return docDate;
		}
	
		this.parseJSdateToSAPDate = function parseJSdateToSAPDate(inputDate)
		{
            // need to add leading zeros to dates... i.e. 8-1-2014 needs to be 08-01-2014
			var startDateFormatted = inputDate;
			var startDate = new Date(startDateFormatted);
			var docDate = (startDate.getFullYear()) +  '-'
            		 + ('0' + (startDate.getMonth()+1)).slice(-2) + '-'
            		 + ('0' + startDate.getDate()).slice(-2);
			return docDate;
		}
        
  // this function will determine what server the app should connect with.
	  this.determineServer = function determineServer()
	  {
            // default to BSP that it is currently running on
			  serverURL = window.location.protocol +'//' + location.hostname + ':' + location.port;
          
            // override for special occassions
			  if (window.location.protocol == "file:") //for file based testing, revert to DEV
			  {
				serverURL = 'http://access.wftcloud.com:8059';		  
			  }
	
			  if (window.location.host == "http://sapps300.madixinc.com:8000") //for dev testing
			  {	
				  serverURL = 'http://sapps300.madixinc.com:8000';	  
			  }
          
			  if (window.location.host == "127.0.0.1:8887") //for local based testing, revert to DEV
			  {	
				  serverURL = 'http://sapps300.madixinc.com:8000';	  
			  }
          			  
              $rootScope.serverURL = serverURL; //copy to parent scope.
					  
			 return serverURL;
	  }

	  this.determineClient = function determineClient()
	  {
			  if ($rootScope.serverURL == 'http://sapps300.madixinc.com:8000')
			  {
				$rootScope.client = '120';		  
			  }
	   
              else //client 800
			  {	
				$rootScope.client = '120';	
			  }
					  
			 return $rootScope.client;
	  }

    // this will show a confirmation popup to the user
  this.showConfirmation = function showConfirmation(message)
  { 
     $("#alertMessage").html(message);
     $('#btnMessagePopup')[0].click();
  }

    var leadingZeros = function leadingZeros(num, size) {
        var s = "0000000000" + num;
        return s.substr(s.length-size);
    }

    this.addLeadingZeros = function addLeadingZeros(num, size)
    {
        var s = "0000000000" + num;
        return s.substr(s.length-size);			
    }
		
  // this function will show the user profile information
  this.showProfile = function showProfile()
  {
      $('#viewUserProfileButton')[0].click();
  } 
  
    // this will get a list of the users...
	// get request with parameters	
	this.synchUserList = function synchUserList()
	{        
        return;
	}
    
    var materialArray = [];
    
// this is responsible for Synching the main data for the application.    
	this.synchCustomerList = function synchCustomerList()
	{
        
	}
    
    // this will synchronize with the SAP price request list for the given user.
	this.synchPriceRequestList = function synchPriceRequestList()
	{

	}
    
       
    // this will synchronize with the C4C opportunity list for the given user.
	this.synchOpportunityList = function synchOpportunityList()
	{
        
	}
    
    var buildOpportunityList = function()
    {
        $rootScope.opportunityListOpen = [];
        $rootScope.opportunityListInProcess = [];

        $rootScope.opportunityListValue = 0;
        $rootScope.opportunityListOpenValue = 0;
        $rootScope.opportunityListInProcessValue = 0;

        var filteredOpportunityList = [];
        
        var length = $rootScope.opportunityList.length;
        for (var x = 0; x<$rootScope.opportunityList.length; x++)
            {
                // we need to summarize the weighted value of these opportunities to calculate the
                // cockpit value.
                  var weightedValue = parseFloat($rootScope.opportunityList[x].WEIGHTED_VALUE);

                  $rootScope.opportunityListValue += weightedValue;
                
                  if ($rootScope.opportunityList[x].STATUSCODE == '1') //open
                        {
                            $rootScope.opportunityListOpen.push($rootScope.opportunityList[x]);
                            filteredOpportunityList.push($rootScope.opportunityList[x]);
                            $rootScope.opportunityListOpenValue += weightedValue;
                        }
                
                  else if ($rootScope.opportunityList[x].STATUSCODE == '2') //in process
                        {
                            $rootScope.opportunityListInProcess.push($rootScope.opportunityList[x]);
                            filteredOpportunityList.push($rootScope.opportunityList[x]);
                            $rootScope.opportunityListInProcessValue += weightedValue;
                        }
            }
        
        // override opportunity list data.
        $rootScope.opportunityList = filteredOpportunityList;
        
    }
    
    /*var buildOpportunityList = function()
    {
        $rootScope.opportunityListOpen = [];
        $rootScope.opportunityListInProcess = [];

        $rootScope.opportunityListValue = 0;
        $rootScope.opportunityListOpenValue = 0;
        $rootScope.opportunityListInProcessValue = 0;

        var filteredOpportunityList = [];
        
        var length = $rootScope.opportunityList.length;
        for (var x = 0; x<$rootScope.opportunityList.length; x++)
            {
                // we need to summarize the weighted value of these opportunities to calculate the
                // cockpit value.
                  var weightedValue = parseFloat($rootScope.opportunityList[x].WeightedValue.content);

                  $rootScope.opportunityListValue += weightedValue;
                
                  if ($rootScope.opportunityList[x].StatusCode == '1') //open
                        {
                            $rootScope.opportunityListOpen.push($rootScope.opportunityList[x]);
                            filteredOpportunityList.push($rootScope.opportunityList[x]);
                            $rootScope.opportunityListOpenValue += weightedValue;
                        }
                
                  else if ($rootScope.opportunityList[x].StatusCode == '2') //in process
                        {
                            $rootScope.opportunityListInProcess.push($rootScope.opportunityList[x]);
                            filteredOpportunityList.push($rootScope.opportunityList[x]);
                            $rootScope.opportunityListInProcessValue += weightedValue;
                        }
            }
        
        $rootScope.opportunityList = filteredOpportunityList;
        
    }*/
    
	this.synchMaterialList = function synchMaterialList()
	{
        

	}

    this.countMetrics = function countMetrics()
        {

		}    
    
	this.synchMessages = function synchMessages()
	{
		//var serverURL = $rootScope.serverURL;
		//var serviceUri = serverURL + "/sap/opu/odata/sap/" + $rootScope.userServiceName + "/UserSet?$format=json&$filter=Userid eq 'SY-UNAME'&$expand=To_AltBases"
        /* not currently used
			var serverURL = "http://h1lp00.nexeosolutions.com:8000";
			var serviceUri = serverURL + "/sap/bc/zhr_easytime/ZHR_TIMEMGT_MESSAGE_READ";
			var requestURL = serviceUri;

		var promise = $http.get(serviceUri).then(function (response) {				
				$rootScope.messageTitle = response.data.TITLE;
				$rootScope.messageText = response.data.MESSAGE;
			   
			  });
		 // Return the promise to the controller
		 return promise;
         */
	}

		
	this.synchUserProfile = function synchUserProfile()
	{
        // not currently used
		/*var serverURL = $rootScope.serverURL;
		var serviceUri = serverURL + "/sap/opu/odata/sap/" + $rootScope.userServiceName + "/UserSet?$format=json&$filter=Userid eq 'SY-UNAME'&$expand=To_AltBases"
		var promise = $http.get(serviceUri).then(function (response) {
				// The then function here is an opportunity to modify the response
				//console.log(response);
				userProfile = response.data.d.results[0];
				$rootScope.userProfile = userProfile;
				$rootScope.currentUser = userProfile.Userid;
				return userProfile;
			   
			  });
		 // Return the promise to the controller
		 return promise;*/
	}
    
});


// numeric formatter
function nFormatter(num) {
     if (num >= 1000000000) {
        return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
     }
     if (num >= 1000000) {
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
     }
     if (num >= 1000) {
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
     }
     return num;
}