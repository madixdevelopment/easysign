'use strict';

// Declare app level module which depends on filters, and services
angular.module('FracAsset', [
  'ngRoute',
  'ngResource',
  "highcharts-ng", 
  "ui.bootstrap",
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers'
])
.controller("InitController", function($scope, $http, AppService, EquipmentService, RequisitionService, QuoteService, OrderService, DeliveryService, ShippingDocService) {
				$scope.userProfile = new Object();
			
				// call the user profile synch and then use it to retrieve the other relevant data for the application.
	   	  		AppService.synchUserProfile().then(function(d) {

					$scope.userProfile = d;

					if ($scope.userProfile)
				   	{
					 $("#usernameField").html('Welcome ' + $scope.userProfile.Userid);
					 $("#UseridProfile").html($scope.userProfile.Userid);
					 $("#RoleProfile").html($scope.userProfile.Role);
					 $("#PrimaryBaseProfile").html($scope.userProfile.PrimaryBase);
	
					//set variable for user id... this is a global variable
					window.currentUser = $scope.userProfile.Userid;
					window.currentUserRole = $scope.userProfile.Role;
				   }
				   
					// synch all data at the start of the app.
					AppService.showPleaseWait();
					AppService.hidePleaseWait();
	  			});
				
				AppService.addEventListeners();
				
	})
	
// now use this to control all of the routes in the application
.config(['$routeProvider', function($routeProvider) {
		
	  $routeProvider.when('/cockpit', {templateUrl: 'components/AssetCockpit/assetCockpit.html'});

	  $routeProvider.when('/scheduleList', {templateUrl: 'components/Scheduling/scheduleList.html'});	  
	  $routeProvider.when('/schedule', {templateUrl: 'components/Scheduling/scheduling.html'});
	  
	  $routeProvider.when('/administration', {templateUrl: 'components/Admin/userList.html'});
	  
	  $routeProvider.when('/equipment', {templateUrl: 'components/Equipment/equipmentList.html'});
	  $routeProvider.when('/equipment/:id', {templateUrl: 'components/Equipment/equipmentDetail.html'});
	  $routeProvider.when('/equipmentEdit/:id', {templateUrl: 'components/Equipment/equipmentEdit.html'});

	  $routeProvider.when('/requisitions', {templateUrl: 'components/Requisitions/requisitionList.html'});
	  $routeProvider.when('/requisitionAdd', {templateUrl: 'components/Requisitions/requisitionCreate.html'});
	  $routeProvider.when('/requisitions/:id', {templateUrl: 'components/Requisitions/requisitionDetail.html'});
	  $routeProvider.when('/requisitionEdit/:id', {templateUrl: 'components/Requisitions/requisitionEdit.html'});

	  $routeProvider.when('/shippingDocList', {templateUrl: 'components/ShippingDocs/shippingDocList.html'});
	  	 
	  $routeProvider.when('/orders', {templateUrl: 'components/Orders/orderList.html'});
	  $routeProvider.when('/orders/:id', {templateUrl: 'components/Orders/orderDetail.html'});

	  $routeProvider.when('/deliveries', {templateUrl: 'components/Deliveries/deliveryList.html'});
	  
	  $routeProvider.when('/quotes', {templateUrl: 'components/Quotes/quoteList.html'});
	  $routeProvider.when('/quotes/:id', {templateUrl: 'components/Quotes/quoteDetail.html'});
	  
	  $routeProvider.otherwise({redirectTo: '/cockpit'});
  
}]);
  