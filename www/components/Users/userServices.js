angular.module("app")
.service('UserService', function($http, $rootScope, $localStorage, $state, AppService) {

		var currentUserEdit = new Object();
		var userAltBases = new Array();
		var userAltBasesOriginal = new Array();
		var baseListSelected = new Array();
		
		this.updateOriginals = function updateOriginals(userAltBases)
		{
			userAltBasesOriginal = angular.copy(userAltBases);			
		}
				
		this.updateCreateFields = function updateCreateFields(userData, selectedBases)
		{
			currentUserEdit = userData;
			userAltBases = selectedBases; 
		}

	// get request with parameters	
	this.synchAgentList = function synchAgentList()
	{
			var serverURL = AppService.determineServer();
			var serviceUri = serverURL + "/sap/bc/zmm_fleet_mgmt/ZMM_FLEET_AGENT_LIST?format=json&?sap-client=020";
			
			var requestURL = serviceUri;

		var promise = $http.get(serviceUri).then(function (response) {	
				$rootScope.agentList	= response.data.TBL_AGENTS;		
				$localStorage.agentList = $rootScope.agentList;
				return $rootScope.agentList;
			  });
		 // Return the promise to the controller
		 return promise;
	}

	// saves a time entry to SAP
	this.saveUserUpdate = function saveUserUpdate(userProfileUpdated)
		{
			//var serverURL = $rootScope.serverURL;
			var serverURL = AppService.determineServer();
			var serviceUri = serverURL + "/sap/bc/zmm_fleet_mgmt/ZMM_FLEET_USER_UPDATE";
			
			var requestURL = serviceUri;
			
			var updateContent = {
				USERINFO: userProfileUpdated
			};
			
		var request = { headers: {
			"X-Requested-With": "XMLHttpRequest",
			"Content-Type": "application/json; charset=utf-8",
			"DataServiceVersion": "2.0",
			"Accept": "application/json" },
			//"X-CSRF-Token": header_xcsrf_token },
			
		requestUri: requestURL,
		method: "POST", //for add
		data: updateContent };
		OData.request(
					  request,
					  function (data, response) { //successfully added the requisition
						var responseJSON = JSON.parse(response.body);
						showMessage('Success',  'User saved successfully.  Your updated shipment list will refresh in a moment.');
		
					  },
					  function (err) {
					  //Error Callback:
					  var errorJSON = JSON.parse(err.response.body);
						showMessage('Error',  errorJSON.error.message.value);					  
						angular.element('#loaderDiv').hide();
					  }
					  );
		}

	// get request with parameters	
	this.synchUsers = function synchUsers()
	{

			var serverURL = AppService.determineServer();
            //serverURL = 'http://gcseccdbd.ecolab.com:8001';   &sap-client=180
			var serviceUri = serverURL + "/sap/bc/zequip_care/ZMM_MATERIAL_USER_DETAILS?format=json";
			
			var requestURL = serviceUri;

		var promise = $http.get(serviceUri).then(function (response) {	
				$rootScope.userProfile	= response.data.USERINFO;	
				$localStorage.userProfile = $rootScope.userProfile;	
				return $rootScope.userProfile;
			  });
              
		 // Return the promise to the controller
		 return promise;
	}			
	
	// get request with parameters	
	this.synchPlantList = function synchPlantList()
	{

			var serverURL = AppService.determineServer();
            //serverURL = 'http://gcseccdbd.ecolab.com:8001';   &sap-client=180
			var serviceUri = serverURL + "/sap/bc/zequip_care/ZMM_MATERIAL_USER_DETAILS?format=json";
			
			var requestURL = serviceUri;

		var promise = $http.get(serviceUri).then(function (response) {	
				$rootScope.userProfile	= response.data.USERINFO;	
				$localStorage.userProfile = $rootScope.userProfile;	
				return $rootScope.userProfile;
			  });
              
		 // Return the promise to the controller
		 return promise;
	}

});