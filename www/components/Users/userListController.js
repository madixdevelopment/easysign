angular.module("app")
.config(function($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
})
.controller("userEditCtrl", function ($scope, $http, $stateParams, $rootScope, UserService, AppService) {

})
.controller("userDetailCtrl", function ($scope, $http, $stateParams, $state, $rootScope, $localStorage, UserService, AppService, ShipmentService) {

		$scope.plantId; 
        $scope.daysHistory; 
        $scope.shipment = {};

		//read details based on the user selected
		$scope.getUser = function getUser() {
			
            if ($rootScope.userProfile)
                {
			     $scope.plantId = $rootScope.plantId; 
                }
            
            $scope.daysHistory = $rootScope.daysHistory;
    	}

        $scope.saveUser = function saveUser()
        {
            $rootScope.plantId = $scope.plantId;
            $localStorage.plantId = $rootScope.plantId;
            
            $rootScope.daysHistory = $scope.daysHistory;
            $localStorage.daysHistory = parseInt($scope.daysHistory);
            
            showMessage('Info', 'Profile updated, reloading data');
            //$state.go('app.cockpit');
            angular.element('#loaderDiv').show();
            ShipmentService.synchShipmentList().then(function(d) {
                $state.go('app.shipmentList');
                angular.element('#loaderDiv').hide();
              }).catch(function(error) {
                showMessage('Error', 'easySign cannot connect with Madix SAP, please check your connection and try again.'); 
                angular.element('#loaderDiv').hide();
              });
        }
        
		$scope.showCancelShipmentCheckIn = function()
		{
			$('#mainScreen').hide();	
			$('#cancelCheckInScreen').show(200);
		}
        
		$scope.hideCancelShipmentCheckIn = function()
		{
			$('#mainScreen').show(200);
			$('#cancelCheckInScreen').hide();			
		}
        
        $scope.cancelCheckIn = function cancelCheckIn()
        {
              ShipmentService.cancelShipmentCheckIn($scope.shipment).then(function(d){
                    if (d.type == 'S') //if it was a success, go to the shipment list
                    {
                        ShipmentService.synchShipmentList().then(function(d) {
                            $state.go('app.shipmentList', {id: 'complete'});    
                        angular.element('#loaderDiv').hide();
                        });
                     }
              });
        }
        
        $('#cancelCheckInScreen').hide();
        

        showMessage('Info', 'Please enter your SAP Plant number and save');
        $scope.getUser();
    
    
});
