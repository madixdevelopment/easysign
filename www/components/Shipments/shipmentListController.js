// pricing controller
// change history
// 1/26/2017 - initial
angular.module("app")
.config(function($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
})
.controller("shipmentCreateCtrl", function ($scope, $http, $stateParams,  $state, $rootScope, AppService, ShipmentService) {
	    
})
.controller("shipmentEditCtrl", function ($scope, $http, $stateParams,  $state, $rootScope, AppService, ShipmentService) {
	    
})
.controller("shipmentDetailCtrl", function ($scope, $http, $stateParams, $state, $translate, ShipmentService) {
	
        var canvas;
        var signaturePad;
    
		$scope.shipment = new Object();
		$scope.shipmentDetails = new Array(); // will hold the shipment items
		$scope.item = {};
		$scope.request_id = $stateParams.id; //$routeParams.id;
        
        $scope.shipTo = {};
        $scope.soldTo = {};

		//read details based on the shipment selected
		$scope.getShipment = function (id) {
			
			$scope.shipmentDetails = ShipmentService.getShipment(id);
            $scope.shipment = $scope.shipmentDetails[0];
            
            for (var x=0; x< $scope.shipmentDetails.length; x++)
                {
                    $scope.shipmentDetails[x].ItemNumber = $scope.shipmentDetails[x].ITM_NUMBER;
                    var itemNbr = {}; //object.
                    itemNbr.id = $scope.shipmentDetails[x].ItemNumber;
                }
    	}
        

    // logic to validate action
		$scope.validateCheckIn = function validateCheckIn()
		{
            
            // check that all mandatory fields are filled
            if ($scope.shipment.COMPANY == "")
                {
                    
                  $translate('app.shipmentDetail.COMPANYERROR').then(function (translation) {
                    showMessage('Error', translation);
                  }, function (translationId) {
                    showMessage('Error', translationId);
                  });
                    
                    return false;
                }
            
            if ($scope.shipment.NAME == "")
                {
                  $translate('app.shipmentDetail.NAMEERROR').then(function (translation) {
                    showMessage('Error', translation);
                  }, function (translationId) {
                    showMessage('Error', translationId);
                  });
                    return false;
                }
            
            if ($scope.shipment.TELEPHONE == "")
                {
                  $translate('app.shipmentDetail.PHONEERROR').then(function (translation) {
                    showMessage('Error', translation);
                  }, function (translationId) {
                    showMessage('Error', translationId);
                  });
                    return false;
                }
            
            if ($scope.shipment.TRUCK == "")
                {
                  $translate('app.shipmentDetail.TRUCKERROR').then(function (translation) {
                    showMessage('Error', translation);
                  }, function (translationId) {
                    showMessage('Error', translationId);
                  });
                    return false;
                }
            
            if ($scope.shipment.TRAILER == "")
                {
                    
                  $translate('app.shipmentDetail.TRAILERERROR').then(function (translation) {
                    showMessage('Error', translation);
                  }, function (translationId) {
                    showMessage('Error', translationId);
                  });
                    return false;
                }
            
			return true; // if we are here then all is good			
		}
		 
        $scope.clearCanvas = function() {
                signaturePad.clear();
        }
              
        $scope.setIsSigned = function()
        {
            $scope.isSigned = true;
        }
        
        $scope.saveSignature = function()
        {
            // Returns true if canvas is empty, otherwise returns false
            var isSignatureEmpty = signaturePad.isEmpty();
            if (isSignatureEmpty == true)
                {
                    $scope.isSigned = false;
                }
            else
                {
                    $scope.isSigned = true;
                }
            
          // check to see if the data has been completed
            if ($scope.isSigned == false)
            {
                //showMessage('Error', 'Please sign the document at the bottom of the page');
                
                  $translate('app.shipmentDetail.SIGNERROR').then(function (translation) {
                    showMessage('Error', translation);
                  }, function (translationId) {
                    showMessage('Error', translationId);
                  });
                
                return;
            }
            
            $scope.getSignatureOnly(); //kicks off the process to save the document.
            // first we will take a snapshot of the signature pane, and then we take a whole screenshot 
        }
            
        // gets the base64 encoded version of the signature
        $scope.getSignatureOnly = function()
        {
            var signatureOnly = document.getElementById("signatureOnlyElement");
            html2canvas(signatureOnly, {
              onrendered: function(canvas) {
                var sigImg = canvas.toDataURL();
                $scope.signatureOnly = sigImg;
                $scope.saveCanvas(); // document.body.appendChild(canvas);

              }
            });                        
        }
            
        $scope.saveDataUpdates = function (document_to_update) {
             console.log('saving data updates');
             DocumentService.updateDocument(document_to_update).then(function (data){
               console.log('saved successfully');
                $ionicLoading.hide();
                $scope.showAlert('Success', 'Signature Saved');
                $scope.buildDocumentList($rootScope.documentList);
                $scope.getAllDocuments();                     
                $state.go('app.list');
            },
             function (error)
             {
                $ionicLoading.hide();                                                    
             });   
        }
             
        var imageData;
        $scope.isSigned = false;
        $scope.saveCanvas = function() {
                
                //show loading...
                   //$ionicLoading.show({template: 'Saving...'});
                
                // render screenshot
                        var detailPageElement = document.getElementById("shipmentPrintScreen");
                        html2canvas(detailPageElement, {
                            onrendered: function(canvas) {
                                // canvas is the final rendered <canvas> element
                                console.log('rendered');
                                //document.body.appendChild(canvas);
                                var sigImg = canvas.toDataURL();
                                $scope.signature = sigImg;
                                
                                var base64  = sigImg.replace("data:image/png;base64,", "");
                                var fileObject = new Object();
                                fileObject.base64 = base64;
                                fileObject.Filename = "signature.jpg";
                                fileObject.ContentType = "img/jpg";
                                var name = "signature.jpg";
                                 
                                var date = new Date();     
                                imageData = base64;

                                  //var pdf = new jsPDF();
                                  //pdf.addImage(imageData, 'JPEG', 0, 0);
                                
                                var pdf = new jsPDF('p','pt','a4');

                                pdf.addHTML(detailPageElement,function() {
                                    console.log('added html');
                                    var pdfData = pdf.output('datauri');
                                    pdfData = pdfData.replace('data:application/pdf;base64,',''); //strip off header
                                    imageData = pdfData;
                                    $scope.checkInComplete();
                                });
                                
                                // sap call
                                //$scope.checkInComplete();
                        
                    }
                });
                
            }
            
 
        $scope.getTimeStamp = function()
        {
            var date = new Date();
            var signatureDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();       
            var signatureTime = ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' +  ('0' + date.getSeconds()).slice(-2);
                if (!$scope.shipment)
                    {
                        $scope.shipment = {};
                    }
         $scope.shipment.SIGNATURE_DATE = signatureDate;
         $scope.shipment.SIGNATURE_TIME = signatureTime;   
        }
        
        // this will show the print confirmation for the driver signature.
        $scope.checkIn = function()
        {
            // verify the user populated the fields   
			var isValid = false;
			isValid = $scope.validateCheckIn($scope.shipment)
			
			if (isValid == true)
				{
                    $scope.getTimeStamp();   
                    $scope.showShipmentPrint();
                }
        }
        
        
        
        $scope.checkInComplete = function checkInComplete()
        {
            
			var isValid = false;
			isValid = $scope.validateCheckIn($scope.shipment)
			
			if (isValid == true)
				{
                        if (imageData)
                            {
                                $scope.shipment.SIGNATURE_DOCUMENT = imageData;  
                            }
                      ShipmentService.saveShipmentCheckIn($scope.shipment).then(function(d){
                            ShipmentService.synchShipmentList().then(function(d) {
                                $state.go('app.shipmentList', {id: 'complete'});
                                angular.element('#loaderDiv').hide();
                            });
                      });
				}
        }
        
		$scope.showShipmentPrint = function()
		{
			$('#createScreen').hide();	
			$('#shipmentPrintScreen').show(300);
		}
        
		$scope.hideShipmentPrint = function()
		{
			$('#createScreen').show(300);
			$('#shipmentPrintScreen').hide();			
		}
        
		// call function to get the price details.
		if ($scope.request_id)
		{            
			$scope.getShipment($scope.request_id);
            var canvas = document.getElementById('signatureCanvas');
            var signaturePad = new SignaturePad(canvas);
		}

        $scope.hideShipmentPrint();
    
})

.controller("shipmentListCtrl", function ($scope, $http, $state, $timeout, $stateParams, $rootScope, AppService, ShipmentService) {
    
    $scope.listType = $stateParams.id;
    $scope.app.shipmentListFiltered = new Array(); 
    
    if ($rootScope.plantId == null)
        {
            $state.go('app.profile');
        }
    
    $scope.filterShipments = function()
    {
        var shipments = new Array();
        
             if ($scope.listType == 'submitted')
             {
                var listLength = $rootScope.shipmentList.length;
                for (x = 0; x < listLength; x++)
                {
                        if ('OPEN' == $rootScope.shipmentList[x].STATUS)
                        {
                            shipments.push($rootScope.shipmentList[x]);
                        }  
                }  
                return shipments;
             }
    }
    
    $scope.clearSearch = function() {
        $scope.search = '';
        
    }
    
    $scope.listShipments = function () {
        // synch if needed.				
        if ($rootScope.shipmentList.length == 0)
        {
            ShipmentService.synchShipmentList().then(function(d) {
                $scope.app.shipmentListFiltered = $scope.filterShipments();
            });
        }
    }
        
   // initialize the controller    
   $scope.listShipments(); //call list to start the screen
    
    if (!$scope.listType)
    {
        $scope.listType = 'submitted';  
    }
    
    $scope.app.shipmentListFiltered = $scope.filterShipments();

    
});

// generic days function
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

var binarySearch = function(array, targetValue, fieldName) {
    var min = 0;
    var max = array.length - 1;
    var guess;
    while(min <= max) {
        guess = Math.floor((max + min) / 2);
        if (array[guess][fieldName] === targetValue) {
            return guess;
        }
        else if (array[guess][fieldName] < targetValue) {
            min = guess + 1;
        }
        else {
            max = guess - 1;
        }
    }
    return -1;
};

