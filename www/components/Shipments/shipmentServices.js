angular.module("app")
.service('ShipmentService', function($http, $state, $rootScope, AppService) {

		// variable to hold price items.		
		var shipmentList = new Array();
			
		var priceUrl = "";
        

		this.updateCreateFields = function updateCreateFields(priceHeader, priceItems)
		{
			priceHeaderCreate = priceHeader;
			priceItemsCreate = priceItems;
		}
		
		function saveData(results)
		{		
		}
		 
        // this will synchronize with the SAP price request list for the given user.
        this.synchShipmentList = function synchShipmentList()
        { 
            
            var fromDate = new Date();
            var daysHist = parseInt($rootScope.daysHistory) * -1;
            fromDate = addDays(fromDate, daysHist);
            
            var fromDateString = '';
            fromDateString = AppService.parseJSdateToSAPDate(fromDate);
            
            // create the server url
            var requestUri = '/refreshShipmentList' + '?LAST_REFRESH=' + $rootScope.lastRefresh + '&PLANT=' + $rootScope.plantId + '&FROM_DATE=' + fromDateString;
            
            // create the server url                
            //var serverURL = $rootScope.serverURL;  
		    //var requestUri = serverURL +  '/sap/bc/zsd_easysign/ZSD_EASYSIGN_SHIPMENT_LIST';
            
            var promise = $http.get(requestUri).then(function (response) {

                    if (response.data.body)
                        {
                        var body = JSON.parse(response.data.body);
                        $rootScope.shipmentList = body.TBL_SHIPMENT_LIST;
                        return $rootScope.shipmentList;
                        }
                    else
                        {
                            return;
                        }

                  });

             // Return the promise to the controller
             return promise;
        }

                             
        // this will save the shipment check in.
        this.saveShipmentCheckIn = function saveShipmentCheckIn(shipmentData)
        { 
            angular.element('#loaderDiv').show();

            //var serverURL = $rootScope.serverURL;       
            //var requestUri = serverURL + '/sap/bc/zsd_easysign/ZSD_EASYSIGN_SHIPMENT_UPDATE';
            //var requestURL = requestUri;
            
            var requestURL = '/saveShipmentCheckIn';
            
            // setup the data to post to the backend
            shipmentData.DCITY =shipmentData.EDORT01;
            shipmentData.DDATE =shipmentData.DPREG;
            shipmentData.DTIME =shipmentData.UPREG;
            shipmentData.DSTATE =shipmentData.REGIO;
            
            var shipmentContent = {
                SHIPMENT: shipmentData.TKNUM,
                CHECK_IN : shipmentData
            }

            var config = { headers: {
                "X-Requested-With": "X",
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json" } };
            

            var promise = $http.post(requestURL, shipmentContent, config).then(function (response) {

                    //$rootScope.shipmentList = response.data.TBL_SHIPMENT_LIST;

                    if (response.data.body)
                        {
                            var body = JSON.parse(response.data.body);

                            showMessage('Info', 'Shipment Checked In');
                        }
                    return response;
                    //return $rootScope.shipmentList;
                  });

             // Return the promise to the controller
             return promise;
        }
        
        // this will save the shipment check in.
        this.cancelShipmentCheckIn = function cancelShipmentCheckIn(shipmentData)
        { 
            angular.element('#loaderDiv').show();            
            var requestURL = '/cancelShipmentCheckIn';
                        
            var shipmentContent = {
                SHIPMENT: shipmentData.SHIPMENT,
                ADMIN_PASSWORD : shipmentData.ADMIN_PASSWORD
            }

            var config = { headers: {
                "X-Requested-With": "X",
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json" } };
            
            var promise = $http.post(requestURL, shipmentContent, config).then(function (response) {
                    if (response.data.body)
                        {
                            var body = JSON.parse(response.data.body);
                            if (body.RETURN.TYPE == 'S')
                                {
                                  showMessage('Info', 'Check In Cancelled'); 
                                    response.type = 'S';
                                }
                            else
                                {
                                showMessage('Error', body.RETURN.MESSAGE);
                                    response.type = 'E';
                                }
                            
                        }
                    return response;
                  });

             // Return the promise to the controller
             return promise;
        }
        
        
        this.uploadFile = function uploadFile(file)
        {
            var serverURL = $rootScope.serverURL;       
            var requestUri = serverURL + '/sap/bc/zsd_easysign/ZSD_EASYSIGN_FILE_UPLOAD';
            var requestURL = requestUri;
            
            var fd = new FormData();
            fd.append('file', file);
            $http.post(requestURL, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
            .success(function(result){
                alert('success', result)
            })
            .error(function(error){
                alert('error', error)
            });
        }

        // update the shipment
        this.updateShipment = function updateShipment(shipmentData)
            {
                 angular.element('#loaderDiv').show();

                var serverURL = $rootScope.serverURL;       
                var requestUri = serverURL + '/sap/bc/zsd_easysign/ZSD_EASYSIGN_SHIPMENT_UPDATE';
                var requestURL = requestUri;

                /*var tblShipmentUpdate = new Array();
                var requestLength = shipmentData.length;
                for (var x = 0; x < requestLength; x++)
                {
                 var priceRequestItem = new Object();  
                    priceRequestItem.PRICE_REQUEST = priceRequestData[x].PRICE_REQUEST;
                    priceRequestItem.COMMENTS = priceRequestData[x].COMMENTS;                  

                    if (priceRequestItem.PRICE_REQUEST) //only send across those with a customer selected
                    {
                        tblPriceRequest.push(priceRequestItem);
                    }
                }*/
            shipmentData.DCITY =shipmentData.EDORT01;
            //shipmentData.STATE =shipmentData.EDORT01;
            shipmentData.DDATE =shipmentData.DPREG;
            
            var shipmentContent = {
                SHIPMENT: shipmentData.TKNUM,
                CHECK_IN : shipmentData
            }

            var config = { headers: {
                "X-Requested-With": "X",
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json" } };

             $http.post(requestURL, shipmentContent, config)
                .success(function (data, status, headers, config) 
                    {
                        showMessage('Info', 'Shipment Checked In: ' + data.TKNUM);
                        PriceService.synchShipmentList().then(function(d) {
                            $state.go('app.shipmentList', {id: 'complete'});
                            angular.element('#loaderDiv').hide();
                        });

                    })
                .error(function (error, status, header, config) 
                    {
                        angular.element('#loaderDiv').hide();
                        showMessage('Error', 'An error occurred ');
                    });
        }	
    
		this.synchshipments = function()
		{
			var promise = $http.get(priceUrl).then(function (response) {
					// The then function here is an opportunity to modify the response
					// The return value gets picked up by the then in the controller.
					priceList = response.data.d.results;
					saveData(priceList);
					return priceList;
				  });
			 // Return the promise to the controller
     		 return promise;
		}
						

		// this will return the price list
		this.getPriceList = function() { 
			return priceList;
		};
		
		//read details based on the shipment selected
		this.getShipment = function (id) {
			
			var shipmentObject = new Array();
			var reqLength = $rootScope.shipmentList.length;
						//now we have an price, we can load the details.
						for (var x = 0; x < reqLength; x++)
						{
							if ($rootScope.shipmentList[x].TKNUM == id)
							{
								shipmentObject.push($rootScope.shipmentList[x]); //copy to header structure
								//break; //exit out if we have the price
							}
						}
			return shipmentObject;
    	}
    
});

// generic days function
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}