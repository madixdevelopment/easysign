angular.module("app")
.service('AccountService', function($http, $state, $rootScope, $q, $localStorage, AppService) {

    
    // get request with parameters	
    this.synchAccountAnalytics = function synchAccountAnalytics(account)
    {          
        // create the server url
        var serverURL = $rootScope.serverURL;        
        var serviceUri = serverURL + "/sap/bc/zeasyprice/ZSD_EASYPRICE_SALES_DATA_GET?format=json" + '&CUSTOMER=' + account  + '&sap-client=' + $rootScope.client;
        var requestUri = serviceUri;
        
        // call the service
		var promise = $http.get(requestUri).then(function (response) {
				return response.data.STR_SALES_DATA;
              });
        
		 // Return the promise to the controller
		 return promise;
        
	}
        
    /* Not used currently
	this.synchAccountList = function synchAccountList()
	{
			var serverURL = $rootScope.serverURL;        
			var serviceUri = serverURL + "/sap/bc/zcms/ZCMS_IPDA_AGREEMENT_LIST?format=json";
			var requestURL = serviceUri;

			showLoading(); //show loading window

			var promise = $http.get(serviceUri).then(function (response) {				
				var dataLength = response.data.ET_AGREEMENT_LIST.length;
				var accountList = new Array();
				for (x = 0; x < dataLength; x++) //update data fields.
				{
					var object = new Object();
					object = response.data.ET_AGREEMENT_LIST[x];
				    accountList.push(object);
				}

				if (dataLength == 0)
				{
					showMessage('Info', response.data.RETURN.MESSAGE);
				}
				
				//$localStorage.altSubList = altSubList; //$rootScope.userList; //copy to local storage
				$rootScope.accountList = accountList; // copy to root scope as well.

				hideLoading(); //hide loading window.
				return $rootScope.accountList;
			  });
		 // Return the promise to the controller
		 return promise;
	}
    
 
    // get request with parameters	
        this.synchAccountDetails = function synchAccountDetails(account)
        {
			var serverURL = $rootScope.serverURL;     
			var serviceUri = serverURL + "/sap/bc/zcms/ZCMS_IPDA_AGREEMENT_READ?I_KNUMA_AG=" + account + "&format=json";
			var requestURL = serviceUri;

			showLoading(); //show loading window

			var promise = $http.get(serviceUri).then(function (response) {
                hideLoading();
				return response.data;
			  });
		 // Return the promise to the controller
		 return promise;
	}*/
	
});