angular.module("app")
.config(function($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
})
.controller("accountDetailCtrl", function ($scope, $http, $rootScope, $state, $stateParams, AccountService, PriceService) {
	
		$scope.account = new Object();	
		$scope.account_id = $stateParams.id; //$routeParams.id;
        $scope.accountOpportunities = [];
        $scope.accountOrders = [];
        $scope.accountQuotes = [];
        $scope.accountAnalytics = {};
    
		//read details based on the requisition selected
		$scope.getAccount = function (id) {
            
            // get the details
            for (var x=0; x<$rootScope.customerList.length; x++)
                {
                    if ($rootScope.customerList[x].CUSTOMER == $scope.account_id)
                        {
                            $scope.account = $rootScope.customerList[x];
                            break;
                        }
                }

            
            /*
            for (var x=0; x<$rootScope.orderList.length; x++)
                {
                    if ($rootScope.orderList[x].OPPORTUNITY_NUM == $scope.opportunity_id)
                        {
                            $scope.opportunityOrders.push($rootScope.orderList[x]);
                        }
                }*/
            
            // get orders
            $scope.totalSales = 0;
            $scope.accountOrders = [];
            for (var x=0; x<$rootScope.orderList.length; x++)
                {
                    if ($rootScope.orderList[x].SOLD_TO == $scope.account.CUSTOMER)
                        {
                            $scope.accountOrders.push($rootScope.orderList[x]);
                            $scope.totalSales = $scope.totalSales + parseFloat($rootScope.orderList[x].NET_VAL_HD);
                        }
                }
            $scope.totalSales = $scope.totalSales.toFixed(0);
            $scope.totalSales = nFormatter( $scope.totalSales);
            
            // get price quotes
            $scope.accountQuotes = [];
            for (var x=0; x<$rootScope.priceRequestList.length; x++)
                {
                    if ($rootScope.priceRequestList[x].CUSTOMER == $scope.account.CUSTOMER)
                        {
                            $scope.accountQuotes.push($rootScope.priceRequestList[x]);
                        }
                }
            
            // get opportunities
            $scope.accountOpportunities = [];
            for (var x=0; x<$rootScope.opportunityList.length; x++)
                {
                    if ($rootScope.opportunityList[x].ACCOUNTID == $scope.account.CUSTOMER)
                        {
                            $scope.accountOpportunities.push($rootScope.opportunityList[x]);
                        }
                }
            
            // read Business Objects Data
	   	  		AccountService.synchAccountAnalytics($scope.account.CUSTOMER).then(function(d) {
		  		  $scope.accountAnalytics = d;
                    console.log('received analytics');
	  			});
            
            
    	}
            
        // now we will launch the quote create screen
        $scope.createQuote = function createQuote()
        {
            $state.go('app.quoteCreate', {id: $scope.account_id});
        }

        // will need to work on this.
        $scope.createOrder = function()
        {
            $state.go('app.orderCreate', {id: $scope.account_id});
        }
        
		// call function to get the order details.
		if ($scope.account_id)
		{
			$scope.getAccount($scope.account_id);
		}

})
.controller("accountListCtrl", function ($scope, $rootScope, $http) {

    $scope.listAccounts = function () {
		
			/*// read from memory if nothing available.
			//if ($scope.app.orderList.length === 0)
			//	{
	   			$scope.app.orderList = OrderService.getOrderList();
			//	}

			// synch if needed.				
			if ($scope.app.orderList.length === 0)
			{
	   	  		OrderService.synchOrders().then(function(d) {
		  		$scope.app.orderList = d;
	  			});
			}*/
    }

//  functions to initialize the controller.
   $scope.listAccounts(); //call list to start the screen
	
});
