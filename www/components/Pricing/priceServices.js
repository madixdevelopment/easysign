angular.module("app")
.service('PriceService', function($http, $state, $rootScope, AppService) {

		// variable to hold price items.		
		var priceList = new Array();

		var priceHeaderUpdate = new Object();
		var priceItemsUpdate = new Array();
		var priceHeaderCreate = new Object();
		var priceItemsCreate = new Array();
		var priceHeaderOriginal = new Object();
		var priceItemsOriginal = new Array();
		var Cart = new Array();
			
		var priceUrl = "";
        

		this.updateCreateFields = function updateCreateFields(priceHeader, priceItems)
		{
			priceHeaderCreate = priceHeader;
			priceItemsCreate = priceItems;
		}
		
		function saveData(results)
		{		
		}
		 
		this.synchprices = function()
		{
			var promise = $http.get(priceUrl).then(function (response) {
					// The then function here is an opportunity to modify the response
					// The return value gets picked up by the then in the controller.
					priceList = response.data.d.results;
					saveData(priceList);
					return priceList;
				  });
			 // Return the promise to the controller
     		 return promise;
		}
						

		// this will return the price list
		this.getPriceList = function() { 
			return priceList;
		};
		
		//read details based on the price selected
		this.getPrice = function (id) {
			
			var priceObject = new Array();
			var reqLength = $rootScope.priceRequestDetail.length;
						//now we have an price, we can load the details.
						for (var x = 0; x < reqLength; x++)
						{
							if ($rootScope.priceRequestDetail[x].PRICE_REQUEST == id)
							{
								priceObject.push($rootScope.priceRequestDetail[x]); //copy to header structure
								//break; //exit out if we have the price
							}
						}
			return priceObject;
    	}
		
		//read details based on the price selected
		this.getOpportunityItems = function (id) {
			
			var priceObject = new Array();
			var reqLength = $rootScope.priceRequestDetail.length;
						//now we have an price, we can load the details.
						for (var x = 0; x < reqLength; x++)
						{
							if ($rootScope.priceRequestDetail[x].OPPORTUNITY_NUM == id)
							{
								priceObject.push($rootScope.priceRequestDetail[x]); //copy to header structure
							}
						}
			return priceObject;
    	}
        
		this.addItemsToCart = function addItemsToCart()
		{
				Cart.length = 0; // initialize cart.
				var itemNbr = 10;
			    $('#priceItems').find('tr').each(function () {
					
        			var row = $(this); //use this to get items that are selected
					var quantity = row.find('#quantity').val();
					quantity = parseInt(quantity);
					if (quantity > 0)
						{
							var cartRow = new Object();
							var qty = row.find('#quantity').val();
							 var material = row.find('#material').val();
							 var salesUnit = row.find('#salesUnit').val();
											
							cartRow.ItemNumber = itemNbr.toString();	
							cartRow.QuoteNumber = priceHeaderCreate.Quote;
							cartRow.Material = material; //'GV7IN15KH';
							cartRow.Quantity = qty;
							cartRow.Plant = priceHeaderCreate.Base;
							cartRow.SalesUnit = salesUnit;
							Cart.push(cartRow);
							itemNbr = itemNbr + 10;
			
       					 }
  			  	});
		}
        
    var buildOpportunityLists = function()
    {
        $rootScope.opportunityListOpen = [];
        $rootScope.opportunityListInProcess = [];

        $rootScope.opportunityListValue = 0;
        $rootScope.opportunityListOpenValue = 0;
        $rootScope.opportunityListInProcessValue = 0;

        
        var length = $rootScope.opportunityList.length;
        for (var x = 0; x<length; x++)
            {
                // we need to summarize the weighted value of these opportunities
                  var weightedValue = parseFloat($rootScope.opportunityList[x].WEIGHTED_VALUE);

                  $rootScope.opportunityListValue += weightedValue;
                
                  if ($rootScope.opportunityList[x].StatusCode == '1') //open
                        {
                            $rootScope.opportunityListOpen.push($rootScope.opportunityList[x]);
                            $rootScope.opportunityListOpenValue += weightedValue;
                        }
                
                  if ($rootScope.opportunityList[x].StatusCode == '2') //in process
                        {
                            $rootScope.opportunityListInProcess.push($rootScope.opportunityList[x]);
                            $rootScope.opportunityListInProcessValue += weightedValue;
                        }
            }
    }
	
    // get opportunities
    this.synchC4COldData = function synchC4COldData()
    {
        // replaced by synchOpportunityList in AppService
        /*// primesource dev = https://my308031.crm.ondemand.com
        // BENSONC Redsox123
        
        // michael mao
        $rootScope.c4cUrl = 'https://my308031.crm.ondemand.com';
        var requestUri = $rootScope.c4cUrl +
                        "/sap/c4c/odata/v1/c4codata/OpportunityCollection?$filter=OwnerID eq '8000000006'&$format=json";
        var req = {
             method: 'GET',
             url: requestUri,
             headers: {
               'Authorization': 'Basic QkVOU09OQzpSZWRzb3gxMjM='  //'BENSONC:Redsox123'
             }
            };
        
        var promise = $http(req).then(function (response) {
				// The then function here is an opportunity to modify the response
                console.log(response);
                $rootScope.opportunityList = response.data.d.results;
                buildOpportunityLists(); //build out the opportunity array data.
				return response;
			   
			  });
              
		 // Return the promise to the controller
		 return promise; */  
    }
    
    this.synchC4CAccounts = function synchC4CAccounts()
    {

    }
    
    this.simulateQuoteItem = function simulateQuoteItem(priceRequest)
    {
		    //var requestUri = 'https://eccdev.primesourcebp.com:44300/sap/bc/zeasyprice/ZSD_EASYORDER_QUOTE_SIMULATE';
                
            // create the server url        
            var serverURL = $rootScope.serverURL;   
			var serverURL = serverURL + "/sap/bc/zeasyprice/ZSD_EASYORDER_QUOTE_SIMULATE";
			var serviceUri = serverURL + "?CUSTOMER_NO=" + priceRequest.CUSTOMER + "&PART_NO=" + priceRequest.MATERIAL + "&ITEM_NBR=" + priceRequest.ItemNumber + "&format=json";
			var requestURL = serviceUri;
        
            // call the service
		      var promise = $http.get(serviceUri).then(function (response) {				
				return response.data;
			  });

		 // Return the promise to the controller
		 return promise;
    }
    

            
	this.savePriceCreateData = function savePriceCreateData(priceRequestData, effectiveDate, expirationDate, soldTo, soldToName)
		{
        
            angular.element('#loaderDiv').show();
        
            // create the server url                
            var serverURL = $rootScope.serverURL;  
		    var requestUri = serverURL +  '/sap/bc/zeasyprice/ZSD_EASY_SALES_PRICE_CREATE';
            var requestURL = requestUri;
 
            // SAP date format = YYYYMMDD
            var effectiveDateArray = effectiveDate.split('/');
            var effectiveDateSAP;
            effectiveDateSAP = effectiveDateArray[2] + effectiveDateArray[0] + effectiveDateArray[1];
        
            var expirationDateArray = expirationDate.split('/');
            var expirationDateSAP;
            expirationDateSAP = expirationDateArray[2] + expirationDateArray[0] + expirationDateArray[1];
          
            var tblPriceRequest = new Array();
            var requestLength = priceRequestData.length;
            for (var x = 0; x < requestLength; x++)
            {
             var priceRequestItem = new Object();  
                priceRequestItem.CUSTOMER = priceRequestData[x].CUSTOMER;
                priceRequestItem.MATERIAL = priceRequestData[x].MATERIAL;
                
                if (priceRequestItem.MATERIAL == "")
                    {
                        continue;
                    }
                priceRequestItem.NAME     = priceRequestData[x].CUSTOMER_NAME;
                priceRequestItem.DESCRIPTION = priceRequestData[x].DESCRIPTION;
                priceRequestItem.SALES_ORG = priceRequestData[x].SALES_ORG;
                priceRequestItem.DISTR_CHAN = priceRequestData[x].DISTR_CHAN;
                priceRequestItem.DIVISON = priceRequestData[x].DIVISION;
                priceRequestItem.REQUEST_STATUS = 'Complete'; //priceRequestData[x].REQUEST_STATUS;
                priceRequestItem.ITM_NUMBER = priceRequestData[x].ItemNumber;
                priceRequestItem.BID_PRICE   = priceRequestData[x].BID_PRICE;                
                priceRequestItem.BID_PRICE_UOM   = priceRequestData[x].BID_PRICE_UOM;               
                priceRequestItem.REQUESTED_BY = priceRequestData[x].REQUESTED_BY;
                priceRequestItem.REQUEST_TYPE = priceRequestData[x].REQUEST_TYPE;
                priceRequestItem.EFFECTIVE_DATE = effectiveDateSAP;
                priceRequestItem.EXPIRATION_DATE = expirationDateSAP;                
                priceRequestItem.REQUEST_DATE = priceRequestData[x].REQUEST_DATE;  
                priceRequestItem.COMMENTS = priceRequestData[x].COMMENTS;                  
                priceRequestItem.QUOTE_NUM = '';
                priceRequestItem.COMMISSION = priceRequestData[x].COMMISSION;  
                priceRequestItem.STRETCH_PRICE = priceRequestData[x].STRETCH_PRICE;   
                priceRequestItem.EXPECTED_REVENUE = priceRequestData[x].EXPECTED_REVENUE;   
                priceRequestItem.SHIP_TO = priceRequestData[x].CUSTOMER;
                priceRequestItem.SHIP_TO_NAME = priceRequestData[x].CUSTOMER_NAME;
                priceRequestItem.SOLD_TO = priceRequestData[x].SOLD_TO;
                priceRequestItem.SOLD_TO_NAME = priceRequestData[x].SOLD_TO_NAME;
                priceRequestItem.CUSTOMER_CITY = priceRequestData[x].CUSTOMER_CITY;
                priceRequestItem.CUSTOMER_REGION = priceRequestData[x].CUSTOMER_REGION;
                priceRequestItem.PACK_TYPE = priceRequestData[x].PACK_TYPE;
                priceRequestItem.SHIPMENT_MODE = priceRequestData[x].SHIPMENT_MODE;
                priceRequestItem.TARGET_PRICE   = priceRequestData[x].TARGET_PRICE; 
                priceRequestItem.TARGET_PRICE_INDEX   = priceRequestData[x].TARGET_PRICE_INDEX;   
                priceRequestItem.TARGET_PRICE_UNI   = priceRequestData[x].TARGET_PRICE_UNI;   
                priceRequestItem.TARGET_PRICE_UOM   = priceRequestData[x].TARGET_PRICE_UOM; 
                priceRequestItem.TARGET_PRICE_CUR   = priceRequestData[x].TARGET_PRICE_CUR;   
                priceRequestItem.EXCEPTION1_PRICE   = priceRequestData[x].EXCEPTION1_PRICE; 
                priceRequestItem.EXCEPTION2_PRICE   = priceRequestData[x].EXCEPTION2_PRICE; 
                priceRequestItem.TPI_CLASS          = priceRequestData[x].TPI_CLASS; 
                priceRequestItem.CSP_PRICE   = priceRequestData[x].CSP_PRICE;   
                priceRequestItem.CSP_PRICE_UNIT   = priceRequestData[x].CSP_PRICE_UNIT;   
                priceRequestItem.CSP_UOM   = priceRequestData[x].CSP_UOM;   
                priceRequestItem.CSP_CURRENCY   = priceRequestData[x].CSP_CURRENCY;   
                priceRequestItem.CSP_EFF_DATE   = priceRequestData[x].CSP_EFF_DATE;   
                priceRequestItem.CSP_EXP_DATE   = priceRequestData[x].CSP_EXP_DATE;   
                priceRequestItem.CSP_CONDITION_NB   = priceRequestData[x].CSP_CONDITION_NB; 
                priceRequestItem.HEADER_TEXT   = priceRequestData[x].HEADER_TEXT;   
                priceRequestItem.QUOTE_SCORE_HDR   = priceRequestData[x].QUOTE_SCORE_HDR;   
                priceRequestItem.COMMISSION_HDR   = priceRequestData[x].COMMISSION_HDR;   
                priceRequestItem.TOTAL_VALUE_HDR   = priceRequestData[x].TOTAL_VALUE_HDR; 
                priceRequestItem.OPPORTUNITY_NUM = priceRequestData[x].OPPORTUNITY_NUM;  
                priceRequestItem.OPPORTUNITY_NAME = priceRequestData[x].OPPORTUNITY_NAME; 
                priceRequestItem.EXPECTED_SALES          = priceRequestData[x].EXPECTED_SALES; 
                
                // configure to order fields
                priceRequestItem.PARENT_ITEM   = priceRequestData[x].PARENT_ITEM;   
                priceRequestItem.IS_PARENT_ITEM      = priceRequestData[x].IS_PARENT_ITEM;  
                priceRequestItem.PRODUCT_LINE   = priceRequestData[x].PRODUCT_LINE;   
                priceRequestItem.UNIT_TYPE      = priceRequestData[x].UNIT_TYPE;   
                priceRequestItem.TOTAL_LENGTH   = priceRequestData[x].TOTAL_LENGTH; 
                priceRequestItem.HEIGHT         = priceRequestData[x].HEIGHT;  
                priceRequestItem.BASE_DEPTH     = priceRequestData[x].BASE_DEPTH; 
                priceRequestItem.BACKING        = priceRequestData[x].BACKING; 
                priceRequestItem.SHELF_TYPE     = priceRequestData[x].SHELF_TYPE;  
                priceRequestItem.SHELF_COUNT    = priceRequestData[x].SHELF_COUNT; 
                priceRequestItem.SHELF_DEPTH    = priceRequestData[x].SHELF_DEPTH; 
                priceRequestItem.COLOR          = priceRequestData[x].COLOR;  
                priceRequestItem.CONFIG_QTY     = priceRequestData[x].CONFIG_QTY; 


                priceRequestItem.EMPLOYEE = $rootScope.salesRep;
                
                // we don't support infinity quote scores
                if (priceRequestItem.TARGET_PRICE_INDEX == "Infinity")
                    {
                        priceRequestItem.TARGET_PRICE_INDEX = '0';
                    }
                
                // we don't support infinity quote scores
                if (priceRequestItem.TARGET_PRICE_INDEX == "NaN")
                    {
                        priceRequestItem.TARGET_PRICE_INDEX = '0';
                    }
                
                if (priceRequestItem.QUOTE_SCORE_HDR == "Infinity")
                    {
                        priceRequestItem.QUOTE_SCORE_HDR = '0';
                    }
                
                if (priceRequestItem.QUOTE_SCORE_HDR == "NaN")
                    {
                        priceRequestItem.QUOTE_SCORE_HDR = '0';
                    }
                
                if (priceRequestItem.CUSTOMER) //only send across those with a customer selected
                {
                    tblPriceRequest.push(priceRequestItem);
                }
            }            
    
        var priceRequestContent = {
            TEST: "", //test flag
            TBL_PRICE_REQUEST : tblPriceRequest
        };
        
            var config = { headers: {
                "X-Requested-With": "X",
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json" } };

            $http.post(requestURL, priceRequestContent, config)
                .success(function (data, status, headers, config) 
                    {
                        if (data.RETURN.TYPE == "S") //success
                        {
                            showMessage('Info', 'Price Quote created: ' + data.REQUEST_NBR);
                            var requestCreated = data.REQUEST_NBR;
                            AppService.synchCustomerList();
                            AppService.synchPriceRequestList().then(function(d) {
                                angular.element('#loaderDiv').hide();
                                $state.go('app.priceDetail', {id: requestCreated});
                            });
                        }
                        else //error, business data
                        {
                            showMessage('Error', 'An error happened: ' + data.RETURN.MESSAGE);
                            angular.element('#loaderDiv').hide();
                        }
                    })
                .error(function (error, status, header, config) 
                    {
                        angular.element('#loaderDiv').hide();
                        showMessage('Error', 'An error occurred '); //technical error
                    });
        
    }

    this.savePriceChangeData = function savePriceCreateData(priceRequestData, effectiveDate, expirationDate, soldTo, soldToName)
		{
        
            angular.element('#loaderDiv').show();

            var serverURL = $rootScope.serverURL;       
		    var requestUri = serverURL + '/sap/bc/zeasyprice/ZSD_EASYPRICE_PRICE_CHANGE';
            var requestURL = requestUri;
 
            // SAP date format = YYYYMMDD
            var effectiveDateArray = effectiveDate.split('/');
            var effectiveDateSAP;
            effectiveDateSAP = effectiveDateArray[2] + effectiveDateArray[0] + effectiveDateArray[1];
        
            var expirationDateArray = expirationDate.split('/');
            var expirationDateSAP;
            expirationDateSAP = expirationDateArray[2] + expirationDateArray[0] + expirationDateArray[1];
            
            var tblPriceRequest = new Array();
            var requestLength = priceRequestData.length;
            for (var x = 0; x < requestLength; x++)
            {
             var priceRequestItem = new Object();  
                priceRequestItem.PRICE_REQUEST = priceRequestData[x].PRICE_REQUEST;
                priceRequestItem.CUSTOMER = priceRequestData[x].CUSTOMER;
                priceRequestItem.MATERIAL = priceRequestData[x].MATERIAL;
                
                if (priceRequestItem.MATERIAL == "")
                    {
                        continue;
                    }
                priceRequestItem.NAME     = priceRequestData[x].CUSTOMER_NAME;
                priceRequestItem.DESCRIPTION = priceRequestData[x].DESCRIPTION;
                priceRequestItem.SALES_ORG = priceRequestData[x].SALES_ORG;
                priceRequestItem.DISTR_CHAN = priceRequestData[x].DISTR_CHAN;
                priceRequestItem.DIVISON = priceRequestData[x].DIVISION;
                priceRequestItem.REQUEST_STATUS = 'NEW'; //priceRequestData[x].REQUEST_STATUS;
                priceRequestItem.ITM_NUMBER = priceRequestData[x].ItemNumber;
                priceRequestItem.BID_PRICE   = priceRequestData[x].BID_PRICE;                
                priceRequestItem.BID_PRICE_UOM   = priceRequestData[x].BID_PRICE_UOM;               
                priceRequestItem.REQUESTED_BY = priceRequestData[x].REQUESTED_BY;
                priceRequestItem.REQUEST_TYPE = priceRequestData[x].REQUEST_TYPE;
                priceRequestItem.EFFECTIVE_DATE = effectiveDateSAP;
                priceRequestItem.EXPIRATION_DATE = expirationDateSAP;                
                priceRequestItem.REQUEST_DATE = priceRequestData[x].REQUEST_DATE;  
                priceRequestItem.COMMENTS = priceRequestData[x].COMMENTS;                  
                priceRequestItem.OPPORTUNITY_NUM = priceRequestData[x].OPPORTUNITY_NUM;  
                priceRequestItem.QUOTE_NUM = '';
                priceRequestItem.COMMISSION = priceRequestData[x].COMMISSION;  
                priceRequestItem.STRETCH_PRICE = priceRequestData[x].STRETCH_PRICE;   
                priceRequestItem.EXPECTED_REVENUE = priceRequestData[x].EXPECTED_REVENUE;   
                priceRequestItem.SHIP_TO = priceRequestData[x].CUSTOMER;
                priceRequestItem.SHIP_TO_NAME = priceRequestData[x].CUSTOMER_NAME;
                priceRequestItem.SOLD_TO = priceRequestData[x].SOLD_TO;
                priceRequestItem.SOLD_TO_NAME = priceRequestData[x].SOLD_TO_NAME;
                priceRequestItem.CUSTOMER_CITY = priceRequestData[x].CUSTOMER_CITY;
                priceRequestItem.CUSTOMER_REGION = priceRequestData[x].CUSTOMER_REGION;
                priceRequestItem.PACK_TYPE = priceRequestData[x].PACK_TYPE;
                priceRequestItem.SHIPMENT_MODE = priceRequestData[x].SHIPMENT_MODE;
                priceRequestItem.TARGET_PRICE   = priceRequestData[x].TARGET_PRICE; 
                priceRequestItem.TARGET_PRICE_INDEX   = priceRequestData[x].TARGET_PRICE_INDEX;   
                priceRequestItem.TARGET_PRICE_UNI   = priceRequestData[x].TARGET_PRICE_UNI;   
                priceRequestItem.TARGET_PRICE_UOM   = priceRequestData[x].TARGET_PRICE_UOM; 
                priceRequestItem.TARGET_PRICE_CUR   = priceRequestData[x].TARGET_PRICE_CUR;   
                priceRequestItem.EXCEPTION1_PRICE   = priceRequestData[x].EXCEPTION1_PRICE; 
                priceRequestItem.EXCEPTION2_PRICE   = priceRequestData[x].EXCEPTION2_PRICE; 
                priceRequestItem.TPI_CLASS          = priceRequestData[x].TPI_CLASS; 
                priceRequestItem.CSP_PRICE   = priceRequestData[x].CSP_PRICE;   
                priceRequestItem.CSP_PRICE_UNIT   = priceRequestData[x].CSP_PRICE_UNIT;   
                priceRequestItem.CSP_UOM   = priceRequestData[x].CSP_UOM;   
                priceRequestItem.CSP_CURRENCY   = priceRequestData[x].CSP_CURRENCY;   
                priceRequestItem.CSP_EFF_DATE   = priceRequestData[x].CSP_EFF_DATE;   
                priceRequestItem.CSP_EXP_DATE   = priceRequestData[x].CSP_EXP_DATE;   
                priceRequestItem.CSP_CONDITION_NB   = priceRequestData[x].CSP_CONDITION_NB; 
                priceRequestItem.HEADER_TEXT   = priceRequestData[x].HEADER_TEXT;   
                priceRequestItem.QUOTE_SCORE_HDR   = priceRequestData[x].QUOTE_SCORE_HDR;   
                priceRequestItem.COMMISSION_HDR   = priceRequestData[x].COMMISSION_HDR;   
                priceRequestItem.TOTAL_VALUE_HDR   = priceRequestData[x].TOTAL_VALUE_HDR; 
                priceRequestItem.OPPORTUNITY_NAME = priceRequestData[x].OPPORTUNITY_NAME; 
                priceRequestItem.EXPECTED_SALES          = priceRequestData[x].EXPECTED_SALES; 
                
                if (priceRequestItem.CUSTOMER) //only send across those with a customer selected
                {
                    tblPriceRequest.push(priceRequestItem);
                }
            }            
    
        var priceRequestContent = {
            TEST: "", //test flag
            TBL_PRICE_REQUEST : tblPriceRequest
        };
        
        var config = { headers: {
            "X-Requested-With": "X",
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/json" } };

        $http.post(requestURL, priceRequestContent, config)
            .success(function (data, status, headers, config) 
                {            
                    if (data.RETURN.TYPE == "S") //success
                    {
                        showMessage('Info', 'Price Quote updated: ' + data.REQUEST_NBR);
                        var requestCreated = data.REQUEST_NBR;
                        AppService.synchCustomerList().then(function(d) {
                            angular.element('#loaderDiv').hide();
                            $state.go('app.priceDetail', {id: requestCreated});
                        });
                    }
                    else //error, business data
                    {
                        showMessage('Error', 'An error happened: ' + data.RETURN.MESSAGE);
                        angular.element('#loaderDiv').hide();
                    }
            
                })
            .error(function (error, status, header, config) 
                {
                    angular.element('#loaderDiv').hide();
                    showMessage('Error', 'An error occurred ');
                });
    }

    // approve the price request
	this.approveRequest = function approveRequest(priceRequestData)
		{
             angular.element('#loaderDiv').show();

            var serverURL = $rootScope.serverURL;       
		    var requestUri = serverURL + '/sap/bc/zeasyprice/ZSD_EASYPRICE_REQUEST_ACTION';
            var requestURL = requestUri;
        
            var tblPriceRequest = new Array();
            var requestLength = priceRequestData.length;
            for (var x = 0; x < requestLength; x++)
            {
             var priceRequestItem = new Object();  
                priceRequestItem.PRICE_REQUEST = priceRequestData[x].PRICE_REQUEST;
                priceRequestItem.COMMENTS = priceRequestData[x].COMMENTS;                  
                
                if (priceRequestItem.PRICE_REQUEST) //only send across those with a customer selected
                {
                    tblPriceRequest.push(priceRequestItem);
                }
            }
    
        var priceRequestContent = {
        
            ACTION: "APPROVE",
            TBL_PRICE_REQUEST : tblPriceRequest
        }
        
        var config = { headers: {
            "X-Requested-With": "X",
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/json" } };

        $http.post(requestURL, priceRequestContent, config)
            .success(function (data, status, headers, config) 
                {
                    showMessage('Info', 'Price Request Approved: ' + data.REQUEST_NBR);
                    AppService.synchCustomerList().then(function(d) {
                        $state.go('app.priceList', {id: 'complete'});
                        angular.element('#loaderDiv').hide();
                    });

                })
            .error(function (error, status, header, config) 
                {
                    angular.element('#loaderDiv').hide();
                    showMessage('Error', 'An error occurred ');
                });
    }	
    
    // Delete or reject the request
    this.rejectRequest = function rejectRequest(priceRequestData)
		{

            angular.element('#loaderDiv').show();
    
            var serverURL = $rootScope.serverURL;       
		    var requestUri = serverURL + '/sap/bc/zeasyprice/ZSD_EASYPRICE_REQUEST_ACTION';
            var requestURL = requestUri;
        
            var tblPriceRequest = new Array();
            var requestLength = priceRequestData.length;
            for (var x = 0; x < requestLength; x++)
            {
             var priceRequestItem = new Object();  
                priceRequestItem.PRICE_REQUEST = priceRequestData[x].PRICE_REQUEST;
                priceRequestItem.COMMENTS = priceRequestData[x].COMMENTS;                  
                
                if (priceRequestItem.PRICE_REQUEST) //only send across those with a customer selected
                {
                    tblPriceRequest.push(priceRequestItem);
                }
            }
    
        var priceRequestContent = {
        
            ACTION: "REJECT",
            TBL_PRICE_REQUEST : tblPriceRequest
        }
        
        var config = { headers: {
            "X-Requested-With": "X",
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/json" } };

        $http.post(requestURL, priceRequestContent, config)
            .success(function (data, status, headers, config) 
                {
                    showMessage('Info', 'Price Request Deleted');
                    AppService.synchCustomerList().then(function(d) {
                        $state.go('app.priceList', {id: 'complete'});
                        angular.element('#loaderDiv').hide();
                    });

                })
            .error(function (error, status, header, config) 
                {
                    angular.element('#loaderDiv').hide();
                    showMessage('Error', 'An error occurred ');
                });
		}	
});