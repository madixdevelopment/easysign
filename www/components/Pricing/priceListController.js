// pricing controller
// change history
// 1/26/2017 - initial
angular.module("app")
.config(function($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
})
.controller("priceEditCtrl", function ($scope, $http, $stateParams,  $state, $rootScope, AppService, PriceService) {
	    
})
.controller("priceDetailCtrl", function ($scope, $http, $stateParams, $state, PriceService) {
	
		$scope.price = new Object();
		$scope.priceDetails = new Array(); // will hold the price items
		$scope.quote = new Object();
		$scope.item = {};
        $scope.configuration = {}; //configuration data
    
		$scope.request_id = $stateParams.id; //$routeParams.id;
		var Cart = new Array();

        // for pricing score gauge
            var opts = {
              lines: 2, // The number of lines to draw
              angle: 0, // The length of each line
              lineWidth: 0.44, // The line thickness
              pointer: {
                length: 0.9, // The radius of the inner circle
                strokeWidth: 0.035, // The rotation offset
                color: '#000000' // Fill color
              },
              colorStart: '#CF44B3',   // Colors
              colorStop: '#FFF',    // just experiment with them
              strokeColor: '#E0E0E0',   // to see which ones work best for you
              percentColors: [[0.0, "#ff0000"], [0.60, "#f9c802"], [0.80, "#22B35A"]],
              generateGradient: true
            };
    
        $scope.buildGauge = function()
        {
            /*var target = document.getElementById('foo'); // your canvas element
            var gauge = new Gauge(target);
            gauge.setOptions(opts); // create sexy gauge!
            gauge.maxValue = 140; // set max gauge value
            gauge.animationSpeed = 32; // set animation speed (32 is default value)
            gauge.set(20); // set actual value*/
        }
        
        $scope.commissionPercent = .10;
        $scope.calculateTotalRevenue = function()
        {
            // loop through all tpi's to calculate an average
            var priceLength = $scope.priceDetails.length;
            var totalRevenueValue = 0;
            for (var y = 0; y < priceLength; y++)
            {
                var bidPriceNumber = parseFloat($scope.priceDetails[y].BID_PRICE);
                
                var expectedSalesString = $scope.priceDetails[y].EXPECTED_SALES;
                expectedSalesString = expectedSalesString.replace(',', '');
                var expectedSalesNumber = parseFloat(expectedSalesString);
                 if (bidPriceNumber > 0 && expectedSalesNumber > 0)
                 {
                     totalRevenueValue = totalRevenueValue + 
                        (bidPriceNumber *
                         expectedSalesNumber)
                     
                     $scope.priceDetails[y].EXPECTED_REVENUE = (bidPriceNumber * expectedSalesNumber);
                     $scope.priceDetails[y].EXPECTED_REVENUE = $scope.priceDetails[y].EXPECTED_REVENUE.toFixed(0);  
                     
                     $scope.priceDetails[y].COMMISSION = $scope.priceDetails[y].EXPECTED_REVENUE * $scope.commissionPercent;
                     $scope.priceDetails[y].COMMISSION = $scope.priceDetails[y].COMMISSION.toFixed(0);  
                 }
            }
            $scope.totalRevenue = totalRevenueValue.toFixed(0);  
            $scope.totalCommission = parseFloat($scope.totalRevenue * $scope.commissionPercent).toFixed(0);                         
        }
        
        $scope.shipTo = {};
        $scope.soldTo = {};
        $scope.customerList = [];
		//read details based on the price request selected
		$scope.getPrice = function (id) {
			
			$scope.priceDetails = PriceService.getPrice(id);
            $scope.price = $scope.priceDetails[0];
            
            for (var x=0; x< $scope.priceDetails.length; x++)
                {
                    $scope.priceDetails[x].ItemNumber = $scope.priceDetails[x].ITM_NUMBER;
                    var itemNbr = {}; //object.
                    itemNbr.id = $scope.priceDetails[x].ItemNumber;
                    $scope.bidPriceChange(itemNbr); //build the dial, etc for the 1st item. 
                    
                    // populate configuration data
                    if ($scope.priceDetails[x].IS_PARENT_ITEM == 'X')
                    {
                        
                        $scope.configuration.PRODUCT_LINE = $scope.priceDetails[x].PRODUCT_LINE; 
                        $scope.configuration.UNIT_TYPE = $scope.priceDetails[x].UNIT_TYPE; 
                        $scope.configuration.TOTAL_LENGTH = $scope.priceDetails[x].TOTAL_LENGTH; 
                        $scope.configuration.HEIGHT = $scope.priceDetails[x].HEIGHT; 
                        $scope.configuration.BASE_DEPTH = $scope.priceDetails[x].BASE_DEPTH; 
                        $scope.configuration.BACKING = $scope.priceDetails[x].BACKING; 
                        $scope.configuration.SHELF_TYPE = $scope.priceDetails[x].SHELF_TYPE; 
                        $scope.configuration.SHELF_COUNT = $scope.priceDetails[x].SHELF_COUNT; 
                        $scope.configuration.SHELF_DEPTH = $scope.priceDetails[x].SHELF_DEPTH; 
                        $scope.configuration.COLOR = $scope.priceDetails[x].COLOR; 
                        $scope.configuration.CONFIG_QTY = $scope.priceDetails[x].CONFIG_QTY; 
                    }
                }
            
            // get customer information.
            if ($scope.customerList)
            {
            for (var x=0; x< $scope.customerList.length; x++)
                {
                    if ($scope.price.CUSTOMER == $scope.customerList[x].CUSTOMER)
                        {
                        $scope.soldTo = $scope.customerList[x];
                        }
                  
                    if ($scope.price.SHIP_TO == $scope.customerList[x].CUSTOMER)
                        {
                        $scope.shipTo = $scope.customerList[x];
                        }
                }
            }
    	}
        
        $scope.determineItemNumber = function(itemNbr)
        {
            // determine the item index number
            for (var x=0; x<$scope.priceDetails.length; x++)
                {
                    if (itemNbr.id == $scope.priceDetails[x].ItemNumber)
                        {
                        return x;
                        }
                }
        }
				
        // updates target price index.
        $scope.bidPriceChange = function(itemNbr)
        {
            $scope.currentItem = $scope.determineItemNumber(itemNbr);
            
            var bidPrice = parseFloat($scope.priceDetails[$scope.currentItem].BID_PRICE);
            var targetPrice = parseFloat($scope.priceDetails[$scope.currentItem].TARGET_PRICE);
            var exception1 = parseFloat($scope.priceDetails[$scope.currentItem].EXCEPTION1_PRICE);
            var exception2 = parseFloat($scope.priceDetails[$scope.currentItem].EXCEPTION2_PRICE);
            
            var bidPriceNumber = isNaN(bidPrice);
            
            $scope.priceDetails[$scope.currentItem].btnClass = 'default';
            $scope.priceDetails[$scope.currentItem].TPI_CLASS = $scope.priceDetails[$scope.currentItem].btnClass;
            //label-success
            if (bidPriceNumber == true) // exit if we don't have a number
            {
                $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX = 0;
                return;
            }
            
            $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX = (bidPrice/targetPrice) * 100;
            $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX = 
            $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX.toFixed(2);
            
            var tpiLocal = $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX;
   
            if (tpiLocal > 80)
            {
                $scope.priceDetails[$scope.currentItem].btnClass = 'success';
            }
            else if (tpiLocal > 60)
            {
                $scope.priceDetails[$scope.currentItem].btnClass = 'warning';  
            }
            else if (tpiLocal >= 0)
            {
                $scope.priceDetails[$scope.currentItem].btnClass = 'danger';  
            }
            
            
            // loop through all tpi's to calculate an average
            var totalTPI = 0;
            var totalItems = 0;
            var priceLength = $scope.priceDetails.length;
            for (var y = 0; y < priceLength; y++)
            {
                 if (parseFloat($scope.priceDetails[y].TARGET_PRICE_INDEX) > 0)
                 {
                     totalItems = totalItems + 1;
                     totalTPI = totalTPI + parseFloat($scope.priceDetails[y].TARGET_PRICE_INDEX);
                 }
            }
            var totalTPIScore = totalTPI / totalItems.toFixed(2);
            $scope.totalTPIValue = totalTPIScore;
            totalTPIScore = parseFloat(totalTPIScore);
            
            $scope.totalTPIValue = $scope.totalTPIValue.toFixed(1);
            
            /*var target = document.getElementById('foo'); // your canvas element
            var gauge = new Gauge(target);

            
            gauge.setOptions(opts); // create sexy gauge!
            gauge.maxValue = 140; // set max gauge value
            gauge.animationSpeed = 32; // set animation speed (32 is default value)
            
            // if we are at 0, then set it to 20 to enable the gauge to render
            if (totalTPIScore == 0)
                {
                    totalTPIScore = 20;
                }
            gauge.set(totalTPIScore); // set actual value
            */

            
            $scope.calculateTotalRevenue();
        }
        
    // logic to validate action
		$scope.validateApproval = function validateApproval(request)
		{
			return true; // if we are here then all is good			
		}
		
		// logic to actually save the price request
		$scope.approvePriceRequest = function(request) {
			
			var isValid = false;
			isValid = $scope.validateApproval(request)
			
			if (isValid == true)
				{
                PriceService.approveRequest($scope.priceDetails);
				}
		}
     
		// logic to actually save the price request
		$scope.rejectPriceRequest = function(request) {
			
            var priceRequestTemp = new Array();
            priceRequestTemp.push(request.id);
                PriceService.rejectRequest($scope.priceDetails);
		}
        
		// logic to copy a price request
		$scope.copyPriceRequest = function(request) {
            $state.go('app.quoteCopy', {id: request.id});
		}
        
		// logic to edit a price request
		$scope.editPriceRequest = function(request) {
            $state.go('app.quoteEdit', {id: request.id});
		}
        
        // launch the create order screens
        $scope.createOrder = function(quote)
        {
            $state.go('app.orderCreate', {id: quote.PRICE_REQUEST});
        }
        
        // launch the change request screens
        $scope.changePriceRequest = function(quote)
        {
            $state.go('app.priceEdit', {id: quote.id});
        }
        
		$scope.showPricePrint = function()
		{
			$('#createScreen').hide();	
			$('#pricePrintScreen').show(300);
		
		}
        
		$scope.hidePricePrint = function()
		{
			$('#createScreen').show(300);
			$('#pricePrintScreen').hide();			
		}
        
        // logic for initialization
        $scope.buildGauge();
		// call function to get the price details.
		if ($scope.request_id)
		{
			$scope.getPrice($scope.request_id);
		}

        $scope.hidePricePrint();
    
})
.controller("priceReviewController", function ($scope, $http, $stateParams,  $rootScope, AppService, PriceService) {
    
    $scope.listType = $stateParams.id;
    $scope.app.priceRequestFiltered = new Array(); 
    
    $scope.filterPriceRequests = function()
    {
        var priceRequests = new Array();
        
             if ($scope.listType == 'submitted')
             {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        if ('NEW' == $rootScope.priceRequestList[x].REQUEST_STATUS)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;
             }
            /*else if ($scope.listType == 'draft')
            {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        if ('Draft' == $rootScope.priceRequestList[x].REQUEST_STATUS)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;                
            }*/
            else if ($scope.listType == 'rejected')
            {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        if ('Rejected' == $rootScope.priceRequestList[x].REQUEST_STATUS)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;                
            }
            else if ($scope.listType == 'complete')
            {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        if ('Complete' == $rootScope.priceRequestList[x].REQUEST_STATUS)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;                
            }
    }
    
    $scope.listPriceRequests = function () {
		
	       //$scope.app.priceList = PriceService.getPriceList();

			// synch if needed.				
			if ($rootScope.priceRequestList.length === 0)
			{
	   	  		AppService.synchPriceRequestList().then(function(d) {
		  		
	  			});
			}
    }
    
    
    // logic to validate action
		$scope.validateApproval = function validateApproval(request)
		{
			
			//if (!$scope.priceHeader.Requisition)
			//{
			//	showMessage('Error', 'Please select a job');
				//return false;
			//}

			//if (!$('#requestDate').val())
			//{
				//showMessage('Error', 'Please select a start date');
				//return false;
			//}
			
			return true; // if we are here then all is good
			
		}
		
		// logic to actually save the price request
		$scope.approvePriceRequest = function(request) {
            
            alert('approving request');
			
			var isValid = false;
			isValid = $scope.validateApproval(request)
			
            //$scope.priceDetails[$scope.currentItem].MATERIAL = material.id.MATERIAL;
			
            var priceRequestTemp = new Array();
            priceRequestTemp.push(request.id);
			if (isValid == true)
				{
                PriceService.approveRequest(priceRequestTemp);
				//PriceService.getCSRFToken(PriceService.savePriceCreateData);
				}
		}
                
        $scope.rejectPriceRequest = function(request)
		{
            alert('rejecting request');
            
		}
        
//  functions to initialize the controller.
   // initialize the controller    
   $scope.listPriceRequests(); //call list to start the screen
    
    if (!$scope.listType)
    {
     $scope.listType = 'submitted';   
    }
    
    $scope.app.priceRequestFiltered = $scope.filterPriceRequests();
    
})
.controller("priceCreateCtrl", function ($scope, $http, $stateParams, $window, $timeout, $rootScope, $location, AppService, PriceService, OpportunityService) {
		$scope.priceHeader = new Object();
		$scope.priceDetails = new Array(); // will hold the price items
		$scope.requisitionHeader = new Object();
		$scope.requisitionDetails = new Array();
        $scope.requestType = 'Configure';
		$scope.quote = new Object();
        $scope.currentItem = 0;
        $scope.totalRevenue = 0;
        $scope.totalTargetValue = 0;
        $scope.totalCommission = 0;
		var Cart = new Array();
    
        $scope.topMaterials = false;
        $scope.allMaterials = false;
        $scope.uploadDone = false;

        $scope.effectiveDate = new Date();    
        $scope.expirationDate = new Date();
        $scope.requestDate = new Date();
        $scope.requestStatus = 'NEW';
        $scope.soldTo = '';
        $scope.soldToName = '';
        $scope.soldToCity = '';
        $scope.soldToRegion = '';
        $scope.totalTPIValue = 0;
        $scope.totalRevenue = 0;
        $scope.headerComments = '';
        $scope.customerMaterialList = []; //holds the list of possible materials for the sold to.
    
        $scope.configuration = {};
    
        //dummy sold to - madix dummy data
        //$scope.soldTo = '1234';            
        
        /*var testCustomer = {};
            testCustomer.CUSTOMER = '1234';
            testCustomer.CUSTOMER_NAME = 'test';
            testCustomer.CITY = 'Dallas';
            testCustomer.REGION = 'TX';
    
            $rootScope.customerList = [];
            $rootScope.customerList.push(testCustomer);*/
    
            $rootScope.customerMaterials = [];
            var testMaterial = {};
            testMaterial.CUSTOMER = '1234';
            testMaterial.MATERIAL = 'GONDOLA';
            $rootScope.customerMaterials.push(testMaterial);
    
            var testMaterial = {};
            testMaterial.CUSTOMER = '1234';
            testMaterial.MATERIAL = 'WALL';
            $rootScope.customerMaterials.push(testMaterial);
    
        // initialize dates
        $scope.dateFormat = 'MM/dd/yyyy';
        // the date model does not work for some reason... need to figure it out.
        $scope.priceEffectiveDate = new Date();
    
        $scope.effectiveDate = new Date();
    
        $scope.effectiveDate = ('0' + ($scope.effectiveDate.getMonth()+1)).slice(-2) + '/'
								 + ('0' + $scope.effectiveDate.getDate()).slice(-2) + '/' +
								($scope.effectiveDate.getFullYear());
    
        $scope.expirationDate.setDate($scope.expirationDate.getDate() + 30); //default to 30 days
        $scope.expirationDate = ('0' + ($scope.expirationDate.getMonth()+1)).slice(-2) + '/'
								 + ('0' + $scope.expirationDate.getDate()).slice(-2) + '/' +
								($scope.expirationDate.getFullYear());
    
        // we need to set the max expiration date to 90 days in the future
        $scope.maxDate = new Date();
        $scope.maxDate = addDays($scope.maxDate, 90);
    
    
        // for pricing score gauge
            var opts = {
              lines: 2, // The number of lines to draw
              angle: 0, // The length of each line
              lineWidth: 0.44, // The line thickness
              pointer: {
                length: 0.9, // The radius of the inner circle
                strokeWidth: 0.035, // The rotation offset
                color: '#000000' // Fill color
              },
              colorStart: '#CF44B3',   // Colors
              colorStop: '#FFF',    // just experiment with them
              strokeColor: '#E0E0E0',   // to see which ones work best for you
              percentColors: [[0.0, "#ff0000"], [0.60, "#f9c802"], [0.80, "#22B35A"]],
              generateGradient: true
            };        
    
        // sets the type of request.
        $scope.setPriceType = function(priceType)
        {
         $scope.requestType = priceType;   
        }
    
        $scope.syncC4CData = function()
        {
            AppService.synchOpportunityList();
        }
        
        $scope.buildGauge = function()
        {

            /*var target = document.getElementById('foo'); // your canvas element
            var gauge = new Gauge(target);
            //alert("here");
            gauge.setOptions(opts); // create sexy gauge!
            gauge.maxValue = 140; // set max gauge value
            gauge.animationSpeed = 32; // set animation speed (32 is default value)
            gauge.set(20); // set actual value*/
        }

    
		// logic to validate price create
		$scope.validatePrice = function validatePrice()
		{
			
			//if (!$scope.priceHeader.Requisition)
			//{
			//	showMessage('Error', 'Please select a job');
				//return false;
			//}

			if (!$('#effectiveDateInput').val())
			{
				//showMessage('Error', 'Please select an effective date');
				//return false;
			}
            
			if (!$('#expirationDateInput').val())
			{
				//showMessage('Error', 'Please select an expiration date');
				//return false;
			}
            
            // ensure all items have data.
            var priceLength = $scope.priceDetails.length;
            for (var z = 0; z < priceLength; z++) // move header variables to the item
            {
            
             // ensure we have a valid material number.  Removed for SAP to verify.
             var lineNumber = z+1;
             if (!$scope.priceDetails[z].IS_FOUND)// item was not found in catalog
                 {
                    //showMessage('Error', 'Material ' +  $scope.priceDetails[z].MATERIAL + ' in line ' + 
                    //lineNumber + ' is invalid, please remove or correct and retry.');
                    //return false;
                 }
                
                // check for no quantity
                if (  parseFloat($scope.priceDetails[z].EXPECTED_SALES == 0) || 
                    $scope.priceDetails[z].EXPECTED_SALES == "")
                    {
                        var itemNumber = x+1;
                        showMessage('Error', 'Please enter a quantity for item '  +  $scope.priceDetails[z].MATERIAL);
                        return false;
                    }
                
                // no bid price
                var bidPriceNum = parseFloat($scope.priceDetails[z].BID_PRICE);
                if (bidPriceNum == 0 || 
                    $scope.priceDetails[z].BID_PRICE == "")
                    {
                     showMessage('Error', 'Please enter a price for item ' +  $scope.priceDetails[z].MATERIAL);
                    return false;
                    }
                
                if ($scope.priceDetails[z].isLoading == true)
                    {
                     //showMessage('Error', 'Please wait for pricing to complete for item ' +  $scope.priceDetails[z].MATERIAL + '.  To retrigger, update the price.');
                    //return false;
                    }
                
            }
            
            // ensure we have line items.
            if (priceLength == 0)
                {
                    showMessage('Error', 'Please add at least one material.');
                   return false; 
                }
			
			return true; // if we are here then all is good
			
		}
		
		// logic to actually save the price request
		$scope.savePrice = function () {
			
			var isValid = false;
			isValid = $scope.validatePrice()
			
			
			if (isValid == true)
				{
				//angular.element('#loaderDiv').show();                    
                    var priceLength = $scope.priceDetails.length;
                    for (var z = 0; z < priceLength; z++) // move header variables to the item
                    {
                     $scope.priceDetails[z].REQUEST_TYPE = $scope.requestType;
                     $scope.priceDetails[z].HEADER_TEXT = $scope.headerComments;
                     $scope.priceDetails[z].QUOTE_SCORE_HDR = $scope.totalTPIValue;                        
                     $scope.priceDetails[z].COMMISSION_HDR = $scope.totalCommission; 
                     $scope.priceDetails[z].TOTAL_VALUE_HDR = $scope.totalRevenue; 
                 
                     if ($scope.opportunity.OPPORTUNITYID)
                         {
                             $scope.priceDetails[z].OPPORTUNITY_NUM = $scope.opportunity.OPPORTUNITYID; 
                        }
                        
                     if ($scope.opportunity.OPPORTUNITYNAME)
                         {
                             $scope.priceDetails[z].OPPORTUNITY_NAME = $scope.opportunity.OPPORTUNITYNAME; 
                        }
                    }
                    
                $scope.effectiveDate = angular.element('#effectiveDateInput').val();
                $scope.expirationDate = angular.element('#expirationDateInput').val();
                PriceService.savePriceCreateData($scope.priceDetails, $scope.effectiveDate, $scope.expirationDate, $scope.soldTo, $scope.soldToName);
				}
		}
			
// logic to actually save the price request edit
		$scope.savePriceChange = function () {
			
			var isValid = false;
			isValid = $scope.validatePrice()
			
			
			if (isValid == true)
				{
				//angular.element('#loaderDiv').show();                    
                    var priceLength = $scope.priceDetails.length;
                    for (var z = 0; z < priceLength; z++) // move header variables to the item
                    {
                     $scope.priceDetails[z].REQUEST_TYPE = $scope.requestType;
                     $scope.priceDetails[z].HEADER_TEXT = $scope.headerComments;
                     $scope.priceDetails[z].QUOTE_SCORE_HDR = $scope.totalTPIValue;       
                     $scope.priceDetails[z].COMMISSION_HDR = $scope.totalCommission; 
                     $scope.priceDetails[z].TOTAL_VALUE_HDR = $scope.totalRevenue; 
                        
                     if ($scope.opportunity.OPPORTUNITYID)
                         {
                             $scope.priceDetails[z].OPPORTUNITY_NUM = $scope.opportunity.OPPORTUNITYID; 
                        }
                        
                     if ($scope.opportunity.OPPORTUNITYNAME)
                         {
                             $scope.priceDetails[z].OPPORTUNITY_NAME = $scope.opportunity.OPPORTUNITYNAME; 
                        }
                    }
                    
                $scope.effectiveDate = angular.element('#effectiveDateInput').val();
                $scope.expirationDate = angular.element('#expirationDateInput').val();
                PriceService.savePriceChangeData($scope.priceDetails, $scope.effectiveDate, $scope.expirationDate, $scope.soldTo, $scope.soldToName);
				}
		}
        
    
        $scope.buildUploadedProducts = function(uploadedData)
        {            
            
            console.log('building uploaded products');
            var dataObject = JSON.parse(uploadedData);
            
            for (var i in dataObject)
                 {
                  if (dataObject.hasOwnProperty(i)) //goes through top level worksheet
                      {
                         console.log(i);
                         var worksheetArray = dataObject[i]; 
                          var worksheetLength = worksheetArray.length;
                          for (var x = 0; x<worksheetArray.length; x++)
                              {
                                  var productRow = worksheetArray[x];
                                  //console.log(productRow.toString());
                                  var productId = productRow[Object.keys(productRow)[0]];
                                  var quantity = productRow[Object.keys(productRow)[1]];
                                  // call logic to add to an item;
                                  $scope.addUploadedItem(productId, quantity);
                                  
                                   // now since we are at the last item, then add one more...
                                  if (x+1 != worksheetLength)
                                      {
                                        $scope.addItems(); //add next item.
                                      }
                                  
                              }
                      }
                 } 
            
            // set the upload field to be hidden since the user has uploaded 1 set of products.
            console.log('hiding upload button since we are done uploading a file');
            $scope.uploadDone = true;
            
        }
        
        $scope.getNextAvailableItemNumber = function()
        {
                for (var x=0; x<$scope.priceDetails.length; x++)
                {
                    if ($scope.priceDetails[x].MATERIAL == "")
                        {
                        return x;
                        }
                }
        }
        
        $scope.readMaterialDescription = function(productId)
        {
            // binary search not working yet
            /*var index = binarySearch($rootScope.materialList, productId, 'MATERIAL')
            if (index > -1)
                {
                    return $rootScope.materialList[index].DESCRIPTION;
                }*/
            var materialListLength = $rootScope.materialList.length;
            for (var x=0; x<materialListLength; x++)
                {
                    if (productId == $rootScope.materialList[x].MATERIAL)
                        {
                            return $rootScope.materialList[x].DESCRIPTION;
                        }
                }
            
        }
        
        $scope.readMaterialDetails = function(productId)
        {
            var materialListLength = $rootScope.materialList.length;
            for (var x=0; x<materialListLength; x++)
                {
                    if (productId == $rootScope.materialList[x].MATERIAL)
                        {
                            return $rootScope.materialList[x];
                        }
                }
        }
        
        // placeholder
        $scope.parentItem;
    
        $scope.addWizardItem = function(productId, quantity, type, description)
        {
            //determine next available item.
            var lastItem = $scope.priceDetails.length - 1;
            if (lastItem < 0) //no items, ensure we add an item.
            {
                $scope.addItems(); //add next item.
                lastItem = $scope.priceDetails.length - 1;
            }
            else if ($scope.priceDetails[lastItem].MATERIAL != "")
            {
                $scope.addItems(); //add next item.
                lastItem = $scope.priceDetails.length - 1;
            }

            // determine the item index number
            console.log(lastItem);
            $scope.priceDetails[lastItem].MATERIAL = productId;
            $scope.priceDetails[lastItem].EXPECTED_SALES = quantity;
            $scope.priceDetails[lastItem].TYPE = type;
            
            var materialDetail = $scope.readMaterialDetails(productId);
            
            if (description)
                {
                    $scope.priceDetails[lastItem].DESCRIPTION = description;
                }
            else
                {
                    $scope.priceDetails[lastItem].DESCRIPTION = materialDetail.DESCRIPTION;
                }

            // populate price
            $scope.priceDetails[lastItem].BID_PRICE = materialDetail.LIST_PRICE;
            
            if (materialDetail.LIST_PRICE)
                {
                    $scope.priceDetails[lastItem].EXPECTED_REVENUE = materialDetail.LIST_PRICE * quantity;
                }

            // set as default parent item.
            // start of configuration logic
            $scope.priceDetails[lastItem].IS_PARENT_ITEM = ''; 
            $scope.priceDetails[lastItem].IS_CHILD_ITEM = 'X';
            //  copy over configuration items.
            if (type == 'Section') //this is top item
                {
                    $scope.priceDetails[lastItem].IS_PARENT_ITEM = 'X'; 
                    $scope.priceDetails[lastItem].IS_CHILD_ITEM = ''; 
                    $scope.parentItem = $scope.priceDetails[lastItem].ItemNumber;
                }
 
            $scope.priceDetails[lastItem].PARENT_ITEM    = $scope.parentItem;   
            $scope.priceDetails[lastItem].PRODUCT_LINE   = $scope.configuration.productLine.NAME;   
            $scope.priceDetails[lastItem].UNIT_TYPE      = $scope.configuration.unitType.NAME;   
            $scope.priceDetails[lastItem].TOTAL_LENGTH   = $scope.configuration.aisleLength; 
            $scope.priceDetails[lastItem].HEIGHT         = $scope.configuration.sectionHeight;  
            $scope.priceDetails[lastItem].BASE_DEPTH     = $scope.configuration.baseDepth; 
            $scope.priceDetails[lastItem].BACKING        = $scope.configuration.unitBacking; 
            $scope.priceDetails[lastItem].SHELF_TYPE     = $scope.configuration.shelfType.NAME;  
            $scope.priceDetails[lastItem].SHELF_COUNT    = $scope.configuration.shelfCount; 
            $scope.priceDetails[lastItem].SHELF_DEPTH    = $scope.configuration.shelfDepth; 
            $scope.priceDetails[lastItem].COLOR          = $scope.configuration.unitColor;  
            $scope.priceDetails[lastItem].CONFIG_QTY     = $scope.configuration.aisleQuantity; 
             // end of configuration logic                   
            
            $scope.priceDetails[lastItem].IS_FOUND = true;
            $scope.priceDetails[lastItem].isLoading = false;
            
            // now trigger the lookup and validation of the item and the pricing retrieval
            var itemNbr = {};
            itemNbr.id = $scope.priceDetails[lastItem].ItemNumber;
            
            // for speed, just simulate
            $scope.simulateQuoteLineItem(itemNbr); //simulate the quote

            // redraw the table to correctly setup for responsiveness
            $scope.redrawTable();
            
        }
        
        $scope.addUploadedItem = function(productId, quantity)
        {
            //determine next available item.
            var lastItem = $scope.priceDetails.length - 1;
            if (lastItem < 0) //no items, ensure we add an item.
            {
                $scope.addItems(); //add next item.
                lastItem = $scope.priceDetails.length - 1;
            }
            else if ($scope.priceDetails[lastItem].MATERIAL != "")
            {
                $scope.addItems(); //add next item.
                lastItem = $scope.priceDetails.length - 1;
            }

            // determine the item index number
            console.log(lastItem);
            $scope.priceDetails[lastItem].MATERIAL = productId;
            $scope.priceDetails[lastItem].EXPECTED_SALES = quantity;
            $scope.priceDetails[lastItem].DESCRIPTION = $scope.readMaterialDescription(productId);

            $scope.priceDetails[lastItem].IS_FOUND = true;
            $scope.priceDetails[lastItem].isLoading = false;
            
            // now trigger the lookup and validation of the item and the pricing retrieval
            var itemNbr = {};
            itemNbr.id = $scope.priceDetails[lastItem].ItemNumber;
            
            // for speed, just simulate
            $scope.simulateQuoteLineItem(itemNbr); //simulate the quote

            // redraw the table to correctly setup for responsiveness
            $scope.redrawTable();
            
        }
        
        
        $scope.dataTest = function(data)
        {
            console.log('data test');
        }
        
		$scope.showCustomerList = function(itemNbr)
		{
            $scope.currentItem = $scope.determineItemNumber(itemNbr);
			$('#customerListScreen').show(300);
			$('#createScreen').hide();	
            
            $scope.redrawTable();
		}

		$scope.showSoldToList = function()
		{
			$('#soldToListScreen').show(300);
			$('#createScreen').hide();	
            
            $scope.redrawTable();
		}
        
		$scope.showOpportunityList = function()
		{
			$('#opportunityListScreen').show(300);
			$('#createScreen').hide();	
            
            $scope.redrawTable();
		}


        $scope.determineItemNumber = function(itemNbr)
        {
            // determine the item index number
            for (var x=0; x<$scope.priceDetails.length; x++)
                {
                    if (itemNbr.id == $scope.priceDetails[x].ItemNumber)
                        {
                        return x;
                        }
                }
        }
        
        $scope.setFocus = function(elementId)
        {
            var element = $window.document.getElementById(elementId);
                if(element)
                 { element.focus();
                 }
        }
        
        $scope.searchMaterial = '';
        $scope.scannedMaterial = '';
    
        // setup initial data.
        $scope.productLines = [];
        var productLine = {};
        productLine.NAME = 'MAXILINE';
        productLine.CODE = 'M';
        productLine.IMG = 'http://www.madixinc.com/images/products/maxi-line/2016-maxi-lp.png';
    
        // add unit types
        productLine.unitTypes = [];
        var unitType = {};
        unitType.NAME = 'GONDOLA';
        unitType.IMG = 'img/configuration/gondola.jpeg';
        unitType.CODE = 'G';
        productLine.unitTypes.push(unitType);
    
        var unitType = {};
        unitType.NAME = 'WALL';
        unitType.IMG = 'img/configuration/wall.jpeg';
        unitType.CODE = 'W';
        productLine.unitTypes.push(unitType);
    
        // add shelf types
        productLine.shelfTypes = [];
        var shelfType = {};
        shelfType.NAME = 'SUS';
        shelfType.DESCRIPTION = 'Standard Upper Shelf';
        shelfType.IMG = 'img/configuration/sa-sus.jpg';
        productLine.shelfTypes.push(shelfType);
    
        var shelfType = {};
        shelfType.NAME = 'STPH';
        shelfType.DESCRIPTION = 'Heavy Duty Shelf';
        shelfType.IMG = 'img/configuration/sa-heavy-duty.jpg';
        productLine.shelfTypes.push(shelfType);
    
    
        // add additional items types
        // add unit types
        productLine.recommendedItems = [];
        var recomendedItem = {};
        recomendedItem.NAME = 'PEG HOOKS';
        recomendedItem.DESCRIPTION = 'Peg Hooks';
        recomendedItem.IMG = 'img/configuration/peghooks.jpg';
        productLine.recommendedItems.push(recomendedItem);
    
        var recomendedItem = {};
        recomendedItem.NAME = 'BASKET';
        recomendedItem.DESCRIPTION = 'Basket';
        recomendedItem.IMG = 'img/configuration/basket.gif';
        productLine.recommendedItems.push(recomendedItem);
    
        var recomendedItem = {};
        recomendedItem.NAME = 'DIVIDER';
        recomendedItem.DESCRIPTION = 'Divider';
        recomendedItem.IMG = 'img/configuration/divider.jpg';
        productLine.recommendedItems.push(recomendedItem);
    
        var recomendedItem = {};
        recomendedItem.NAME = 'LABEL';
        recomendedItem.DESCRIPTION = 'Shelf Labels';
        recomendedItem.IMG = 'img/configuration/labels.jpg';
        productLine.recommendedItems.push(recomendedItem);
    
    
    
        // finally add the product line to the catalog.
        $scope.productLines.push(productLine);
    
        var productLine = {};
        productLine.NAME = 'TUBELINE';
        productLine.DESCRIPTION = '';
        productLine.CODE = 'T';
        productLine.IMG = 'http://www.madixinc.com/images/products/tube-line/2015-tube-line-hero.jpg';
        $scope.productLines.push(productLine);
    
        // select the initial product line
        $scope.unitTypes = [];
        $scope.selectProductLine = function(productLine)
        {
            $scope.configuration.productLine = productLine.id;    
            //$scope.configuration.productLineImg = productLine.i;  
            $scope.unitTypes = productLine.id.unitTypes;
            $scope.steps.step2=true;
        }
        
        // user selected unit type
        $scope.shelfTypes = [];
        $scope.selectUnitType = function(unitType)
        {
            $scope.configuration.unitType = unitType.id;                
            $scope.shelfTypes = $scope.configuration.productLine.shelfTypes; //productLine.id.shelfTypes;
            $scope.steps.step3=true;
        }
        
        // user selected shelf type
        $scope.selectShelfType = function(shelfType)
        {
            $scope.configuration.shelfType = shelfType.id;                
            $scope.recommendedItems = $scope.configuration.productLine.recommendedItems;
            $scope.steps.step4=true;
        }

        // user recommended item
        $scope.selectRecommendedItem = function(recommendedItem)
        {
            $scope.configuration.accessories.push(recommendedItem.id);                
        }
        
        var sectionCount = 0;
        // this will use rules to determine the configuration
        $scope.determineConfiguration = function()
        {
            var section = $scope.determineSection();
            var ends = $scope.determineEnds();
            var base = $scope.determineBase();
            var shelf = $scope.determineShelves();
            var accessories = $scope.determineAccessories();
 
            $scope.configuration.bom = [];
            $scope.configuration.bom.push(section);
            $scope.configuration.bom.push(ends);
            $scope.configuration.bom.push(base);
            $scope.configuration.bom.push(shelf);
        }
        
        $scope.determineSection = function()
        {
            // determine how many sections of each we need...
            var width = 4;
            var section = {};
            section.material = $scope.configuration.unitType.CODE + width + $scope.configuration.sectionHeight;

            section.description = $scope.configuration.unitType.NAME + ' ' + width + ' feet wide' + $scope.configuration.sectionHeight + '" high';
            section.type = 'Section';
            
            // calculate the quantity
            section.count = parseFloat($scope.configuration.aisleLength) / 4;
            section.count = section.count.toFixed(0);
            section.count = parseFloat(section.count);
            sectionCount = section.count;
            
            section.quantity = sectionCount * parseFloat($scope.configuration.aisleQuantity);
            return section;
        }
        
        
        $scope.determineEnds = function()
        {
            // determine how many ends of each we need...
            var end = {};
            end.material = $scope.configuration.unitType.CODE + 'E' + $scope.configuration.sectionHeight;
            end.quantity = 2 * parseFloat($scope.configuration.aisleQuantity);
            end.description = $scope.configuration.unitType.NAME + ' End ' + $scope.configuration.sectionHeight + '" high';
            end.type = 'End';
            
            // calculate the quantity
            return end;
        }
        
        $scope.determineShelves = function()
        {
            // determine how many ends of each we need...
            var shelf = {};
            shelf.material = $scope.configuration.shelfType.NAME + '48' + $scope.configuration.shelfDepth;
            shelf.quantity = $scope.configuration.shelfCount * sectionCount * parseFloat($scope.configuration.aisleQuantity);
            shelf.description = $scope.configuration.shelfType.NAME + '48" wide' + $scope.configuration.shelfDepth + '" deep';
            shelf.type = 'Shelf';
            // calculate the quantity
            return shelf;
        }
        
        $scope.determineAccessories = function()
        {
            // determine how many ends of each we need...
            /*var accessory = {};
            accessory.material = $scope.configuration.shelfType.NAME + '48' + $scope.configuration.shelfDepth;
            accessory.quantity = $scope.configuration.shelfCount * sectionCount * parseFloat($scope.configuration.aisleQuantity);
            accessory.description = $scope.configuration.shelfType.NAME + '48" wide' + $scope.configuration.shelfDepth + '" deep';
            accessory.type = 'Accessory';
            // calculate the quantity
            return accessory;*/
        }
        
        $scope.determineBase = function()
        {
            // determine the base configuration
            var base = {};
            
            var backingCode = '';
            // convert backing to a code.
            if ($scope.configuration.unitBacking == 'PEG')
                {
                    backingCode = 'P';
                }
            if ($scope.configuration.unitBacking == 'SOLID')
                {
                    backingCode = 'H';
                }
            if ($scope.configuration.unitBacking == 'SLATWALL')
                {
                    backingCode = 'H';
                }
            
            base.material = '5' + $scope.configuration.baseDepth + $scope.configuration.baseDepth + ' ' + backingCode;
            base.quantity = sectionCount * parseFloat($scope.configuration.aisleQuantity);
            base.type = 'Base';
            base.description = $scope.configuration.unitBacking + ' backing with ' + $scope.configuration.baseDepth + '" deep' + ' 5" high';
            // calculate the quantity
            return base;
        }
        
		$scope.showGuideScreen = function(itemNbr)
		{
            
            // initialize fields
            $scope.configuration = {};
            
            $scope.currentItem = $scope.determineItemNumber(itemNbr);
            
            var customer = $scope.priceDetails[$scope.currentItem].CUSTOMER;
            if (!customer)
            {
                showMessage('please select a customer first', '');
                return;
            }
            
          //now based on the customer selection, filter our material list to just those
            //for the selected customer.
            $scope.filteredMaterials = $scope.filterMaterialList(itemNbr);
        
            // always default to top materials.
            $scope.topMaterials = true;
            $scope.allMaterials = false;	

            //set the grid to have the full material list
            //$scope.gridOptions.data = $rootScope.materialList;
            //$scope.redrawTable();            
            //$scope.setFocus('scanInput');
			$('#guideScreen').show(300);
			$('#createScreen').hide();			
		}
        
        
        // this will finish the configuration and load the results into the order items box
        $scope.finishConfiguration = function()
        {
            for (var x = 0; x < $scope.configuration.bom.length; x++)
                {
                  // add a new item to the order for each entry.
                  var productRow = $scope.configuration.bom[x];
                  //console.log(productRow.toString());
                  var productId = productRow.material;
                  var quantity = productRow.quantity;
                  // call logic to add to an item;
                  $scope.addWizardItem(productId, quantity, productRow.type, productRow.description);

                   // now since we are at the last item, then add one more...
                  if (x+1 != $scope.configuration.bom.length)
                      {
                        $scope.addItems(); //add next item.
                      }
                }
            
            // now calculate total revenue
            $scope.calculateTotalRevenue();
            
            $scope.closeGuideScreen();
            
        }
        
        $scope.filteredMaterials = new Array();
		$scope.showMaterialList = function(itemNbr)
		{
            // initialize filter fields
            $scope.searchMaterial = '';
            $scope.scannedMaterial = '';
            
            $scope.currentItem = $scope.determineItemNumber(itemNbr);
            
            var customer = $scope.priceDetails[$scope.currentItem].CUSTOMER;
            if (!customer)
            {
                showMessage('please select a customer first', '');
                return;
            }
            
            //now based on the customer selection, filter our material list to just those
            //for the selected customer.
            $scope.filteredMaterials = $scope.filterMaterialList(itemNbr);
        
            // always default to top materials.
            $scope.topMaterials = true;
            $scope.allMaterials = false;	
            
			$('#materialListScreen').show(300);
			$('#createScreen').hide();	



            //set the grid to have the full material list
            $scope.gridOptions.data = $rootScope.materialList;
            /*$scope.gridApi.grid.refresh();
            $scope.gridApi.handleWindowResize();
            $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.ALL);
            */
            
            $scope.redrawTable();
            
            $scope.setFocus('scanInput');

		}
                
        // we will use the item number to filter
        $scope.filterMaterialList = function(itemNbr)
        {
            var materials = new Array();
			var listLength = $rootScope.materialList.length;
			for (x = 0; x < listLength; x++)
			{

                    if ($rootScope.materialList[x].MATERIAL_TYPE != 'KMAT')
                    {
                        materials.push($rootScope.materialList[x]);
                    }
                    
			}            
            return materials;
        }
        
        // we will use the item number to filter
        $scope.filterMaterialListCustomer = function(itemNbr)
        {
            $scope.currentItem = $scope.determineItemNumber(itemNbr);
            
            var materials = new Array();
            var customer = $scope.priceDetails[$scope.currentItem].CUSTOMER;
            if (!customer)
            {
                showMessage('please select a customer first');
                return;
            }
            
			// for the initial cart, we default to 5 line items.
			var listLength = $rootScope.customerMaterials.length;
			for (x = 0; x < listLength; x++)
			{

                    if (customer == $rootScope.customerMaterials[x].CUSTOMER)
                    {
                        materials.push($rootScope.customerMaterials[x]);
                    }
                    
			}            
            return materials;
        }

        $scope.redrawTable = function()
        {
            $timeout(function(){
            $('.table').trigger('footable_redraw');
            //console.log('redrawing table');
            }, 100);
        }
        
        
        // sets the type of request.
        $scope.setPriceType = function(priceType)
        {
         $scope.requestType = priceType;  
            
            /*if ($scope.requestType == 'Configure')
            {
                var item = {};
                item.ItemNumber = 1;
                $scope.showGuideScreen({id:item.ItemNumber})
            }*/
        }
        
		$scope.closeCustomerList = function()
		{
			$('#createScreen').show(300);
			$('#customerListScreen').hide();			
		}

        $scope.closeSoldToList = function()
		{
			$('#createScreen').show(300);
			$('#soldToListScreen').hide();			
		}
        
        $scope.closeOpportunityList = function()
		{
			$('#createScreen').show(300);
			$('#opportunityListScreen').hide();			
		}
        
  		$scope.closeGuideScreen = function()
		{
			$('#createScreen').show(300);
			$('#guideScreen').hide();			
		}      
        
		$scope.closeMaterialList = function()
		{
			$('#createScreen').show(300);
			$('#materialListScreen').hide();			
		}
        
		$scope.closeMaterialWizard = function()
		{
			$('#createScreen').show(300);
			$('#materialWizardScreen').hide();			
		}
        
        // default topMAterials to true
        $scope.topMaterials = true;
		$scope.showAllMaterials = function()
		{
            $scope.topMaterials = false;
            $scope.allMaterials = true;		
		}

		$scope.showTopMaterials = function()
		{
            $scope.topMaterials = true;
            $scope.allMaterials = false;		
		}
    
        $scope.selectSoldTo = function(customer)
		{
			$('#soldToListScreen').hide(0);
			$('#createScreen').show(300);
	
            $scope.soldTo = customer.id.CUSTOMER;
            $scope.soldToName = customer.id.CUSTOMER_NAME;  
            $scope.soldToCity = customer.id.CITY;
            $scope.soldToRegion = customer.id.REGION;
            
            // automatically populate the customer on the quote items
            for (var x=0; x<$scope.priceDetails.length; x++)
                {
                    $scope.priceDetails[x].CUSTOMER = customer.id.CUSTOMER;
                    $scope.priceDetails[x].CUSTOMER_NAME = customer.id.CUSTOMER_NAME;
                    $scope.priceDetails[x].CUSTOMER_CITY = customer.id.CITY;
                    $scope.priceDetails[x].CUSTOMER_REGION = customer.id.REGION;

                    //trigger a reprice for all the materials.
                    var itemNbr = {};
                    itemNbr.id = $scope.priceDetails[x].ItemNumber;
                    $scope.simulateQuoteLineItem(itemNbr); //simulate the quote  
                }
            
                // build the material list
                var listLength = $rootScope.customerMaterials.length;
                $scope.customerMaterialList.length = 0;
                for (x = 0; x < listLength; x++)
                {
                    if ($scope.soldTo == $rootScope.customerMaterials[x].CUSTOMER)
                    {
                        $scope.customerMaterialList.push($rootScope.customerMaterials[x]);
                    }
                }   
            
                // redraw the table to correctly setup for responsiveness
                $scope.redrawTable();
                console.log('found ' + $scope.customerMaterialList.length + ' materials for customer');
		}
        
        $scope.selectOpportunity = function(opportunity)
		{
			$('#opportunityListScreen').hide(0);
			$('#createScreen').show(300);
	
			$scope.getOpportunity(opportunity.id.OPPORTUNITYID);
		}
        
        $scope.selectCustomer = function(customer)
		{
			$('#customerListScreen').hide(0);
			$('#createScreen').show(300);
	
            $scope.priceDetails[$scope.currentItem].CUSTOMER = customer.id.CUSTOMER;
            $scope.priceDetails[$scope.currentItem].CUSTOMER_NAME = customer.id.CUSTOMER_NAME;
            $scope.priceDetails[$scope.currentItem].CUSTOMER_CITY = customer.id.CITY;
            $scope.priceDetails[$scope.currentItem].CUSTOMER_REGION = customer.id.REGION;
            
		}
        
        $scope.scannedMaterial = '';
        $scope.scanMaterial = function()
        {
            var foundMaterial = false;
            
            // convert to upper case
            $scope.scannedMaterial = $scope.scannedMaterial.toUpperCase();
                        
         // this will check to see if a material matches.
            for (var x=0; x< $scope.filteredMaterials.length; x++)
                {
                    if ($scope.scannedMaterial == $scope.filteredMaterials[x].MATERIAL ||
                        $scope.scannedMaterial == $scope.filteredMaterials[x].UPC_CODE)
                        {
                            var material = {}
                            material.id = $scope.filteredMaterials[x];
                            $scope.selectMaterial(material);
                            // found it.
                            foundMaterial = true;
                            break;
                        }
                }
            
            // if we haven't found the material, then read the entire material list.
            if (foundMaterial == false)
            {
                var materialListFullLength = $scope.materialList.length;
                for (var x=0; x< materialListFullLength; x++)
                {
                    if ($scope.scannedMaterial == $scope.materialList[x].MATERIAL ||
                        $scope.scannedMaterial == $scope.materialList[x].UPC_CODE)
                        {
                            var material = {}
                            material.id = $scope.materialList[x];
                            $scope.selectMaterial(material);
                            // found it.
                            foundMaterial = true;
                            break;
                        }
                }
            }
            
            if (foundMaterial == false)
                {
                    //showMessage('Unable to find the material in our catalog, please correct or contact IT to add it to the catalog');
                }
        }
        
        // from excel upload of program or template.
        $scope.uploadedMaterial = '';
        $scope.uploadExcel = function()
        {
            
        }
        

        $scope.typeMaterial = function(itemNbr)
        {
         // this will check to see if a material matches.
            var rowIndex = $scope.determineItemNumber(itemNbr);
            $scope.currentItem = rowIndex;
            
            var customer = $scope.priceDetails[$scope.currentItem].CUSTOMER;
            if (!customer)
            {
                $scope.priceDetails[rowIndex].MATERIAL = '';
                showMessage('Please select a customer first', '');
                return;
            }
            
            var material = $scope.priceDetails[rowIndex].MATERIAL;
            var foundMaterial = false;
            
            // initialize the fields
            $scope.priceDetails[$scope.currentItem].DESCRIPTION = null; 
            $scope.priceDetails[$scope.currentItem].BASE_UOM = null;
            $scope.priceDetails[$scope.currentItem].BID_PRICE_UOM = null  
            $scope.priceDetails[$scope.currentItem].DISCOUNT_PCT = 0; 
            $scope.priceDetails[$scope.currentItem].IS_FOUND = false; 
            $scope.priceDetails[$scope.currentItem].isLoading = false; 
            $scope.priceDetails[$scope.currentItem].EXCEPTION1_PRICE = null;
            $scope.priceDetails[$scope.currentItem].EXCEPTION2_PRICE = null; 
            $scope.priceDetails[$scope.currentItem].STRETCH_PRICE = null; 
            $scope.priceDetails[$scope.currentItem].LIST_PRICE = null;
            $scope.priceDetails[$scope.currentItem].CONTRACT_PRICE = null;
            $scope.priceDetails[$scope.currentItem].CSP_PRICE = null;
            $scope.priceDetails[$scope.currentItem].TBL_LAST_ORDERS = null;
            $scope.priceDetails[$scope.currentItem].ATP_INFO = null;
            $scope.priceDetails[$scope.currentItem].BASIC_INFO = null;
            $scope.priceDetails[$scope.currentItem].TARGET_PRICE_UOM = null;
            
            // convert to upper case
            material = material.toUpperCase();
            
            if (material == "")
                {
                    return;
                }
            
            // loop for matches in the customer material list
            for (var x=0; x< $scope.customerMaterialList.length; x++)
                {
                    if (material == $scope.customerMaterialList[x].MATERIAL || 
                        material == $scope.customerMaterialList[x].UPC_CODE)
                        {
                            var material = {}
                            material.id = $scope.customerMaterialList[x];
                            $scope.selectMaterial(material);
                            // found it.
                            foundMaterial = true;
                            break;
                        }
                }
            
            // if we haven't found the material, then read the entire material list for UPC Code at 12 chars.
            if (foundMaterial == false)
            {
                var length = material.length;
                if (length == 12)//upc code, then try to find the material
                {
                    var materialListFullLength = $scope.materialList.length;
                    for (var x=0; x< materialListFullLength; x++)
                    {
                        if (material == $scope.materialList[x].UPC_CODE)
                            {
                                var material = {}
                                material.id = $scope.materialList[x];
                                $scope.selectMaterial(material);
                                // found it.
                                foundMaterial = true;
                                break;
                            }
                    }
                }
            }
            
            // if we haven't found the material, then read the entire material list.
            if (foundMaterial == false)
            {
                    var materialListFullLength = $scope.materialList.length;
                    for (var x=0; x< materialListFullLength; x++)
                    {
                        if (material == $scope.materialList[x].MATERIAL)
                            {
                                var material = {}
                                material.id = $scope.materialList[x];
                                $scope.selectMaterial(material);
                                // found it.
                                foundMaterial = true;
                                break;
                            }
                    }
            }

            
        }
        
        // recalculate revenue
        $scope.quantityChanged = function()
        {
            $scope.calculateTotalRevenue();
        }
        
        $scope.selectMaterial = function(material)
		{
            
			$('#materialListScreen').hide(0);
			$('#createScreen').show(300);
	
            // based on materialList
            $scope.priceDetails[$scope.currentItem].MATERIAL = material.id.MATERIAL;
            $scope.priceDetails[$scope.currentItem].DESCRIPTION = material.id.DESCRIPTION;
            $scope.priceDetails[$scope.currentItem].BASE_UOM = material.id.BASE_UOM;  
            $scope.priceDetails[$scope.currentItem].BID_PRICE = material.id.LIST_PRICE; 
            $scope.priceDetails[$scope.currentItem].EXPECTED_SALES = 1;  //default to 1 unit
            $scope.priceDetails[$scope.currentItem].EXPECTED_REVENUE = $scope.priceDetails[$scope.currentItem].BID_PRICE * $scope.priceDetails[$scope.currentItem].EXPECTED_SALES;  //default to 1 unit
            $scope.priceDetails[$scope.currentItem].BID_PRICE_UOM = material.id.BASE_UOM;  
            $scope.priceDetails[$scope.currentItem].DISCOUNT_PCT = 0; 
            $scope.priceDetails[$scope.currentItem].IS_FOUND = true; 
            $scope.priceDetails[$scope.currentItem].isLoading = false; 
            var itemNbr = {};
            itemNbr.id = $scope.priceDetails[$scope.currentItem].ItemNumber;
            
            //$scope.simulateQuoteLineItem(itemNbr); //simulate the quote

            $scope.calculateTotalRevenue();
            
            // redraw the table to correctly setup for responsiveness
            $scope.redrawTable();
        }
        
        $scope.deleteItem = function(itemNbr)
        {
         
          if (itemNbr.id > -1) {
              for (var x=0; x< $scope.priceDetails.length; x++)
                  {
                    // delete the selected item number
                    if (itemNbr.id == $scope.priceDetails[x].ItemNumber)
                        {
                        $scope.priceDetails.splice(x, 1);
                        // recalculate score
                        $scope.calculateQuoteScore();
                        }
                  }
            }
        }
    
        $scope.materialInfoData = {};
    
        $scope.materialInfo = function(itemNbr)
        {
         
          if (itemNbr.id > -1) {
              for (var x=0; x< $scope.priceDetails.length; x++)
                  {
                    // delete the selected item number
                    if (itemNbr.id == $scope.priceDetails[x].ItemNumber)
                        {
                            $scope.materialInfoData = $scope.priceDetails[x];
                        // show the material info slider
                            if (angular.element('#materialInfoDrawer').hasClass('active') == true)
                            {
                               // let it stay active
                            }
                            else //element needs to be active
                            {
                                angular.element('#materialInfoDrawer').addClass('active'); 
                            }
                            
                            break; //stop processing, all good here.
                        }
                  }
            }
        }

        $scope.addItems = function()
        {
            $scope.buildInitialCart(true);
            
            // automatically populate the sold to customers
            for (var x=0; x<$scope.priceDetails.length; x++)
                {
                $scope.priceDetails[x].CUSTOMER = $scope.soldTo;
                $scope.priceDetails[x].CUSTOMER_NAME = $scope.soldToName;
                $scope.priceDetails[x].CUSTOMER_CITY = $scope.soldToCity;
                $scope.priceDetails[x].CUSTOMER_REGION = $scope.soldToRegion;
                $scope.priceDetails[x].isLoading = false;
                }
            
            // redraw the table to correctly setup for responsiveness
            $scope.redrawTable();
            
        }
        
        $scope.copyItem = function(itemNbr)
        {
            var newItem = angular.copy($scope.priceDetails[itemNbr.id - 1]);
            
            var lastRow = $scope.priceDetails.length;
            lastRow = lastRow - 1;
            newItem.ItemNumber = parseInt($scope.priceDetails[lastRow].ItemNumber + 1); 
            $scope.priceDetails.push(newItem);
        }
        
        $scope.simulateQuoteLineItem = function(itemNbr)
        {
            /* do not call for distributor version
            var rowIndex = $scope.determineItemNumber(itemNbr);
            var priceRequestItem = $scope.priceDetails[rowIndex];
            
            // check to make sure we have a material
            if ($scope.priceDetails[rowIndex].MATERIAL == "")
                {
                    return;// exit if we don't have a material
                }
            $scope.priceDetails[rowIndex].isLoading = true;
            PriceService.simulateQuoteItem(priceRequestItem).then(function(d) {
                    var simulationResults = d;
                    var itemNbr = {};
                    itemNbr.id = simulationResults.E_ITEM_NBR;
                
                    var rowIndex = $scope.determineItemNumber(itemNbr);
                  
                // build items into the cart if they are valid.   
                    $scope.priceDetails[rowIndex].isLoading = false;
                    $scope.priceDetails[rowIndex].TARGET_PRICE = simulationResults.TARGET_PRICE;
                    $scope.priceDetails[rowIndex].TARGET_PRICE_UOM = simulationResults.TARGET_PRICE_UOM;

                    if (parseFloat(simulationResults.TARGET_PRICE) == 0)
                        {
                            $scope.priceDetails[rowIndex].noPrice = true; 
                        }
                
                    // no target price found... then copy the current bid price if it is specified
                    if (parseFloat($scope.priceDetails[rowIndex].BID_PRICE) != 0 && parseFloat(simulationResults.TARGET_PRICE) == 0)
                        {
                            simulationResults.TARGET_PRICE = $scope.priceDetails[rowIndex].BID_PRICE;  
                        }
                
                    // default bid price to the Target
                    // do not do this if we are in change mode.
                    if ($scope.priceScenario != 'EDIT')
                        {
                            $scope.priceDetails[rowIndex].BID_PRICE = simulationResults.TARGET_PRICE;  
                        }
                
                    $scope.priceDetails[rowIndex].EXCEPTION1_PRICE = simulationResults.FLOOR1_PRICE; 
                    $scope.priceDetails[rowIndex].EXCEPTION2_PRICE = simulationResults.FLOOR2_PRICE;  
                    $scope.priceDetails[rowIndex].STRETCH_PRICE = simulationResults.STRETCH_PRICE; 
                    $scope.priceDetails[rowIndex].LIST_PRICE = simulationResults.LIST_PRICE;  
                    $scope.priceDetails[rowIndex].CONTRACT_PRICE = simulationResults.CONTRACT_PRICE;
                    $scope.priceDetails[rowIndex].CSP_PRICE = simulationResults.CURRENT_PRICE;
                
                    // add material info data.
                    $scope.priceDetails[rowIndex].TBL_LAST_ORDERS = simulationResults.TBL_LAST_ORDERS;
                    $scope.priceDetails[rowIndex].ATP_INFO = simulationResults.ATP_INFO;
                    $scope.priceDetails[rowIndex].BASIC_INFO = simulationResults.BASIC_INFO;

                
                    $scope.bidPriceChange(itemNbr);
            });
            */
            
        }
               
        $scope.discountSliderChange = function(sliderElement)
        {
          var itemNbr = {};
            var itemId = sliderElement.id.split('_');
            itemId = parseInt(itemId[1]);
            itemNbr.id = itemId;
            $scope.discountChange(itemNbr);
        }
            
        $scope.discountChange = function(itemNbr)
        {
            $scope.currentItem = $scope.determineItemNumber(itemNbr);
            var targetPrice = parseFloat($scope.priceDetails[$scope.currentItem].TARGET_PRICE);
            var bidPrice = parseFloat($scope.priceDetails[$scope.currentItem].BID_PRICE);
            var discountPct = parseFloat($scope.priceDetails[$scope.currentItem].DISCOUNT_PCT);
            if (discountPct > 0)// surcharge
                {
                    bidPrice = targetPrice * (1 + ((discountPct/100) * 1));
                    $scope.priceDetails[$scope.currentItem].BID_PRICE = bidPrice;
                    $scope.priceDetails[$scope.currentItem].BID_PRICE = $scope.priceDetails[$scope.currentItem].BID_PRICE.toFixed(2);
                }
            if (discountPct < 0)
                {
                    bidPrice = targetPrice - (((discountPct/100) * -1) * targetPrice);
                    $scope.priceDetails[$scope.currentItem].BID_PRICE = bidPrice;
                    $scope.priceDetails[$scope.currentItem].BID_PRICE = $scope.priceDetails[$scope.currentItem].BID_PRICE.toFixed(2);
                }
            if (discountPct == 0)
                {
                    bidPrice = targetPrice;
                    $scope.priceDetails[$scope.currentItem].BID_PRICE = bidPrice;
                    $scope.priceDetails[$scope.currentItem].BID_PRICE = $scope.priceDetails[$scope.currentItem].BID_PRICE.toFixed(2);
                }
            
            
            $scope.bidPriceChange(itemNbr);
            
            $scope.$digest();
            
            console.log('New Bid Price is ' + bidPrice + 'Array bid price is ' + $scope.priceDetails[$scope.currentItem].BID_PRICE);
        }
        
        // updates target price index.
        $scope.bidPriceChange = function(itemNbr)
        {
            $scope.currentItem = $scope.determineItemNumber(itemNbr);
            
            if ($scope.priceDetails[$scope.currentItem].MATERIAL == "")
                {
                    return;
                }
            
            var bidPrice = parseFloat($scope.priceDetails[$scope.currentItem].BID_PRICE);
            var targetPrice = parseFloat($scope.priceDetails[$scope.currentItem].TARGET_PRICE);
            
            // if we don't have a target, then assume that the bid price = target to enable 
            // quote scoring to continue.  this code now always changes it...
                if ($scope.priceDetails[$scope.currentItem].hasOwnProperty('noPrice'))
                {
                    $scope.priceDetails[$scope.currentItem].TARGET_PRICE = $scope.priceDetails[$scope.currentItem].BID_PRICE;
                    targetPrice = bidPrice;
                }


            var exception1 = parseFloat($scope.priceDetails[$scope.currentItem].EXCEPTION1_PRICE);
            var exception2 = parseFloat($scope.priceDetails[$scope.currentItem].EXCEPTION2_PRICE);
            
            var bidPriceNumber = isNaN(bidPrice);
            
            $scope.priceDetails[$scope.currentItem].btnClass = 'default';
            $scope.priceDetails[$scope.currentItem].TPI_CLASS = $scope.priceDetails[$scope.currentItem].btnClass;
            //label-success
            
            if (bidPriceNumber == true) // exit if we don't have a number
            {
                $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX = 0;
                return;
            }
            
            if (targetPrice == 0)// the TPI must be 0
                {
                    $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX = 0
                }
            else //calculate it.
                {
                    $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX = (bidPrice/targetPrice) * 100;    
                }

            $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX = 
            $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX.toFixed(2);
            
            var tpiLocal = $scope.priceDetails[$scope.currentItem].TARGET_PRICE_INDEX;
   
            if (tpiLocal > 80)
            {
                $scope.priceDetails[$scope.currentItem].btnClass = 'success';
            }
            else if (tpiLocal > 60)
            {
                $scope.priceDetails[$scope.currentItem].btnClass = 'warning';  
            }
            else if (tpiLocal >= 0)
            {
                $scope.priceDetails[$scope.currentItem].btnClass = 'danger';  
            }
            
            $scope.priceDetails[$scope.currentItem].TPI_CLASS = $scope.priceDetails[$scope.currentItem].btnClass;
            
            $scope.calculateQuoteScore();

        }
        
        
        $scope.calculateQuoteScore = function calculateQuoteScore()
        {
            
            // now calculate total revenue
            $scope.calculateTotalRevenue();
        
            // now generate the quote score
            var totalRevenueNbr = parseFloat($scope.totalRevenue);
            var totalTargetNbr = parseFloat($scope.totalTargetValue);
            var totalTPIScore =  (totalRevenueNbr / totalTargetNbr) * 100;
            
            // if we have no target value, then default to 100.
            var isNotANumber = isNaN(totalTPIScore);
            if (isNotANumber == true) // then just calculate without revenue
                {
                    // loop through all tpi's to calculate an average
                    var totalTPI = 0;
                    var totalItems = 0;
                    var priceLength = $scope.priceDetails.length;
                    for (var y = 0; y < priceLength; y++)
                    {
                         if (parseFloat($scope.priceDetails[y].TARGET_PRICE_INDEX) > 0)
                         {
                             totalItems = totalItems + 1;
                             totalTPI = totalTPI + parseFloat($scope.priceDetails[y].TARGET_PRICE_INDEX);
                         }
                    }
                    
                    if (totalItems > 0)
                        {
                            var totalTPIScore = totalTPI / totalItems.toFixed(2);
                        }
                    else{ //items have 0 TPI, header must be 0 too
                            var totalTPIScore = 0;
                        }

                }

            $scope.totalTPIValue = totalTPIScore;
            totalTPIScore = parseFloat(totalTPIScore); //convert it to a number
            
            $scope.totalTPIValue = $scope.totalTPIValue.toFixed(1);
            
            // update gauge
            /*var target = document.getElementById('foo'); // your canvas element
            var gauge = new Gauge(target);

            //console.log('updating gauge')
            gauge.setOptions(opts); // create sexy gauge!
            gauge.maxValue = 141; // set max gauge value
            gauge.animationSpeed = 32; // set animation speed (32 is default value)
            
           // if we are at 0, then set it to 20 to enable the gauge to render
            if (totalTPIScore == 0)
                {
                    totalTPIScore = 20;
                }
            
            gauge.set(totalTPIScore); // set actual value
            */
            
        }
        

        $scope.commissionPercent = .10;
    
        $scope.calculateTotalRevenue = function()
        {
            // loop through all tpi's to calculate an average
            var priceLength = $scope.priceDetails.length;
            var totalRevenueValue = 0;
            var totalTargetValue = 0;
            for (var y = 0; y < priceLength; y++)
            {
                var bidPriceNumber = parseFloat($scope.priceDetails[y].BID_PRICE);
                var targetPriceNumber = parseFloat($scope.priceDetails[y].TARGET_PRICE);
                
                if (typeof $scope.priceDetails[y].EXPECTED_SALES == 'string')
                    {
                var expectedSalesString = $scope.priceDetails[y].EXPECTED_SALES;
                expectedSalesString = expectedSalesString.replace(',', '');
                var expectedSalesNumber = parseFloat(expectedSalesString);
                    }
                else
                    {
                        var expectedSalesNumber = $scope.priceDetails[y].EXPECTED_SALES;
                    }
                 if (bidPriceNumber >= 0 && expectedSalesNumber >= 0)
                 {
                     totalRevenueValue = totalRevenueValue + 
                        (bidPriceNumber *
                         expectedSalesNumber);
                     
                     totalTargetValue = totalTargetValue + 
                        (targetPriceNumber *
                         expectedSalesNumber);
                     
                     $scope.priceDetails[y].EXPECTED_REVENUE = (bidPriceNumber * expectedSalesNumber);
                     $scope.priceDetails[y].EXPECTED_REVENUE = $scope.priceDetails[y].EXPECTED_REVENUE.toFixed(0);  
                     
                     $scope.priceDetails[y].COMMISSION = $scope.priceDetails[y].EXPECTED_REVENUE * $scope.commissionPercent;
                     $scope.priceDetails[y].COMMISSION = $scope.priceDetails[y].COMMISSION.toFixed(0);                       
                 }
            }
            $scope.totalRevenue = totalRevenueValue.toFixed(0);  
            $scope.totalTargetValue = totalTargetValue.toFixed(0);  
            $scope.totalCommission = parseFloat($scope.totalRevenue * $scope.commissionPercent).toFixed(0);                         
            // redraw the table to correctly setup for responsiveness
            // need to see if we can change to support iphones.
            //$scope.redrawTable();
        }
		
		// this is used to load data from a quote in case of a quote copy
		$scope.loadQuoteData = function loadQuoteData(quoteNum)
		{
			$scope.quote = QuoteService.getQuote(quoteNum);		
		}

        $scope.opportunity = {};
        $scope.opportunityDetails = [];
    
		//read details based on the opp selected
		$scope.getOpportunity = function (id) {
			$scope.opportunity = OpportunityService.getOpportunity(id);
            
            var customer = {};
            customer.id = {};
            
            // look up the SAP number
            for (var x=0; x<$rootScope.customerList.length; x++)
                {
                    if ($rootScope.customerList[x].CUSTOMER == $scope.opportunity.ACCOUNTID)
                        {
                            customer.id.CUSTOMER = $rootScope.customerList[x].CUSTOMER; 
                            customer.id.CITY = $rootScope.customerList[x].CITY;
                            customer.id.REGION = $rootScope.customerList[x].REGION;
                            customer.id.CUSTOMER_NAME = $rootScope.customerList[x].CUSTOMER_NAME;    
                           break;
                        }
                }
            $scope.selectSoldTo(customer);
            
            // automatically populate the key opportunity fields
            for (var x=0; x<$scope.priceDetails.length; x++)
                {
                $scope.priceDetails[x].OPPORTUNITY = $scope.opportunity.OPPORTUNITYID;
                $scope.priceDetails[x].OPPORTUNITY_NUM = $scope.opportunity.OPPORTUNITYID; 
                $scope.priceDetails[x].OPPORTUNITY_NAME = $scope.opportunity.OPPORTUNITYNAME;  
                }
            
            // do not do direct calls of the opportunity
            /*OpportunityService.synchOpportunityDetails($scope.opportunity).then(function(d) {
                $scope.opportunityDetails = d;
                // build items into the cart if they are valid.
                
            });*/
            
    	}
        
		//read details based on the price request selected
		$scope.getPrice = function (id) {
			
			$scope.priceDetails = PriceService.getPrice(id);
            $scope.price = $scope.priceDetails[0];
                        
            var day = $scope.price.EFFECTIVE_DATE.slice(8,10);
            var month = $scope.price.EFFECTIVE_DATE.slice(5,7);
            var year = $scope.price.EFFECTIVE_DATE.slice(0,4);
            $scope.effectiveDate = month + '/' + day + '/' + year;
    
            var day = $scope.price.EXPIRATION_DATE.slice(8,10);
            var month = $scope.price.EXPIRATION_DATE.slice(5,7);
            var year = $scope.price.EXPIRATION_DATE.slice(0,4);
            $scope.expirationDate = month + '/' + day + '/' + year;
            
            for (var x=0; x< $scope.priceDetails.length; x++)
                { 
                    $scope.priceDetails[x].ItemNumber = $scope.priceDetails[x].ITM_NUMBER;
                    $scope.priceDetails[x].OPPORTUNITY_NUM = $scope.priceDetails[x].OPPORTUNITY_NUM;  
                    $scope.priceDetails[x].DESCRIPTION = $scope.priceDetails[x].SHORT_TEXT;  
                    $scope.priceDetails[x].IS_FOUND = true;
                    var itemNbr = {}; //object.
                    itemNbr.id = $scope.priceDetails[x].ItemNumber;
                    $scope.simulateQuoteLineItem(itemNbr);
                }
            
            // populate opportunity information
            $scope.opportunity = {};
            $scope.opportunity.OPPORTUNITYID = $scope.price.OPPORTUNITY_NUM;
            //$scope.opportunity.Name = {};
            $scope.opportunity.OPPORTUNITYNAME = $scope.price.OPPORTUNITY_NAME;
            

            // populate customer information
            var customer = {};
            customer.id = {};
            for (var x=0; x<$rootScope.customerList.length; x++)
                {
                    if ($rootScope.customerList[x].CUSTOMER == $scope.price.CUSTOMER)
                        {
                            customer.id.CUSTOMER = $rootScope.customerList[x].CUSTOMER; 
                            customer.id.CITY = $rootScope.customerList[x].CITY;
                            customer.id.REGION = $rootScope.customerList[x].REGION;
                            customer.id.CUSTOMER_NAME = $rootScope.customerList[x].CUSTOMER_NAME;    
                           break;
                        }
                }
            $scope.selectSoldTo(customer);
            
            // dont use the effective and expiration dates as they will be in the past.
            // the price details will come in from the application.
            
            
    	}
        
        var currentItem = 0;
		$scope.buildInitialCart = function buildInitialCart(addItems)
		{
			// for the initial cart, we default to 5 line items.
			var reqLength = 1;
            
            if (addItems = true)
            {
                
            }
            else
            {
			 $scope.priceDetails.length = 0;
            }
            
            var today = new Date();
			var requestedDate = (today.getFullYear()) +  '-'
            		 + ('0' + (today.getMonth()+1)).slice(-2) + '-'
            		 + ('0' + today.getDate()).slice(-2);
            
			for (x = 0; x < reqLength; x++)
			{

                    var cartRow = new Object();
                    cartRow.ItemNumber = currentItem += 1; 
                    if ($scope.priceScenario == 'EDIT')
                        {
                            cartRow.PRICE_REQUEST = $scope.request_id;
                        }
                    cartRow.REQUEST_TYPE = $scope.requestType;
                    cartRow.REQUESTED_BY = 'CBENSON';
                    cartRow.CUSTOMER = '';
                    cartRow.CUSTOMER_NAME = '';
                    cartRow.SALES_ORG = '1000';
                    cartRow.DISTR_CHAN = '10';
                    cartRow.DIVISION = '00';
                    cartRow.BID_PRICE = 0;
                    cartRow.PRICE_UNIT = '';
                    cartRow.TARGET_PRICE = 0;
                    cartRow.TARGET_PRICE_INDEX = 0;
                    cartRow.EXPECTED_SALES = '';
                    cartRow.CURRENT_PRICE = 0;
                    cartRow.EXPIRATION_DATE;
                    cartRow.EFFECTIVE_DATE;
                    cartRow.REQUEST_DATE = requestedDate;
                    cartRow.REQUEST_STATUS;
                    cartRow.COMMENTS;
                    cartRow.DISCOUNT_PCT = 0;
                    cartRow.isLoading = false;
                    cartRow.MATERIAL = ''; //equipmentConsumablesList[y].Material;
                    cartRow.DESCRIPTION = '';// equipmentConsumablesList[y].Description;
                
                $scope.priceDetails.push(cartRow);
			}
		}
        

        
        // setup full material grid
        $scope.gridOptions = {
            showGridFooter: true,
            data: $rootScope.materialList,
            enableGridMenu: true,
            enableFiltering: false,
            flatEntityAccess: true,
            fastWatch: true,
            paginationPageSizes: [50, 100, 250,1000],
            paginationPageSize: 250,
            exporterAllDataFn: function() {
                return getPage(1, $scope.gridOptions.totalItems, paginationOptions.sort)
                .then(function() {
                  $scope.gridOptions.useExternalPagination = false;
                  $scope.gridOptions.useExternalSorting = false;
                  getPage = null;
                });
            }
        }
        
          $scope.gridOptions.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;
            $scope.gridApi.grid.registerRowsProcessor( $scope.singleFilter, 200 );
          }


          $scope.filter = function() {
            $scope.gridApi.grid.refresh();
          };
    
          $scope.singleFilter = function( renderableRows ){
              var upperCase;
              if ($scope.filterValue)
                  {
                    upperCase = $scope.filterValue.toUpperCase();
                  }

            var matcher = new RegExp($scope.filterValue);
            renderableRows.forEach( function( row ) {
              var match = false;
              [ 'DESCRIPTION', 'MATERIAL', 'UPC_CODE' ].forEach(function( field ){
                if ( row.entity[field].match(matcher) ){
                  match = true;
                }
                else if ( row.entity[field].match(upperCase) ){
                  match = true;
                }
              });
              if ( !match ){
                row.visible = false;
              }
            });
            return renderableRows;
          };
    
          $scope.gridOptions.columnDefs = [
            { name: ' ',
             width: 100,
             cellTemplate:  '<div style="text-align: center; margin-top:3px"><a ng-click="grid.appScope.selectGridRow(row)" class="btn btn-xs btn-info tooltips"> <i class="fa fa-edit"></i>Select</a></div>' },
            { name: 'Description', field: 'DESCRIPTION'},
            { name: 'Material', field: 'MATERIAL'},
            { name: 'UPC', field: 'UPC_CODE'}
          ];

        // goes to the detail page for the selected row
        $scope.selectGridRow = function(row){
            
            var material = {};
            material = row.entity; 
            $scope.selectMaterial({id:material});

        };

        $scope.reset = function()
        {
            // resets the form
            $scope.priceHeader = {};
            $scope.priceDetails = [];
            $scope.requestType = 'Direct';
            $scope.quote = {};
            $scope.currentItem = 0;
            $scope.totalRevenue = 0;
            $scope.totalCommission = 0;
            var Cart = [];

            $scope.requestStatus = 'NEW';
            $scope.soldTo = '';
            $scope.soldToName = '';
            $scope.totalTPIValue = 0;
            $scope.totalRevenue = 0;
        }

        // default scenario to CREATE
        $scope.priceScenario = 'CREATE';
		$scope.request_id = $stateParams.id; //$routeParams.id;
            
        // initialize the gauge
        $scope.buildGauge();
    
	//  functions to initialize the controller.
        $scope.buildInitialCart(); //build initial pricing cart
		
		// call function to get the price for the opportunity.
		if ($scope.request_id)
		{
            // determine if we copying the price request or an opportunity reference
            // if we have a request, then that means we are copying a prior request as part of
            // the create functionality.
		   if ($location.$$path.indexOf("quoteCopy") > -1)
		   {
              $scope.priceScenario = 'COPY';
		      $scope.getPrice($scope.request_id);//copy an existing quote... load the details
                   
                $scope.effectiveDate = new Date();    
                $scope.expirationDate = new Date();

                $scope.effectiveDate = ('0' + ($scope.effectiveDate.getMonth()+1)).slice(-2) + '/'
                                         + ('0' + $scope.effectiveDate.getDate()).slice(-2) + '/' +
                                        ($scope.effectiveDate.getFullYear());

                $scope.expirationDate.setDate($scope.expirationDate.getDate() + 30); //default to 30 days
                $scope.expirationDate = ('0' + ($scope.expirationDate.getMonth()+1)).slice(-2) + '/'
                                         + ('0' + $scope.expirationDate.getDate()).slice(-2) + '/' +
                                        ($scope.expirationDate.getFullYear());
               
		   }
		   else if ($location.$$path.indexOf("quoteEdit") > -1)
		   {
              $scope.priceScenario = 'EDIT';
		      $scope.getPrice($scope.request_id);//copy an existing quote... load the details
		   }
		   else //opportunity reference
		   {
              $scope.priceScenario = 'CREATE';
			  $scope.getOpportunity($scope.request_id);
		   }
            
		}

        // hide the selection screens
   	    $('#customerListScreen').hide(0);
        $('#materialListScreen').hide(0);
        $('#opportunityListScreen').hide(0);
        $('#guideScreen').hide(0);
        $('#soldToListScreen').hide(0);    
    
     // product builder
    function ProductBuilder( element ) {
		this.element = element;
		this.stepsWrapper = this.element.children('.cd-builder-steps');
		this.steps = this.element.find('.builder-step');
		//store some specific bulider steps
		this.models = this.element.find('[data-selection="models"]'); 
		this.summary;
		this.optionsLists = this.element.find('.options-list');
		//bottom summary 
		this.fixedSummary = this.element.find('.cd-builder-footer');
		this.modelPreview = this.element.find('.selected-product').find('img');
		this.totPriceWrapper = this.element.find('.tot-price').find('b');
		//builder navigations
		this.mainNavigation = this.element.find('.cd-builder-main-nav');
		this.secondaryNavigation = this.element.find('.cd-builder-secondary-nav');
		//used to check if the builder content has been loaded properly
		this.loaded = true;
		
		// bind builder events
		this.bindEvents();
        //alert('ready');
	}

	ProductBuilder.prototype.bindEvents = function() {
		var self = this;

		//detect click on the left navigation
		this.mainNavigation.on('click', 'li:not(.active)', function(event){
			event.preventDefault();
			self.loaded && self.newContentSelected($(this).index());
		});

		//detect click on bottom fixed navigation
		this.secondaryNavigation.on('click', '.nav-item li:not(.buy)', function(event){ 
			event.preventDefault();
			var stepNumber = ( $(this).parents('.next').length > 0 ) ? $(this).index() + 1 : $(this).index() - 1;
			self.loaded && self.newContentSelected(stepNumber);
		});
		//detect click on one element in an options list (e.g, models, accessories)
		this.optionsLists.on('click', '.js-option', function(event){
			self.updateListOptions($(this));
		});
		//detect clicks on customizer controls (e.g., colors ...)
		this.stepsWrapper.on('click', '.cd-product-customizer a', function(event){
			event.preventDefault();
			self.customizeModel($(this));
		});
	};
    
	ProductBuilder.prototype.newContentSelected = function(nextStep) {
		//first - check if a model has been selected - user can navigate through the builder
		if( this.fixedSummary.hasClass('disabled') ) {
			//no model has been selected - show alert
			this.fixedSummary.addClass('show-alert');
		} else {
			//model has been selected so show new content 
			//first check if the color step has been completed - in this case update the product bottom preview
			if( this.steps.filter('.active').is('[data-selection="colors"]') ) {
				//in this case, color has been changed - update the preview image
				var imageSelected = this.steps.filter('.active').find('.cd-product-previews').children('.selected').children('img').attr('src');
				this.modelPreview.attr('src', imageSelected);
			}
			//if Summary is the selected step (new step to be revealed) -> update summary content
			if( nextStep + 1 >= this.steps.length ) {
				this.createSummary();
			}
			
			this.showNewContent(nextStep);
			this.updatePrimaryNav(nextStep);
			this.updateSecondaryNav(nextStep);
		}
	}

	ProductBuilder.prototype.showNewContent = function(nextStep) {
		var actualStep = this.steps.filter('.active').index() + 1;
		if( actualStep < nextStep + 1 ) {
			//go to next section
			this.steps.eq(actualStep-1).removeClass('active back').addClass('move-left');
			this.steps.eq(nextStep).addClass('active').removeClass('move-left back');
		} else {
			//go to previous section
			this.steps.eq(actualStep-1).removeClass('active back move-left');
			this.steps.eq(nextStep).addClass('active back').removeClass('move-left');
		}
	}

	ProductBuilder.prototype.updatePrimaryNav = function(nextStep) {
		this.mainNavigation.find('li').eq(nextStep).addClass('active').siblings('.active').removeClass('active');
	}

	ProductBuilder.prototype.updateSecondaryNav = function(nextStep) {
		( nextStep == 0 ) ? this.fixedSummary.addClass('step-1') : this.fixedSummary.removeClass('step-1');

		this.secondaryNavigation.find('.nav-item.next').find('li').eq(nextStep).addClass('visible').removeClass('visited').prevAll().removeClass('visited').addClass('visited').end().nextAll().removeClass('visible visited');
		this.secondaryNavigation.find('.nav-item.prev').find('li').eq(nextStep).addClass('visible').removeClass('visited').prevAll().removeClass('visited').addClass('visited').end().nextAll().removeClass('visible visited');
	}

	ProductBuilder.prototype.createSummary = function() {
		var self = this;
		this.steps.each(function(){
			//this function may need to be updated according to your builder steps and summary
			var step = $(this);
			if( $(this).data('selection') == 'colors' ) {
				//create the Color summary
				var colorSelected = $(this).find('.cd-product-customizer').find('.selected'),
					color = colorSelected.children('a').data('color'),
					colorName = colorSelected.data('content'),
					imageSelected = $(this).find('.cd-product-previews').find('.selected img').attr('src');
				
				self.summary.find('.summary-color').find('.color-label').text(colorName).siblings('.color-swatch').attr('data-color', color);
				self.summary.find('.product-preview').attr('src', imageSelected);
			} else if( $(this).data('selection') == 'accessories' ) {
				var selectedOptions = $(this).find('.js-option.selected'),
					optionsContent = '';

				if( selectedOptions.length == 0 ) {
					optionsContent = '<li><p>No Accessories selected;</p></li>';
				} else {
					selectedOptions.each(function(){
						optionsContent +='<li><p>'+$(this).find('p').text()+'</p></li>';
					});
				}

				self.summary.find('.summary-accessories').children('li').remove().end().append($(optionsContent));
			}
		});
	}

	ProductBuilder.prototype.updateListOptions = function(listItem) {
		var self = this;
		
		if( listItem.hasClass('js-radio') ) {
			//this means only one option can be selected (e.g., models) - so check if there's another option selected and deselect it
			var alreadySelectedOption = listItem.siblings('.selected'),
				price = (alreadySelectedOption.length > 0 ) ? -Number(alreadySelectedOption.data('price')) : 0;

			//if the option was already selected and you are deselecting it - price is the price of the option just clicked
			( listItem.hasClass('selected') ) 
				? price = -Number(listItem.data('price'))
				: price = Number(listItem.data('price')) + price;

			//now deselect all the other options
			alreadySelectedOption.removeClass('selected');
			//toggle the option just selected
			listItem.toggleClass('selected');
			//update totalPrice - only if the step is not the Models step
			(listItem.parents('[data-selection="models"]').length == 0) && self.updatePrice(price);
		} else {
			//more than one options can be selected - just need to add/remove the one just clicked
			var price = ( listItem.hasClass('selected') ) ? -Number(listItem.data('price')) : Number(listItem.data('price'));
			//toggle the option just selected
			listItem.toggleClass('selected');
			//update totalPrice
			self.updatePrice(price);
		}
		
		if( listItem.parents('[data-selection="models"]').length > 0 ) {
			//since a model has been selected/deselected, you need to update the builder content
			self.updateModelContent(listItem);
		}
	};

	ProductBuilder.prototype.updateModelContent = function(model) {
		var self = this;
		if( model.hasClass('selected') ) {
			var modelType = model.data('model'),
				modelImage = model.find('img').attr('src');

			//need to update the product image in the bottom fixed navigation
			this.modelPreview.attr('src', modelImage);

			//need to update the content of the builder according to the selected product
			//first - remove the contet which refers to a different model
			this.models.siblings('li').remove();
			//second - load the new content
			$.ajax({
		        type       : "GET",
		        dataType   : "html",
		        url        : 'components/Pricing/'+modelType+".html",
		        beforeSend : function(){
		        	self.loaded = false;
		        	model.siblings().removeClass('loaded');
		        },
		        success    : function(data){
		        	self.models.after(data);
		        	self.loaded = true;
		        	model.addClass('loaded');
		        	//activate top and bottom navigations
		        	self.fixedSummary.add(self.mainNavigation).removeClass('disabled show-alert');
		        	//update properties of the object
					self.steps = self.element.find('.builder-step');
					self.summary = self.element.find('[data-selection="summary"]');
					//detect click on one element in an options list
					self.optionsLists.off('click', '.js-option');
					self.optionsLists = self.element.find('.options-list');
					self.optionsLists.on('click', '.js-option', function(event){
						self.updateListOptions($(this));
					});

					//this is used not to load the animation the first time new content is loaded
					self.element.find('.first-load').removeClass('first-load');
		        },
		        error     : function(jqXHR, textStatus, errorThrown) {
		            //you may want to show an error message here
		        }
			});

			//update price (no adding/removing)
			this.totPriceWrapper.text(model.data('price'));
		} else {
			//no model has been selected
			this.fixedSummary.add(this.mainNavigation).addClass('disabled');
			//update price
			this.totPriceWrapper.text('0');

			this.models.find('.loaded').removeClass('loaded');
		}
	};

	ProductBuilder.prototype.customizeModel = function(target) {
		var parent = target.parent('li')
			index = parent.index();
		
		//update final price
		var price = ( parent.hasClass('selected') )
			? 0
			: Number(parent.data('price')) - parent.siblings('.selected').data('price');

		this.updatePrice(price);
		target.parent('li').addClass('selected').siblings().removeClass('selected').parents('.cd-product-customizer').siblings('.cd-product-previews').children('.selected').removeClass('selected').end().children('li').eq(index).addClass('selected');
	};

	ProductBuilder.prototype.updatePrice = function(price) {
		var actualPrice = Number(this.totPriceWrapper.text()) + price;
		this.totPriceWrapper.text(actualPrice);
	};
    
	if( $('.cd-product-builder').length > 0 ) {
		$('.cd-product-builder').each(function(){
			//create a productBuilder object for each .cd-product-builder
			new ProductBuilder($(this));
		});
	}
    
    
})
.controller("priceBookCtrl", function ($scope, $http, $stateParams,  $rootScope, AppService, PriceService) {
 
    
})
.controller("priceListCtrl", function ($scope, $http, $stateParams,  $rootScope, AppService, PriceService) {
    
    $scope.listType = $stateParams.id;
    $scope.app.priceRequestFiltered = new Array(); 
    
    $scope.filterPriceRequests = function()
    {
        var priceRequests = new Array();
        
             if ($scope.listType == 'submitted')
             {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        if ('NEW' == $rootScope.priceRequestList[x].REQUEST_STATUS)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;
             }
        
            // show pending/submitted quotes and approved quotes
             if ($scope.listType == 'active')
             {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        var status = $rootScope.priceRequestList[x].REQUEST_STATUS.toUpperCase();
                        if (status == 'NEW' ||
                            status == 'COMPLETE')
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;
             }
            /*else if ($scope.listType == 'draft')
            {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        if ('Draft' == $rootScope.priceRequestList[x].REQUEST_STATUS)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;                
            }*/
            else if ($scope.listType == 'rejected')
            {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        var status = $rootScope.priceRequestList[x].REQUEST_STATUS.toUpperCase();
                        if ('REJECTED' == status)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;                
            }
            else if ($scope.listType == 'expired')
            {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                       var status = $rootScope.priceRequestList[x].REQUEST_STATUS.toUpperCase();
                        if ('EXPIRED' == status)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;                
            }
            else if ($scope.listType == 'complete')
            {
                var listLength = $rootScope.priceRequestList.length;
                for (x = 0; x < listLength; x++)
                {
                        var status = $rootScope.priceRequestList[x].REQUEST_STATUS.toUpperCase();
                        if ('COMPLETE' == status)
                        {
                            priceRequests.push($rootScope.priceRequestList[x]);
                        }  
                }  
                return priceRequests;                
            }
    }
    
    
    $scope.listPriceRequests = function () {

			// synch if needed.				
			if ($rootScope.priceRequestList.length == 0)
			{
	   	  		AppService.synchPriceRequestList().then(function(d) {
		  		
	  			});
			}
    }
    
    // logic to validate action
		$scope.validateApproval = function validateApproval(request)
		{
			
			//if (!$scope.priceHeader.Requisition)
			//{
			//	showMessage('Error', 'Please select a job');
				//return false;
			//}

			//if (!$('#requestDate').val())
			//{
				//showMessage('Error', 'Please select a start date');
				//return false;
			//}
			
			return true; // if we are here then all is good
			
		}
		
		// logic to actually save the price request
		$scope.approvePriceRequest = function(request) {
			
			var isValid = false;
			isValid = $scope.validateApproval(request)
			
            //$scope.priceDetails[$scope.currentItem].MATERIAL = material.id.MATERIAL;
			
            var priceRequestTemp = new Array();
            priceRequestTemp.push(request.id);
			if (isValid == true)
				{
                PriceService.approveRequest(priceRequestTemp);
				//PriceService.getCSRFToken(PriceService.savePriceCreateData);
				}
		}        
     
		// logic to actually save the price request
		$scope.rejectPriceRequest = function(request) {
			
            var priceRequestTemp = new Array();
            priceRequestTemp.push(request.id);

                PriceService.rejectRequest(priceRequestTemp);

		}
        
		// logic to display the price request
		/*$scope.priceRequestDetails = function(request) {
			
                PriceService.rejectRequest(priceRequestTemp);

		}*/
        
        
   // initialize the controller    
   $scope.listPriceRequests(); //call list to start the screen
    
    if (!$scope.listType)
    {
     $scope.listType = 'submitted';   
    }
    
    $scope.app.priceRequestFiltered = $scope.filterPriceRequests();

    
});

// generic days function
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

var binarySearch = function(array, targetValue, fieldName) {
    var min = 0;
    var max = array.length - 1;
    var guess;
    while(min <= max) {
        guess = Math.floor((max + min) / 2);
        if (array[guess][fieldName] === targetValue) {
            return guess;
        }
        else if (array[guess][fieldName] < targetValue) {
            min = guess + 1;
        }
        else {
            max = guess - 1;
        }
    }
    return -1;
};