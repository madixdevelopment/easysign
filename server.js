
/**
 * Module dependencies.
 */
var compression = require('compression');
var express = require('express');
var routes = require('./routes');
var hits = require('./routes/hits');
var http = require('http');
var path = require('path');
var request = require('request');

var app = express();
app.use(compression());

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'www')));
app.use(app.router);

// replace for different systems.
//var serverURL = 'http://sapps300.madixinc.com:8000/sap/bc/zsd_easysign/';     //development
//var clientSAP = '120';                                                     //development

//var serverURL = 'http://sapps200.madixinc.com:8000/sap/bc/zsd_easysign/';//qa
//var clientSAP = '900';  

var serverURL = 'http://sapps105.madixinc.com:8005/sap/bc/zsd_easysign/';//prd
var clientSAP = '900';  


//var serverURL = 'http://sapps300.madixinc.com:8000/sap/bc/zsd_easysign/';//dev
//var clientSAP = '120';   //ECCAPPDC, ECCAPPCORP

//CBENSON, Redsox123 old pw

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/hits', hits.count);
app.post('/hit', hits.registerNew);

// refreshes data
app.get('/refreshShipmentList', function(req, res) {
    
    // get query parts
    var lastRefresh = req.query.LAST_REFRESH; //no longer needed, as we get all materials
    var plantId = req.query.PLANT;
    var fromDate = req.query.FROM_DATE;
    
    // build up url
    // default login in server SAP does not work 
    var url = serverURL + 'ZSD_EASYSIGN_SHIPMENT_LIST?format=json&sap-client=' + clientSAP +
        '&FROM_DATE=' + fromDate + '&PLANT=' + plantId + '&sap-user=EASYAPPS&sap-password=CHECKIN';
    
    console.log('calling shipment list refresh');
    console.log(url);
    //Lets try to make a HTTP GET request.
    request(url, function (error, response, body) {
        console.log('calling shipment list');
        if (!error && response.statusCode == 200) {
            console.log('success calling shipment list refresh');
            console.log(JSON.stringify(response));
            res.send(response);
        }
        else
        {
           console.log('error calling shipment list refresh');
           //console.log(response.body);
           //console.log('status' + response.statusCode);
            console.log(error);
           res.send(error); 
        }
    });
    
});


// saves a shipment checkin
app.post('/cancelShipmentCheckIn', function(req, res) {
    
    // build up url
    var url = serverURL + 'ZSD_EASYSIGN_SHIPMENT_CANCEL' + '?sap-user=EASYAPPS&sap-password=CHECKIN';
    
     console.log('calling shipment cancel');
     console.log(url);
     var stringData = JSON.stringify(req.body);
     request.post({
          headers: {'content-type' : 'application/json; charset=utf-8',
                    'X-Requested-With': 'X',
                    'Accept': 'application/json'},
          rejectUnauthorized: false, //stops certificate errors
          url:     url,
          body:    stringData
        }, function(error, response, body){
         
            if (!error && response.statusCode == 200) {
                console.log('success calling shipment cancellation');
                console.log(JSON.stringify(response));
                res.send(response);
            }
            else
            {
                //console.log(body)
                   console.log('error calling shipment cancellation');
                   console.log(error);
                   //console.log('status' + response.statusCode);
                   res.send(error); 
            }
         
        });
    
});

// saves a shipment
app.post('/saveShipmentCheckIn', function(req, res) {
    
    // build up url
    var url = serverURL + 'ZSD_EASYSIGN_SHIPMENT_UPDATE' + '?sap-user=EASYAPPS&sap-password=CHECKIN';
    
     console.log('calling shipment update');
    console.log(url);
     var stringData = JSON.stringify(req.body);
     request.post({
          headers: {'content-type' : 'application/json; charset=utf-8',
                    'X-Requested-With': 'X',
                    'Accept': 'application/json'},
          rejectUnauthorized: false, //stops certificate errors
          url:     url,
          body:    stringData
        }, function(error, response, body){
         
            if (!error && response.statusCode == 200) {
                console.log('success calling shipment save');
                console.log(JSON.stringify(response));
                res.send(response);
            }
            else
            {
                //console.log(body)
                   console.log('error calling shipment save');
                   //console.log(response.body);
                   //console.log('status' + response.statusCode);
                    console.log(error);   
                res.send(error); 
            }
         
        });
    
});

// iis version
//http.createServer(app).listen(process.env.PORT, function(){
//  console.log('Express server listening on port ' + process.env.PORT);
//});

//standard node
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

